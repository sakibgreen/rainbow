<?php


Auth::routes(); 

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('users', 'UserController');
Route::resource('roles', 'RoleController');
Route::resource('permissions', 'PermissionController');


//Admin Part- Backend
Route::get('/backend', 'AdminController@index');
Route::get('/backend/index', 'AdminController@adminindex');
Route::get('/backend/upload', 'AdminController@adminupload');
Route::get('/backend/quote', 'AdminController@adminquote');
Route::get('/backend/freetrial', 'AdminController@adminfreetrial');


//Rainbow
Route::get('/','IndexController@create')->name('indices.create');
Route::post('indices-save','IndexController@store');
Route::get('indices/{index}/edit', 'IndexController@edit')->name('indices.edit');
Route::post('indices/{id}', 'IndexController@update')->name('indices.update');
Route::delete('index/{index}', 'IndexController@destroy')->name('indices.destroy');


//Upload
Route::get('upload','UploadController@create')->name('uploads.create');
Route::post('uploads-save','UploadController@store');
Route::get('uploads/{upload}/edit', 'UploadController@edit')->name('uploads.edit');
Route::post('uploads/{id}', 'UploadController@update')->name('uploads.update');
Route::delete('upload/{upload}', 'UploadController@destroy')->name('uploads.destroy');

 
//Freetrial
Route::get('freetrial','FreetrialController@create')->name('freetrials.create');
Route::post('freetrials-save','FreetrialController@store');
Route::get('freetrials/{index}/edit', 'FreetrialController@edit')->name('freetrials.edit');
Route::post('freetrials/{id}', 'FreetrialController@update')->name('freetrials.update');
Route::delete('freetrial/{freetrial}', 'FreetrialController@destroy')->name('freetrials.destroy');


//Quote
Route::get('quote','QuoteController@create')->name('quotes.create');
Route::post('quotes-save','QuoteController@store');
Route::get('quotes/{index}/edit', 'QuoteController@edit')->name('quotes.edit');
Route::post('quotes/{id}', 'QuoteController@update')->name('quotes.update');
Route::delete('quote/{quote}', 'QuoteController@destroy')->name('quotes.destroy');


Route::post('send','PostController@send');
Route::get('email','PostController@email');


//Nav Bar
Route::get('/gallery', function () {
    return view('rainbow.gallery');
});
Route::get('/pricing', function () {
    return view('rainbow.pricing');
});
Route::get('/aboutus', function () {
    return view('rainbow.aboutus');
});

//Nav DropDown Sevice

Route::get('/clippingpath', function () {
    return view('rainbow.services.clippingpath');
});
Route::get('/color', function () {
    return view('rainbow.services.color');
});
Route::get('/ghost', function () {
    return view('rainbow.services.ghost');
});
Route::get('/imagemasking', function () {
    return view('rainbow.services.imagemasking');
});
Route::get('/photocroping', function () {
    return view('rainbow.services.photocroping');
});
Route::get('/photorestoration', function () {
    return view('rainbow.services.photorestoration');
});
Route::get('/photoretouching', function () {
    return view('rainbow.services.photoretouching');
});
Route::get('/realestate', function () {
    return view('rainbow.services.realestate');
});
Route::get('/product', function () {
    return view('rainbow.services.product');
});
Route::get('/realestate', function () {
    return view('rainbow.services.realestate');
});
Route::get('/shadowcreation', function () {
    return view('rainbow.services.shadowcreation');
});
Route::get('/vector', function () {
    return view('rainbow.services.vector');
});
Route::get('/wedding', function () {
    return view('rainbow.services.wedding');
});
Route::get('/logodesign', function () {
    return view('rainbow.services.logodesign');
});
Route::get('/webdesign', function () {
    return view('rainbow.webservice.webdesign');
});
Route::get('/apps', function () {
    return view('rainbow.webservice.apps');
});
Route::get('/software', function () {
    return view('rainbow.webservice.software');
});
Route::get('/ecommerce', function () {
    return view('rainbow.webservice.ecommerce');
});


//Payment to Paypal
Route::get('/paypal', function () {
    return view('rainbow.paywithpaypal');
});

Route::post('paypal','PaymentController@paywithpaypal');
Route::get('status','PaymentController@getPaymentStatus');
