<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use Session;
use App\User;
use App\Upload;
use App\Admin;
use Mail;
use App\Mail\SendMail;
class UploadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct() {
        $this->middleware(['auth', 'clearance'])->except('create','edit');
    }
    
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('rainbow.upload');
    }

 
    public function store(Request $request)
    {
        $this->validate($request,
            [
				'name'           => 'required|max:255',
                'phone'          => 'required|max:11',
                'address'        => 'max:255',
            ]);
                //dd($request->all());
        $images = array();
        if ($request->hasFile('images')) {
            foreach ($request->images as $image) {

                $name = $image->getClientOriginalName();
                $image->storeAs('public/upload', $name);
                $images[] = $name;

            }
        } 
 
        Upload::create([
			'name'          => request('name'),
            'company'       => request('company'),
            'phone'         => request('phone'),
            'address'       => request('address'),
            'website'       => request('website'),
            'images'        => implode("|", $images), 
            'country'       => request('country'),
            'job_title'     => request('job_title'),
            'to'            => request('to'),
            'message'       => request('message'),

		]);
		Mail::send(new SendMail("message"));

        return redirect('upload');
    }

    public function edit($id)
    {
        $uploads = Upload::find($id);
        return view('rainbow.upload_edit',compact('uploads'));
    }

    public function update(Request $request, $id)
    {
        $upload              = Upload::find($id);
        $upload->name        = $request->name;
        $upload->company     = $request->company;
        $upload->phone       = $request->phone;
        $upload->address     = $request->address;
        $upload->website     = $request->website;
        $upload->images      = $request->images;
        $upload->country     = $request->country;
        $upload->job_title   = $request->job_title;
        $upload->to          = $request->to;
        $upload->message     = $request->message; 
        $upload->save();

        return redirect('/backend/upload');
    }

    public function destroy($id)
    {
        $uploads =  Upload::find($id);
        $uploads->delete();

        return back(); 
    }

}
