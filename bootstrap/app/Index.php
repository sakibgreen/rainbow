<?php

namespace App;
use App\Admin;
use App\User;

use Illuminate\Database\Eloquent\Model;

class Index extends Model
{
    protected $guarded = [];

    public function admins()
	{
		return $this->belongsTo(Admin::class);
	}

	public function users()
    {
    	return $this->belongsTo(User::class);
    }

    public static function user_name($id)
    {
    	$name = User::find($id);
    	if($name != NULL)
    	{
    		return $name->name;
    	}
    	else
    	{
    		return 'Null';
    	}
    }
}
   