<?php

namespace App;
use App\Index;
use App\Quote;
use App\Freetrial;
use App\Upload;

use Illuminate\Database\Eloquent\Model;

class Admin extends Model
{
    protected $guarded = [];

    public function indices()
    {
    	return $this->hasMany(Index::class);
    }

    public function uploads()
    {
    	return $this->hasMany(Upload::class);
    }

    public function quotes()
    {
    	return $this->hasMany(Quote::class);
    }
    
    public function freetrials()
    {
    	return $this->hasMany(Freetrial::class);
    }
}
   