@extends('layouts.master')
@section('title', '| Admin Quote')
@section('content')          
                            <div class="table-responsive table-responsive-data2">
                                <table class="table table-data2">
                                    <thead>
                                        <tr>
                                            <th>Mark</th>
                                            <th>User ID</th>
                                            <th>Name</th>
                                            <th>Company</th>
                                            <th>Phone</th>
                                            <th>Address</th>
                                            <th>Website</th>
                                            <th>File</th>
                                            <th>Conutry</th>
                                            <th>Job Tytle</th>
                                            <th>Mail</th>
                                            <th>Message</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($quotes as $quote)    
                                            <tr class="tr-shadow">
                                            <td>
                                                <label class="au-checkbox">
                                                    <input type="checkbox">
                                                    <span class="au-checkmark"></span>
                                                </label>
                                            </td>
                                            <td> {{ $quote->id }}</td>
                                            <td> {{ $quote->name }}</td>
                                            <td> {{ $quote->company }} </td>
                                            <td class="desc">{{ $quote->phone }}</td>
                                            <td class="desc">{{ $quote->address }}</td>
                                            <td class="desc">{{ $quote->website }}</td>
                                            <td>
                                                @foreach(explode('|', $quote->images) as $detail )
                                                    <a href="{{ asset('storage/upload/' . $detail) }}" class="widget-products-image">
                                                        <img src="{{ asset('storage/upload/' . $detail) }}" style="max-height: 300px">
                                                        <span class="widget-products-overlay"></span>
                                                    </a>
                                                @endforeach
                                            </td>
                                            <td class="desc">{{ $quote->country }}</td>
                                            <td class="desc">{{ $quote->job_title }}</td>                                         
                                            <td>
                                                <span class="status--process">{{ $quote->to }}</span>
                                            </td>
                                            <td>{{ $quote->message }}</td>
                                            <td>
                                                <div class="table-data-feature">
                                                    <!-- <form action="{{route('quotes.edit', $quote->id)}}">
                                                        <button class="item" data-toggle="tooltip" data-placement="top" title="Edit">
                                                            <i class="zmdi zmdi-edit"></i>
                                                        </button>
                                                    </form> -->
                                                    <form action="{{route('quotes.destroy',$quote->id)}}" method="post">
                                                    {{csrf_field()}}         
                                                        <input type="hidden" name="_method" value="DELETE">
                                                        <button class="item" data-toggle="tooltip" data-placement="top" title="Delete">
                                                            <i class="zmdi zmdi-delete"></i>
                                                        </button>               
                                                    </form> 
                                                </div>
                                            </td>
                                        </tr>
                                        <tr class="spacer"></tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                <div class="text-center">
                                    {!! $quotes->links() !!}
                                </div>
                            </div>

@endsection