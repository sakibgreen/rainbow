@extends('layouts.master')
@section('title', '| Admin Upload')
@section('content')          
                            <div class="table-responsive table-responsive-data2">
                                <table class="table table-data2">
                                    <thead>
                                        <tr>
                                            <th>Mark</th>
                                            <th>User ID</th>
                                            <th>Name</th>
                                            <th>Company</th>
                                            <th>Phone</th>
                                            <th>Address</th>
                                            <th>Website</th>
                                            <th>File</th>
                                            <th>Conutry</th>
                                            <th>Job Tytle</th>
                                            <th>Mail</th>
                                            <th>Message</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($uploads as $upload)    
                                            <tr class="tr-shadow">
                                            <td>
                                                <label class="au-checkbox">
                                                    <input type="checkbox">
                                                    <span class="au-checkmark"></span>
                                                </label>
                                            </td>
                                            <td> {{ $upload->id }}</td>
                                            <td> {{ $upload->name }}</td>
                                            <td> {{ $upload->company }} </td>
                                            <td class="desc">{{ $upload->phone }}</td>
                                            <td class="desc">{{ $upload->address }}</td>
                                            <td class="desc">{{ $upload->website }}</td>
                                            <td>
                                                @foreach(explode('|', $upload->images) as $detail )
                                                    <a href="{{ asset('storage/upload/' . $detail) }}" class="widget-products-image">
                                                        <img src="{{ asset('storage/upload/' . $detail) }}" style="max-height: 300px">
                                                        <span class="widget-products-overlay"></span>
                                                    </a>
                                                @endforeach
                                            </td>
                                            <td class="desc">{{ $upload->country }}</td>
                                            <td class="desc">{{ $upload->job_title }}</td>                                         
                                            <td>
                                                <span class="status--process">{{ $upload->to }}</span>
                                            </td>
                                            <td>{{ $upload->message }}</td>
                                            <td>
                                                <div class="table-data-feature">
                                                    
                                                    <form action="{{route('uploads.destroy',$upload->id)}}" method="post">
                                                    {{csrf_field()}}         
                                                        <input type="hidden" name="_method" value="DELETE">
                                                        <button class="item" data-toggle="tooltip" data-placement="top" title="Delete">
                                                            <i class="zmdi zmdi-delete"></i>
                                                        </button>               
                                                    </form> 
                                                </div>
                                            </td>
                                        </tr>
                                        <tr class="spacer"></tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                <div class="text-center">
                                    {!! $uploads->links() !!}
                                </div>
                            </div>

@endsection