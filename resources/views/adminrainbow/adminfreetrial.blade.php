@extends('layouts.master')
@section('title', '| Admin freetrial')
@section('content')
                            <div class="table-responsive table-responsive-data2">
                                <table class="table table-data2">
                                    <thead>
                                        <tr>
                                            <th>Mark</th>
                                            <th>User ID</th>
                                            <th>Name</th>
                                            <th>Company</th>
                                            <th>Phone</th>
                                            <th>Address</th>
                                            <th>Website</th>
                                            <th>File</th>
                                            <th>Job Tytle</th>
                                            <th>Mail</th>
                                            <th>Message</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($freetrials as $freetrial)    
                                            <tr class="tr-shadow">
                                            <td>
                                                <label class="au-checkbox">
                                                    <input type="checkbox">
                                                    <span class="au-checkmark"></span>
                                                </label>
                                            </td>
                                            <td> {{ $freetrial->id }}</td>
                                            <td> {{ $freetrial->name }}</td>
                                            <td> {{ $freetrial->company }} </td>
                                            <td class="desc">{{ $freetrial->phone }}</td>
                                            <td class="desc">{{ $freetrial->address }}</td>
                                            <td class="desc">{{ $freetrial->website }}</td>
                                            <td>
                                                @foreach(explode('|', $freetrial->images) as $detail )
                                                    <a href="{{ asset('storage/upload/' . $detail) }}" class="widget-products-image">
                                                        <img src="{{ asset('storage/upload/' . $detail) }}" style="max-height: 30px">
                                                        <span class="widget-products-overlay"></span>
                                                    </a>
                                                @endforeach
                                            </td>
                                            <td class="desc">{{ $freetrial->job_title }}</td>                                         
                                            <td>
                                                <span class="status--process">{{ $freetrial->to }}</span>
                                            </td>
                                            <td>{{ $freetrial->message }}</td>
                                            <td>
                                                <div class="table-data-feature">
                                                    <!-- <form action="{{route('freetrials.edit', $freetrial->id)}}">
                                                        <button class="item" data-toggle="tooltip" data-placement="top" title="Edit">
                                                            <i class="zmdi zmdi-edit"></i>
                                                        </button>
                                                    </form> -->
                                                    <form action="{{route('freetrials.destroy',$freetrial->id)}}" method="post">
                                                    {{csrf_field()}}         
                                                        <input type="hidden" name="_method" value="DELETE">
                                                        <button class="item" data-toggle="tooltip" data-placement="top" title="Delete">
                                                            <i class="zmdi zmdi-delete"></i>
                                                        </button>               
                                                    </form> 
                                                </div>
                                            </td>
                                        </tr>
                                        <tr class="spacer"></tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                <div class="text-center">
                                    {!! $freetrials->links() !!}
                                </div>
                            </div>

@endsection 