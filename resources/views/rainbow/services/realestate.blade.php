@extends('rainbow.layouts.master')

@section('title', '| Realestate')

@section('content')
	<main id="content" role="main">
		<!-- Author Section -->
		<div class="position-relative text-center u-space-3-top u-gradient-overlay-half-info-v1 u-bg-img-hero" style="background-image: url(assets/img/1920x800/img10.jpg);">
			<div class="row justify-content-md-center">
				<div class="col-md-8 col-lg-7 col-xl-11">
					<!-- Info -->
					<div class="service_color">
					<br><br>
						<h1 class="display-6 text-white font-weight-normal mb-3">REAL ESTATE PHOTO EDITING</h1>					
						<p style="text-align: justify;"> For professionals at any architectural company, it is mandatory to use clean and clear image of real estate property, so that it is easy for clients or customer to check the property image. Exterior lamp post or recycle bins could hamper the view of any building image, vehicles parked outside the house could also distract potential customers. Our Image Editing HQ and its well experienced graphics designers are here to help you with your real estate property image. We manage all kind of photo editing technique to remove any unwanted things from your image and turn it into a professional look, which later could be used for magazines or brochures.</p><br>
						
						<h3 style="text-align: justify;"> Background Replacement</h3>						
						<p style="text-align: justify;"> Background replacement in a property image is a very attractive way for various real estate business owners. This procedure helps to create attractive location background to enhance and beautify the location. Sometimes the backgrounds is removed and a transparent layer is used, which later can be used to upload in websites or used in text based brochure of any individual company. Our expert designers at Image Editing HQ can help you to get the best background replacement for your real estate picture.</p><br>
						
						<h3 style="text-align: justify;"> Sky Change real estate photo retouching services</h3>						
						<p style="text-align: justify;"> The clear blue sky at the back top of house can enhance the image quality to a much greater view. it is an essential to change the cloudy or dull saturated sky, as it does not display a good composition of the image. Our team at Image Editing HQ can help you to achieve a eye catching sky background to enhance the image quality and composition of your property image.</p><br>						
						
						<h3 style="text-align: justify;"> Real Estate Photo Retouching</h3>						
						<p style="text-align: justify;"> Retouching real estate photographs is one major thing to do after the image are photographed, this is the procedure to clean out any extra unwanted things , fix different portions of an image if there is any. Retouching real estate photos can be greatly helpful to beautify any unclean portion of a room interior or exterior, and thus it could help in grow the business rapidly.</p><br>
						
						<h3 style="text-align: justify;"> Exterior Twilight</h3>						
						<p style="text-align: justify;"> A well lit up house at the time of dusk shows the true beauty of the house and locale. Our expert designers can help you to light up a house and beautify the image with best professional look for various websites and architectural magazines.</p><br>
						
						<h3 style="text-align: justify;"> Blending Service</h3>						
						<p style="text-align: justify;"> Blending is the process to reveal the true color and nature of an image via various color adjustment and exposure adjustment. And blending is very essential for real estate business owners, as it showcases the actual situation of a rooms interior or exterior under proper light setup. For best professional outlook of your real estate property image, our team is available 24/7 around the clock at your service.</p><br>
						
						<p style="text-align: justify;"> Hire us for all kinds of professional real estate photo editing services from our expert retouchers.</p><br>

						<h3 style="text-align: justify;"> </h3>						
						<p style="text-align: justify;"> </p><br>
					</div>
					<!-- End Info -->

					<!-- Followers -->
					<div class="d-flex justify-content-center align-items-center mb-7">
						<!-- Followers List -->
						<ul class="list-inline mr-2 mb-0">
							<li class="list-inline-item mr-0">
								<img class="img-fluid u-sm-avatar u-sm-avatar--bordered rounded-circle" src="assets/img/32x32/img1.jpg" alt="Image Description">
							</li>
							<li class="list-inline-item ml-offset-3 mr-0">
								<img class="img-fluid u-sm-avatar u-sm-avatar--bordered rounded-circle" src="assets/img/32x32/img3.jpg" alt="Image Description">
							</li>
							<li class="list-inline-item ml-offset-3 mr-0">
								<img class="img-fluid u-sm-avatar u-sm-avatar--bordered rounded-circle" src="assets/img/32x32/img2.jpg" alt="Image Description">
							</li>
							<li class="list-inline-item ml-offset-3 mr-0">
								<img class="img-fluid u-sm-avatar u-sm-avatar--bordered rounded-circle" src="assets/img/32x32/img4.jpg" alt="Image Description">
							</li>
							<li class="list-inline-item ml-offset-3 mr-0">
								<img class="img-fluid u-sm-avatar u-sm-avatar--bordered rounded-circle" src="assets/img/32x32/img5.jpg" alt="Image Description">
							</li>
						</ul>
						<!-- End Followers List -->
					</div>
					<!-- End Followers -->

					<!-- Avatar -->
					<img class="img-fluid u-xl-avatar u-xl-avatar--bordered rounded-circle mx-auto" src="assets/img/cli.jpg" alt="Image Description">
					<!-- End Avatar -->
				</div>

				<!-- SVG Background -->
				<figure class="position-absolute-bottom-0 z-index-minus-1">
					<svg preserveAspectRatio="none" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="100%" height="200px" viewBox="20 -20 300 100" style="margin-bottom: -8px;" xml:space="preserve">
						<path class="u-fill-white" opacity="0.4" d="M30.913,43.944c0,0,42.911-34.464,87.51-14.191c77.31,35.14,113.304-1.952,146.638-4.729
              c48.654-4.056,69.94,16.218,69.94,16.218v54.396H30.913V43.944z" />
						<path class="u-fill-white" opacity="0.4" d="M-35.667,44.628c0,0,42.91-34.463,87.51-14.191c77.31,35.141,113.304-1.952,146.639-4.729
              c48.653-4.055,69.939,16.218,69.939,16.218v54.396H-35.667V44.628z" />
						<path class="u-fill-white" opacity="0" d="M43.415,98.342c0,0,48.283-68.927,109.133-68.927c65.886,0,97.983,67.914,97.983,67.914v3.716
              H42.401L43.415,98.342z" />
						<path class="u-fill-white" d="M-34.667,62.998c0,0,56-45.667,120.316-27.839C167.484,57.842,197,41.332,232.286,30.428
              c53.07-16.399,104.047,36.903,104.047,36.903l1.333,36.667l-372-2.954L-34.667,62.998z" />
					</svg>
				</figure>
				<!-- End SVG Background Section -->
			</div>
		</div>
		<!-- End Author Section -->

		<!-- Cubeportfolio Section -->
		<div class="container u-space-2-top u-space-3-bottom u-cubeportfolio">

			<!-- Content -->
			<div class="cbp mb-7" data-controls="#cubeFilter" data-animation="quicksand" data-x-gap="16" data-y-gap="16" data-load-more-selector="#cubeLoadMore" data-load-more-action="auto" data-load-items-amount="2" data-media-queries='[
              {"width": 1500, "cols": 4},
              {"width": 1100, "cols": 4},
              {"width": 800, "cols": 3},
              {"width": 480, "cols": 2},
              {"width": 300, "cols": 1}
            ]'>
				<!-- Item -->
				<div class="cbp-item rounded abstract">
					<div class="cbp-caption">
						<a class="cbp-lightbox u-media-viewer" href="assets/img/Realestatephotoediting/11.png" data-title="RainbowClipping">
							<img src="assets/img/Realestatephotoediting/1.png" alt="Image Description">
							<span class="u-media-viewer__container">
								<span class="u-media-viewer__icon">
									<span class="fa fa-plus u-media-viewer__icon-inner"></span>
								</span>
							</span>
						</a>
					</div>
				</div>
				<!-- End Item -->

				<!-- Item -->
				<div class="cbp-item rounded branding">
					<div class="cbp-caption">
						<a class="cbp-lightbox u-media-viewer" href="assets/img/Realestatephotoediting/22.png" data-title="RainbowClipping">
							<img src="assets/img/Realestatephotoediting/2.png" alt="Image Description">
							<span class="u-media-viewer__container">
								<span class="u-media-viewer__icon">
									<span class="fa fa-plus u-media-viewer__icon-inner"></span>
								</span>
							</span>
						</a>
					</div>
				</div>
				<!-- End Item -->

				<!-- Item -->
				<div class="cbp-item rounded abstract">
					<div class="cbp-caption">
						<a class="cbp-lightbox u-media-viewer" href="assets/img/Realestatephotoediting/22.png" data-title="RainbowClipping">
							<img src="assets/img/Realestatephotoediting/2.png" alt="Image Description">
							<span class="u-media-viewer__container">
								<span class="u-media-viewer__icon">
									<span class="fa fa-plus u-media-viewer__icon-inner"></span>
								</span>
							</span>
						</a>
					</div>
				</div>
				<!-- End Item -->

				<!-- Item -->
				<div class="cbp-item rounded branding">
					<div class="cbp-caption">
						<a class="cbp-lightbox u-media-viewer" href="assets/img/Realestatephotoediting/33.png" data-title="RainbowClipping">
							<img src="assets/img/Realestatephotoediting/3.png" alt="Image Description">
							<span class="u-media-viewer__container">
								<span class="u-media-viewer__icon">
									<span class="fa fa-plus u-media-viewer__icon-inner"></span>
								</span>
							</span>
						</a>
					</div>
				</div>

				<!-- End Item -->
			</div>
			<!-- End Content -->

		</div>
		<!-- End Cubeportfolio Section -->

		<!-- Instafeed -->
		<div id="instaFeed" class="js-instagram row mx-gutters-2" data-user-id="4815936096" data-client-id="60648b9247324e87a4d7ab3aa7a58f6c" data-token="4815936096.1677ed0.1cbcaacad4de4ffa96aa15ec76e0e5ce" data-limit="4" data-template='<div class="col-md-3 mb-3 mb-sm-0"><a href="%7b%7blink%7d%7d.html" target="_blank"><img class="img-fluid w-100 rounded" src="%7b%7bimage%7d%7d.html" /></a></div>'></div>
		<!-- End Instafeed -->
		<!-- End Instagram -->

		<hr class="my-0">
	</main>

@endsection