@extends('rainbow.layouts.master')

@section('title', '| Vector')

@section('content')
	<main id="content" role="main">
		<!-- Author Section -->
		<div class="position-relative text-center u-space-3-top u-gradient-overlay-half-info-v1 u-bg-img-hero" style="background-image: url(assets/img/1920x800/img10.jpg);">
			<div class="row justify-content-md-center">
				<div class="col-md-8 col-lg-7 col-xl-11">
					<!-- Info -->
					<div class="service_color">
					<br><br>
						<h1 class="display-6 text-white font-weight-normal mb-3 text-uppercase">Vector Path Service</h1>
						<p style="text-align: justify;"> Photoshop compatible, such as Adobe Illustrator, Adobe Photoshop,  InDesign and others, supports image editing services. For this, in the world of computer graphics, playing with manipulating your image is now very easy and interesting. You can apply any readjustment and new scale in the rest of the image with the benefit of the Photoshop programs that were created as before.</p>
                        <p style="text-align: justify;"> Photoshop programs gradually introduce a large number of image editing tools such as Clipping Path, multipath, vector path, deep  etch closed to the vector path, image masking, image retouching, color correction, Vector rendering, or plus. Your application does not matter what Photoshop can instantly solve with the tools listed above.</p>
                        <h3 style="text-align: justify;"> Why Using Vector Path Service?</h3>
                        <p style="text-align: justify;"> The vector path or Path is the best of all the services provided by Clipping Art Image. The clipping path helps you create a point with pencil tools inside or outside the edge that you want to erase or divide from your frame. You can often see that your images can not give an adequate result due to unwanted incidents. Since you have taken your precious photos several times before you want to store them for your future needs. You must take a step to modify your image. On the other hand, you often want to manipulate your image with different tests. The background can be isolated, replacing the beloved part of the image in a new or perfect frame. Clipping Path can solve all previous expectations of the clutch vector path for selection. Creates a selection line on a soft border and is covered by a font created by the Pen tool. Once selected, you can make revolutionary changes and save it for later use.</p>
                        <p style="text-align: justify;"> Clipping Art Image provides the Clipping Path service worldwide. We have a lot of experienced designers for many years. They are all very skilled. They invest all their effort to create a precise and satisfactory cutting path with the pencil tool and in different ways they need to take a step to overcome them. After creating the clipping path that the designers have made, we verify the triangular income to obtain a better result. On the other hand, we offer low cost instead of others without losing image quality.</p>
                        <h3 style="text-align: justify;"> Why Choose Us?</h3>
                        <p style="text-align: justify;"> If you want to know our services about the cutting Path, you can contact us using your email or contact number directly. Be sure to answer in a few minutes. We are always committed to serving you as our supreme innovator.</p>
                        <p style="text-align: justify;"> In fact, this gives a new look to your previous photo or converts your logo or designs into fully customizable vector graphics for use in different applications or advertising tools. The score is known as the routing service process of the router. Dotting refers to the vector of the technology used in the conversion process. There are many vector conversion programs available in the market that will facilitate the conversion process. The vector logo design can be fun and credible. Clipping Way offers a 100% high-quality conversion service that transforms the client’s image into an elegant image, as the best tracking service provider in Asia.</p>
                        <p style="text-align: justify;"> </p>					
                    </div>
					<!-- End Info -->

					<!-- Followers -->
					<div class="d-flex justify-content-center align-items-center mb-7">
						<!-- Followers List -->
						<ul class="list-inline mr-2 mb-0">
							<li class="list-inline-item mr-0">
								<img class="img-fluid u-sm-avatar u-sm-avatar--bordered rounded-circle" src="assets/img/32x32/img1.jpg" alt="Image Description">
							</li>
							<li class="list-inline-item ml-offset-3 mr-0">
								<img class="img-fluid u-sm-avatar u-sm-avatar--bordered rounded-circle" src="assets/img/32x32/img3.jpg" alt="Image Description">
							</li>
							<li class="list-inline-item ml-offset-3 mr-0">
								<img class="img-fluid u-sm-avatar u-sm-avatar--bordered rounded-circle" src="assets/img/32x32/img2.jpg" alt="Image Description">
							</li>
							<li class="list-inline-item ml-offset-3 mr-0">
								<img class="img-fluid u-sm-avatar u-sm-avatar--bordered rounded-circle" src="assets/img/32x32/img4.jpg" alt="Image Description">
							</li>
							<li class="list-inline-item ml-offset-3 mr-0">
								<img class="img-fluid u-sm-avatar u-sm-avatar--bordered rounded-circle" src="assets/img/32x32/img5.jpg" alt="Image Description">
							</li>
						</ul>
						<!-- End Followers List -->
					</div>
					<!-- End Followers -->

					<!-- Avatar -->
					<img class="img-fluid u-xl-avatar u-xl-avatar--bordered rounded-circle mx-auto" src="assets/img/cli.jpg" alt="Image Description">
					<!-- End Avatar -->
				</div>

				<!-- SVG Background -->
				<figure class="position-absolute-bottom-0 z-index-minus-1">
					<svg preserveAspectRatio="none" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="100%" height="200px" viewBox="20 -20 300 100" style="margin-bottom: -8px;" xml:space="preserve">
						<path class="u-fill-white" opacity="0.4" d="M30.913,43.944c0,0,42.911-34.464,87.51-14.191c77.31,35.14,113.304-1.952,146.638-4.729
              c48.654-4.056,69.94,16.218,69.94,16.218v54.396H30.913V43.944z" />
						<path class="u-fill-white" opacity="0.4" d="M-35.667,44.628c0,0,42.91-34.463,87.51-14.191c77.31,35.141,113.304-1.952,146.639-4.729
              c48.653-4.055,69.939,16.218,69.939,16.218v54.396H-35.667V44.628z" />
						<path class="u-fill-white" opacity="0" d="M43.415,98.342c0,0,48.283-68.927,109.133-68.927c65.886,0,97.983,67.914,97.983,67.914v3.716
              H42.401L43.415,98.342z" />
						<path class="u-fill-white" d="M-34.667,62.998c0,0,56-45.667,120.316-27.839C167.484,57.842,197,41.332,232.286,30.428
              c53.07-16.399,104.047,36.903,104.047,36.903l1.333,36.667l-372-2.954L-34.667,62.998z" />
					</svg>
				</figure>
				<!-- End SVG Background Section -->
			</div>
		</div>
		<!-- End Author Section -->

		<!-- Cubeportfolio Section -->
		<div class="container u-space-2-top u-space-3-bottom u-cubeportfolio">

			<!-- Content -->
			<div class="cbp mb-7" data-controls="#cubeFilter" data-animation="quicksand" data-x-gap="16" data-y-gap="16" data-load-more-selector="#cubeLoadMore" data-load-more-action="auto" data-load-items-amount="2" data-media-queries='[
              {"width": 1500, "cols": 4},
              {"width": 1100, "cols": 4},
              {"width": 800, "cols": 3},
              {"width": 480, "cols": 2},
              {"width": 300, "cols": 1}
            ]'>
				<!-- Item -->
				<div class="cbp-item rounded abstract">
					<div class="cbp-caption">
						<a class="cbp-lightbox u-media-viewer" href="assets/img/VectorIllustrationConversion/44.png" data-title="RainbowClipping">
							<img src="assets/img/VectorIllustrationConversion/4.png" alt="Image Description">
							<span class="u-media-viewer__container">
								<span class="u-media-viewer__icon">
									<span class="fa fa-plus u-media-viewer__icon-inner"></span>
								</span>
							</span>
						</a>
					</div>
				</div>
				<!-- End Item -->

				<!-- Item -->
				<div class="cbp-item rounded branding">
					<div class="cbp-caption">
						<a class="cbp-lightbox u-media-viewer" href="assets/img/VectorIllustrationConversion/11.png" data-title="RainbowClipping">
							<img src="assets/img/VectorIllustrationConversion/1.png" alt="Image Description">
							<span class="u-media-viewer__container">
								<span class="u-media-viewer__icon">
									<span class="fa fa-plus u-media-viewer__icon-inner"></span>
								</span>
							</span>
						</a>
					</div>
				</div>
				<!-- End Item -->

				<!-- Item -->
				<div class="cbp-item rounded abstract">
					<div class="cbp-caption">
						<a class="cbp-lightbox u-media-viewer" href="assets/img/VectorIllustrationConversion/22.png" data-title="RainbowClipping">
							<img src="assets/img/VectorIllustrationConversion/2.png" alt="Image Description">
							<span class="u-media-viewer__container">
								<span class="u-media-viewer__icon">
									<span class="fa fa-plus u-media-viewer__icon-inner"></span>
								</span>
							</span>
						</a>
					</div>
				</div>
				<!-- End Item -->

				<!-- Item -->
				<div class="cbp-item rounded branding">
					<div class="cbp-caption">
						<a class="cbp-lightbox u-media-viewer" href="assets/img/VectorIllustrationConversion/33.png" data-title="RainbowClipping">
							<img src="assets/img/VectorIllustrationConversion/3.png" alt="Image Description">
							<span class="u-media-viewer__container">
								<span class="u-media-viewer__icon">
									<span class="fa fa-plus u-media-viewer__icon-inner"></span>
								</span>
							</span>
						</a>
					</div>
				</div>

				<!-- End Item -->
			</div>
			<!-- End Content -->

		</div>
		<!-- End Cubeportfolio Section -->

		<!-- Instafeed -->
		<div id="instaFeed" class="js-instagram row mx-gutters-2" data-user-id="4815936096" data-client-id="60648b9247324e87a4d7ab3aa7a58f6c" data-token="4815936096.1677ed0.1cbcaacad4de4ffa96aa15ec76e0e5ce" data-limit="4" data-template='<div class="col-md-3 mb-3 mb-sm-0"><a href="%7b%7blink%7d%7d.html" target="_blank"><img class="img-fluid w-100 rounded" src="%7b%7bimage%7d%7d.html" /></a></div>'></div>
		<!-- End Instafeed -->
		<!-- End Instagram -->

		<hr class="my-0">
	</main>

@endsection