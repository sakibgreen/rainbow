@extends('rainbow.layouts.master')

@section('title', '| Photo Retouching')

@section('content')
	<main id="content" role="main"> 
		<!-- Author Section -->
		<div class="position-relative text-center u-space-3-top u-gradient-overlay-half-info-v1 u-bg-img-hero" style="background-image: url(assets/img/1920x800/img10.jpg);">
			<div class="row justify-content-md-center">
				<div class="col-md-8 col-lg-7 col-xl-11">
					<!-- Info -->
					<div class="service_color">
					<br><br>
						<h1 class="display-6 text-white font-weight-normal mb-3">Photo Retouching Service</h1>
						<p style="text-align: justify;"> Images are often modified and manipulated for marketing purposes, as they are an innovative and creative way to reach your target audience. The correct image can speak a thousand words. If your organization is looking for expert image editing services or if your photo studio contains thousands of images to be edited in a quick time, you have come to the right place.</p>
						<p style="text-align: justify;"> Clipping Art Image has been working for years rendering Photoshop retouching services globally with the photo studios, publishers, organizations, professional photographers and etc. Our skilled photo editors specify in the use of the latest image editing as well as image retouching tools and only we can provide the highest quality services up to our customer requirements.</p>
						<h3 style="text-align: justify;"> Clipping Art Image photo retouching services</h3>
						<p style="text-align: justify;"> Photo retouching is a slow, skilful and experienced process on behalf of the photo editing professional. We can refine your digital photos and make them look clearly defined with the exact shape you want. We work with a variety of image file formats such as JPEG, PSD, TIFF, GIF, PNG, PGF, RAW, etc. Some of our main image reprint services include:</p>
						<p style="text-align: justify;"> </p>
						<ul>
							<li><p style="text-align: justify;"> Product Photo Retouching</p></li>
						</ul>
						<p style="text-align: justify;"> Photo Redirection Services at Clipping Art Image offers you an advantage over other e-commerce sites. Our services can help you solve lighting problems, eliminate funds and improve the attractiveness of the product in the market.</p>
						<ul>
							<li><p style="text-align: justify;"> Portrait Retouching</p></li>
						</ul>
						<p style="text-align: justify;"> Our personal image reformulation services ensure that the images look natural and realistic. We are well trained to provide professional looking images, while avoiding the use of steam spray techniques that give personal photos a plastic appearance.</p>
						<ul>
							<li><p style="text-align: justify;"> Real Estate Photography Retouching</p></li>
						</ul>
						<p style="text-align: justify;"> Our real estate photo retouching services add an attractive atmosphere, adequate lighting and a precise size to your real estate images that should attract property hunters. We try to reject the errors caused by the cameras and make the structural images look realistic.</p>
						<ul>
							<li><p style="text-align: justify;"> Commercial photo Retouching</p></li>
						</ul>
						<p style="text-align: justify;"> Our services include scanning commercial photos while maintaining the name and reputation of your brand. Our image editors work with site photos and corporate photographs to maintain a commercial reputation at a level that supports your brand.</p>
						<ul>
							<li><p style="text-align: justify;"> Amateur Image Retouching</p></li>
						</ul>
						<p style="text-align: justify;"> Our team also works with amateur photographers and helps them to retouch the image. In addition, we also help them to better understand their cameras and to focus on the correct photographic techniques.</p>
						<ul>
							<li><p style="text-align: justify;"> Jewellery Photo Retouching</p></li>
						</ul>
						<p style="text-align: justify;"> At Clipping Art Image we have a dedicated team of editors working with jewellery photographers, as well as online and offline retailers. We personalize jewellery images with brilliant improvements, adjust the focus stack, eliminate the background, eliminate scratches, etc.</p>
						<ul>
							<li><p style="text-align: justify;"> Wedding Image Retouching</p></li>
						</ul>
						<p style="text-align: justify;"> Our image editing team at Clipping Art Image ensures that long-lasting wedding memories are captured in the best possible way. We do all kinds of services for remodelling wedding photos and after-treatment services, such as training photos, wedding photos, etc.</p>
						
						<p style="text-align: justify;"> </p>

					</div>
					<!-- End Info -->
					<!-- Followers -->
					<div class="d-flex justify-content-center align-items-center mb-7">
						<!-- Followers List -->
						<ul class="list-inline mr-2 mb-0">
							<li class="list-inline-item mr-0">
								<img class="img-fluid u-sm-avatar u-sm-avatar--bordered rounded-circle" src="assets/img/32x32/img1.jpg" alt="Image Description">
							</li>
							<li class="list-inline-item ml-offset-3 mr-0">
								<img class="img-fluid u-sm-avatar u-sm-avatar--bordered rounded-circle" src="assets/img/32x32/img3.jpg" alt="Image Description">
							</li>
							<li class="list-inline-item ml-offset-3 mr-0">
								<img class="img-fluid u-sm-avatar u-sm-avatar--bordered rounded-circle" src="assets/img/32x32/img2.jpg" alt="Image Description">
							</li>
							<li class="list-inline-item ml-offset-3 mr-0">
								<img class="img-fluid u-sm-avatar u-sm-avatar--bordered rounded-circle" src="assets/img/32x32/img4.jpg" alt="Image Description">
							</li>
							<li class="list-inline-item ml-offset-3 mr-0">
								<img class="img-fluid u-sm-avatar u-sm-avatar--bordered rounded-circle" src="assets/img/32x32/img5.jpg" alt="Image Description">
							</li>
						</ul>
						<!-- End Followers List -->
					</div>
					<!-- End Followers -->

					<!-- Avatar -->
					<img class="img-fluid u-xl-avatar u-xl-avatar--bordered rounded-circle mx-auto" src="assets/img/cli.jpg" alt="Image Description">
					<!-- End Avatar -->
				</div>

				<!-- SVG Background -->
				<figure class="position-absolute-bottom-0 z-index-minus-1">
					<svg preserveAspectRatio="none" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="100%" height="200px" viewBox="20 -20 300 100" style="margin-bottom: -8px;" xml:space="preserve">
						<path class="u-fill-white" opacity="0.4" d="M30.913,43.944c0,0,42.911-34.464,87.51-14.191c77.31,35.14,113.304-1.952,146.638-4.729
              c48.654-4.056,69.94,16.218,69.94,16.218v54.396H30.913V43.944z" />
						<path class="u-fill-white" opacity="0.4" d="M-35.667,44.628c0,0,42.91-34.463,87.51-14.191c77.31,35.141,113.304-1.952,146.639-4.729
              c48.653-4.055,69.939,16.218,69.939,16.218v54.396H-35.667V44.628z" />
						<path class="u-fill-white" opacity="0" d="M43.415,98.342c0,0,48.283-68.927,109.133-68.927c65.886,0,97.983,67.914,97.983,67.914v3.716
              H42.401L43.415,98.342z" />
						<path class="u-fill-white" d="M-34.667,62.998c0,0,56-45.667,120.316-27.839C167.484,57.842,197,41.332,232.286,30.428
              c53.07-16.399,104.047,36.903,104.047,36.903l1.333,36.667l-372-2.954L-34.667,62.998z" />
					</svg>
				</figure>
				<!-- End SVG Background Section -->
			</div>
		</div>
		<!-- End Author Section -->

		<!-- Cubeportfolio Section -->
		<div class="container u-space-2-top u-space-3-bottom u-cubeportfolio">

			<!-- Content -->
			<div class="cbp mb-7" data-controls="#cubeFilter" data-animation="quicksand" data-x-gap="16" data-y-gap="16" data-load-more-selector="#cubeLoadMore" data-load-more-action="auto" data-load-items-amount="2" data-media-queries='[
              {"width": 1500, "cols": 4},
              {"width": 1100, "cols": 4},
              {"width": 800, "cols": 3},
              {"width": 480, "cols": 2},
              {"width": 300, "cols": 1}
            ]'>
				<!-- Item -->
				<div class="cbp-item rounded abstract">
					<div class="cbp-caption">
						<a class="cbp-lightbox u-media-viewer" href="assets/img/Photoretouching/55.png" data-title="RainbowClipping">
							<img src="assets/img/Photoretouching/5.png" alt="Image Description">
							<span class="u-media-viewer__container">
								<span class="u-media-viewer__icon">
									<span class="fa fa-plus u-media-viewer__icon-inner"></span>
								</span>
							</span>
						</a>
					</div>
				</div>
				<!-- End Item -->

				<!-- Item -->
				<div class="cbp-item rounded branding">
					<div class="cbp-caption">
						<a class="cbp-lightbox u-media-viewer" href="assets/img/Photoretouching/11.png" data-title="RainbowClipping">
							<img src="assets/img/Photoretouching/1.png" alt="Image Description">
							<span class="u-media-viewer__container">
								<span class="u-media-viewer__icon">
									<span class="fa fa-plus u-media-viewer__icon-inner"></span>
								</span>
							</span>
						</a>
					</div>
				</div>
				<!-- End Item -->

				<!-- Item -->
				<div class="cbp-item rounded abstract">
					<div class="cbp-caption">
						<a class="cbp-lightbox u-media-viewer" href="assets/img/Photoretouching/22.png" data-title="RainbowClipping">
							<img src="assets/img/Photoretouching/2.png" alt="Image Description">
							<span class="u-media-viewer__container">
								<span class="u-media-viewer__icon">
									<span class="fa fa-plus u-media-viewer__icon-inner"></span>
								</span>
							</span>
						</a>
					</div>
				</div>
				<!-- End Item -->

				<!-- Item -->
				<div class="cbp-item rounded branding">
					<div class="cbp-caption">
						<a class="cbp-lightbox u-media-viewer" href="assets/img/Photoretouching/33.png" data-title="RainbowClipping">
							<img src="assets/img/Photoretouching/3.png" alt="Image Description">
							<span class="u-media-viewer__container">
								<span class="u-media-viewer__icon">
									<span class="fa fa-plus u-media-viewer__icon-inner"></span>
								</span>
							</span>
						</a>
					</div>
				</div>

				<!-- End Item -->
			</div>
			<!-- End Content -->

		</div>
		<!-- End Cubeportfolio Section -->

		<!-- Instafeed -->
		<div id="instaFeed" class="js-instagram row mx-gutters-2" data-user-id="4815936096" data-client-id="60648b9247324e87a4d7ab3aa7a58f6c" data-token="4815936096.1677ed0.1cbcaacad4de4ffa96aa15ec76e0e5ce" data-limit="4" data-template='<div class="col-md-3 mb-3 mb-sm-0"><a href="%7b%7blink%7d%7d.html" target="_blank"><img class="img-fluid w-100 rounded" src="%7b%7bimage%7d%7d.html" /></a></div>'></div>
		<!-- End Instafeed -->
		<!-- End Instagram -->

		<hr class="my-0">
	</main>

@endsection
