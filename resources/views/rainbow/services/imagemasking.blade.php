@extends('rainbow.layouts.master')

@section('title', '| Image Masking')

@section('content')
	<!-- ========== MAIN CONTENT ========== -->
	<main id="content" role="main">
		<!-- Author Section -->
		<div class="position-relative text-center u-space-3-top u-gradient-overlay-half-info-v1 u-bg-img-hero" style="background-image: url(assets/img/1920x800/img10.jpg);">
			<div class="row justify-content-md-center">
				<div class="col-md-8 col-lg-7 col-xl-11">
					<!-- Info -->
					<div class="service_color">
					<br><br>
						<h1 class="display-6 text-white font-weight-normal mb-3">Photoshop Image Masking</h1>
						<p style="text-align: justify;"> Image masking Services have many types of uses. Some advanced uses of Image Masking include background removal are discussed here for the soft edges of an image such as human hair, fur and etc.  the technique of masking images is applied with dolls, skins, etc. in addition to Clipping Path technique to remove or replace the background image. For the hard edge cutting method, the skin cutting or edge cutting technique is used. In the case, where more precise details of the soft edges cannot be obtained using only the cutting technique, the method of concealing the image is called to isolate the object from the background.</p>
						<p style="text-align: justify;"> Another method that we apply to a product or object that has a colored background close to the object’s color. To separate the product from the background with soft natural edges, we use the tablet pencil (Wacom). With this method, we can easily mix the solid area and the soft area. The mixing edge of the product depends on the volume of the airbrush and the extraction pressure. Before boarding the plane as a customer, you can try it for free.</p>
						<h1 class="display-6 text-white font-weight-normal mb-3">Image Masking Service and its Categories: </h1>
						<h3 style="text-align: justify;"> Layer Masking:</h3>
						<p style="text-align: justify;"> Layer Masking means high quality manual hand-painted Photoshop. The masks are applied directly to the layer with a smooth and refined edge with a digitizing tablet. Use it to remove the background or isolate the object. The price may vary depending on the complexity and time required.</p>
						<h3 style="text-align: justify;"> Alpha channel Masking:</h3>
						<p style="text-align: justify;"> By separating the object from the background, we save it as an alpha channel to release brightness, contrast, exposure, etc. The size of your file is lighter to download / download. A colorful background is ideal for this hidden photo.</p>
						<h3 style="text-align: justify;"> Hair and Fur masking</h3>
						<p style="text-align: justify;"> This service includes hair separation, stylus for soft edge, bottom coat, etc. If necessary, apply color, contrast, brightness, exposure correction, etc. to do the normal things. Image Masking is more effective than cutting hair.</p>
						<h3 style="text-align: justify;"> Transparent Object Masking:</h3>
						<p style="text-align: justify;"> The glass of things, the glass of the car, the ordinary glasses, the glass bottle, the water, etc. 0% -5% Opacity and light can pass. To preserve transparency and make the color or background visible through the objects, this process is applied.</p>
						<h3 style="text-align: justify;"> Translucent Object Masking:</h3>
						<p style="text-align: justify;"> This service includes sunglasses, paper, enamelled glass, fabrics, plastic bottle such as gauze, bridal veil, etc. They are transparent objects with opacity of 5% -100%. The light passes through light objects and blurry images to identify them.</p>
						<h3 style="text-align: justify;"> Object masking:</h3>
						<p style="text-align: justify;"> When hiding the object, an inappropriate portion of the image is selected using the Quick Selection tool and then removed by applying a mask using the layer mask. Hide the object is very useful to eliminate distortions of the image.</p>
						<h3 style="text-align: justify;"> Color Masking:</h3>
						<p style="text-align: justify;"> To avoid the hollow line, cut the image on the edge or outside the outline instead of 1 or 2 pixels on the inside is a coloured mask. Designers then apply different effects such as color correction, exposure, smoothing, etc.</p>
						<h3 style="text-align: justify;"> Our specialty</h3>
						<p style="text-align: justify;"> Our expert graphic designers use a Pen Tablet palette for skin, hair, skin and soft-edged clothing for changing color, brightness, contrast, image exposure, etc. We work in famous global magazines, fashion houses, e-commerce sites, advertising agencies, and product and model photographers, among others, to provide a photo masking service with pen tablet.</p>
						<h3 style="text-align: justify;"> </h3>
						<p style="text-align: justify;"> </p>

					</div>
					<!-- End Info -->

					<!-- Followers -->
					<div class="d-flex justify-content-center align-items-center mb-7">
						<!-- Followers List -->
						<ul class="list-inline mr-2 mb-0">
							<li class="list-inline-item mr-0">
								<img class="img-fluid u-sm-avatar u-sm-avatar--bordered rounded-circle" src="assets/img/32x32/img1.jpg" alt="Image Description">
							</li>
							<li class="list-inline-item ml-offset-3 mr-0">
								<img class="img-fluid u-sm-avatar u-sm-avatar--bordered rounded-circle" src="assets/img/32x32/img3.jpg" alt="Image Description">
							</li>
							<li class="list-inline-item ml-offset-3 mr-0">
								<img class="img-fluid u-sm-avatar u-sm-avatar--bordered rounded-circle" src="assets/img/32x32/img2.jpg" alt="Image Description">
							</li>
							<li class="list-inline-item ml-offset-3 mr-0">
								<img class="img-fluid u-sm-avatar u-sm-avatar--bordered rounded-circle" src="assets/img/32x32/img4.jpg" alt="Image Description">
							</li>
							<li class="list-inline-item ml-offset-3 mr-0">
								<img class="img-fluid u-sm-avatar u-sm-avatar--bordered rounded-circle" src="assets/img/32x32/img5.jpg" alt="Image Description">
							</li>
						</ul>
						<!-- End Followers List -->
					</div>
					<!-- End Followers -->

					<!-- Avatar -->
					<img class="img-fluid u-xl-avatar u-xl-avatar--bordered rounded-circle mx-auto" src="assets/img/cli.jpg" alt="Image Description">
					<!-- End Avatar -->
				</div>

				<!-- SVG Background -->
				<figure class="position-absolute-bottom-0 z-index-minus-1">
					<svg preserveAspectRatio="none" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="100%" height="200px" viewBox="20 -20 300 100" style="margin-bottom: -8px;" xml:space="preserve">
						<path class="u-fill-white" opacity="0.4" d="M30.913,43.944c0,0,42.911-34.464,87.51-14.191c77.31,35.14,113.304-1.952,146.638-4.729
              c48.654-4.056,69.94,16.218,69.94,16.218v54.396H30.913V43.944z" />
						<path class="u-fill-white" opacity="0.4" d="M-35.667,44.628c0,0,42.91-34.463,87.51-14.191c77.31,35.141,113.304-1.952,146.639-4.729
              c48.653-4.055,69.939,16.218,69.939,16.218v54.396H-35.667V44.628z" />
						<path class="u-fill-white" opacity="0" d="M43.415,98.342c0,0,48.283-68.927,109.133-68.927c65.886,0,97.983,67.914,97.983,67.914v3.716
              H42.401L43.415,98.342z" />
						<path class="u-fill-white" d="M-34.667,62.998c0,0,56-45.667,120.316-27.839C167.484,57.842,197,41.332,232.286,30.428
              c53.07-16.399,104.047,36.903,104.047,36.903l1.333,36.667l-372-2.954L-34.667,62.998z" />
					</svg>
				</figure>
				<!-- End SVG Background Section -->
			</div>
		</div>
		<!-- End Author Section -->

		<!-- Cubeportfolio Section -->
		<div class="container u-space-2-top u-space-3-bottom u-cubeportfolio">

			<!-- Content -->
			<div class="cbp mb-7" data-controls="#cubeFilter" data-animation="quicksand" data-x-gap="16" data-y-gap="16" data-load-more-selector="#cubeLoadMore" data-load-more-action="auto" data-load-items-amount="2" data-media-queries='[
              {"width": 1500, "cols": 4},
              {"width": 1100, "cols": 4},
              {"width": 800, "cols": 3},
              {"width": 480, "cols": 2},
              {"width": 300, "cols": 1}
            ]'>
				<!-- Item -->
				<div class="cbp-item rounded abstract">
					<div class="cbp-caption">
						<a class="cbp-lightbox u-media-viewer" href="assets/img/ImageMasking/22.png" data-title="RainbowClipping">
							<img src="assets/img/ImageMasking/2.png" alt="Image Description">
							<span class="u-media-viewer__container">
								<span class="u-media-viewer__icon">
									<span class="fa fa-plus u-media-viewer__icon-inner"></span>
								</span>
							</span>
						</a>
					</div>
				</div>
				<!-- End Item -->
                
                <!-- Item -->
				<div class="cbp-item rounded abstract">
					<div class="cbp-caption">
						<a class="cbp-lightbox u-media-viewer" href="assets/img/ImageMasking/44.png" data-title="RainbowClipping">
							<img src="assets/img/ImageMasking/4.png" alt="Image Description">
							<span class="u-media-viewer__container">
								<span class="u-media-viewer__icon">
									<span class="fa fa-plus u-media-viewer__icon-inner"></span>
								</span>
							</span>
						</a>
					</div>
				</div>
				<!-- End Item -->
                
				<!-- Item -->
				<div class="cbp-item rounded branding">
					<div class="cbp-caption">
						<a class="cbp-lightbox u-media-viewer" href="assets/img/ImageMasking/11.png" data-title="RainbowClipping">
							<img src="assets/img/ImageMasking/1.png" alt="Image Description">
							<span class="u-media-viewer__container">
								<span class="u-media-viewer__icon">
									<span class="fa fa-plus u-media-viewer__icon-inner"></span>
								</span>
							</span>
						</a>
					</div>
				</div>
				<!-- End Item -->

				<!-- Item -->
				<div class="cbp-item rounded abstract">
					<div class="cbp-caption">
						<a class="cbp-lightbox u-media-viewer" href="assets/img/ImageMasking/33.png" data-title="RainbowClipping">
							<img src="assets/img/ImageMasking/3.png" alt="Image Description">
							<span class="u-media-viewer__container">
								<span class="u-media-viewer__icon">
									<span class="fa fa-plus u-media-viewer__icon-inner"></span>
								</span>
							</span>
						</a>
					</div>
				</div>
				<!-- End Item -->

				<!-- Item -->
				<!-- End Item -->
			</div>
			<!-- End Content -->

		</div>
		<!-- End Cubeportfolio Section -->

		<!-- Instafeed -->
		<div id="instaFeed" class="js-instagram row mx-gutters-2" data-user-id="4815936096" data-client-id="60648b9247324e87a4d7ab3aa7a58f6c" data-token="4815936096.1677ed0.1cbcaacad4de4ffa96aa15ec76e0e5ce" data-limit="4" data-template='<div class="col-md-3 mb-3 mb-sm-0"><a href="%7b%7blink%7d%7d.html" target="_blank"><img class="img-fluid w-100 rounded" src="%7b%7bimage%7d%7d.html" /></a></div>'></div>
		<!-- End Instafeed -->
		<!-- End Instagram -->

		<hr class="my-0">
	</main>

@endsection
