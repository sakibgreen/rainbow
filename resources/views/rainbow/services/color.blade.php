@extends('rainbow.layouts.master')

@section('title', '| Color')

@section('content')
	<main id="content" role="main">
		<!-- Author Section -->
		<div class="position-relative text-center u-space-3-top u-gradient-overlay-half-info-v1 u-bg-img-hero" style="background-image: url(assets/img/1920x800/img10.jpg);">
			<div class="row justify-content-md-center">
				<div class="col-md-8 col-lg-7 col-xl-11">
					<!-- Info -->
					<div class="service_color">
					<br><br>
						<h1 class="display-6 text-white font-weight-normal mb-3 text-uppercase">Color correction service</h1>
						<p style="text-align: justify;"> Color correction is a process to change the color of a digital image, digital video or any other form of digital media. Many times, certain colors of objects in an image, such as clothing or accessories, must be changed to different colors, moderated to correct colors or re-colored to restore balance. This is achieved with perfection using professional Photoshop services. The primary and main stage of the reissue is a color correction. It has many tools that aim to adjust the quality of the image and eliminate all possible negatives to achieve a desirable image adjustment. Photographers use it to eliminate the limits of images and improve their structure. First, it refers to the preparation of the light and the color of the image to achieve almost perfect results. These services are an important tool to keep the air insured and skillful style, not only the illustrator but also the original message.</p>
						
						<h2 style="text-align: justify;">Our services include:</h2>
						<ul>
							<li><p style="text-align: justify;">White balance </p></li>
							<li><p style="text-align: justify;">Adjust the contrast </p></li>
							<li><p style="text-align: justify;">Correct exposure </p></li>
							<li><p style="text-align: justify;">Cultivation </p></li>
							<li><p style="text-align: justify;">straightening </p></li>
							<li><p style="text-align: justify;">Sharpen </p></li>
							<li><p style="text-align: justify;">Noise reduction </p></li>
							<li><p style="text-align: justify;">Match your style </p></li>

						</ul>
						
						<h1 class="display-6 text-white font-weight-normal mb-3 text-uppercase">Why use the patch or color service?</h1>
						<p style="text-align: justify;">There are many potential uses for color correction services. However, the most common example is an e-commerce site that needs to display a product in different colors. For example, it is necessary to show a pole of different colors to imagine what best suits the consumer. </p>
						<p style="text-align: justify;">The model does not need to wear a shirt of any color during the photo shoot, which would make it long, expensive and annoying. The blue T-Shirta model can be redesigned using the color correction service for other colors such as green, red, violet, etc. This option is more cost-effective compared to the use of a model and the use of clothing in all available colored garments. Therefore, it saves costs and time without compromising the display of multiple products for the benefit of consumers. </p>

						<h2 class="display-6 text-white font-weight-normal mb-3 text-uppercase">Why should I demonstrate color correction in our photo color correction company?</h2>
						<h3 style="text-align: justify;">The price starts at $ 0.25</h3>
						<ul>
							<li><p style="text-align: justify;">Moderate color correction costs: starting at $ 0.25 per image </p></li>
							<li><p style="text-align: justify;">Professional adjustment numbers are very low, which makes the service attractive to most customers. </p></li>
							<li><p style="text-align: justify;">Fluid color correction </p></li>
						</ul>

						<h1 class="display-6 text-white font-weight-normal mb-3 text-uppercase">Fix the color in the instant start with Clipping Art Image</h1>
						<p style="text-align: justify;">On the path of creative cutting, we offer one of the most professional graphic design teams to ensure the best color correction service to repair or change colors in a digital image. Here are the many advantages of our color correction services: </p>
						<ul>
							<li><p style="text-align: justify;"> Low service cost</p></li>
							<li><p style="text-align: justify;"> Different ranges of service</p></li>	
							<li><p style="text-align: justify;"> Professional team of graphic designers</p></li>	
							<li><p style="text-align: justify;"> Quality and satisfaction guaranteed</p></li>	
							<li><p style="text-align: justify;"> Instant connection</p></li>	
						</ul>
						<p style="text-align: justify;">With the use of high-tech tools and the availability of a variety of digital image processing technologies, the color correction services at Clipping Art Image will ensure that your image fits your needs. </p>
					</div>
										
					<!-- End Info -->

					<!-- Followers -->
					<div class="d-flex justify-content-center align-items-center mb-7">
						<!-- Followers List -->
						<ul class="list-inline mr-2 mb-0">
							<li class="list-inline-item mr-0">
								<img class="img-fluid u-sm-avatar u-sm-avatar--bordered rounded-circle" src="assets/img/32x32/img1.jpg" alt="Image Description">
							</li>
							<li class="list-inline-item ml-offset-3 mr-0">
								<img class="img-fluid u-sm-avatar u-sm-avatar--bordered rounded-circle" src="assets/img/32x32/img3.jpg" alt="Image Description">
							</li>
							<li class="list-inline-item ml-offset-3 mr-0">
								<img class="img-fluid u-sm-avatar u-sm-avatar--bordered rounded-circle" src="assets/img/32x32/img2.jpg" alt="Image Description">
							</li>
							<li class="list-inline-item ml-offset-3 mr-0">
								<img class="img-fluid u-sm-avatar u-sm-avatar--bordered rounded-circle" src="assets/img/32x32/img4.jpg" alt="Image Description">
							</li>
							<li class="list-inline-item ml-offset-3 mr-0">
								<img class="img-fluid u-sm-avatar u-sm-avatar--bordered rounded-circle" src="assets/img/32x32/img5.jpg" alt="Image Description">
							</li>
						</ul>
						<!-- End Followers List -->
					</div>
					<!-- End Followers -->

					<!-- Avatar -->
					<img class="img-fluid u-xl-avatar u-xl-avatar--bordered rounded-circle mx-auto" src="assets/img/cli.jpg" alt="Image Description">
					<!-- End Avatar -->
				</div>

				<!-- SVG Background -->
				<figure class="position-absolute-bottom-0 z-index-minus-1">
					<svg preserveAspectRatio="none" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="100%" height="200px" viewBox="20 -20 300 100" style="margin-bottom: -8px;" xml:space="preserve">
						<path class="u-fill-white" opacity="0.4" d="M30.913,43.944c0,0,42.911-34.464,87.51-14.191c77.31,35.14,113.304-1.952,146.638-4.729
              c48.654-4.056,69.94,16.218,69.94,16.218v54.396H30.913V43.944z" />
						<path class="u-fill-white" opacity="0.4" d="M-35.667,44.628c0,0,42.91-34.463,87.51-14.191c77.31,35.141,113.304-1.952,146.639-4.729
              c48.653-4.055,69.939,16.218,69.939,16.218v54.396H-35.667V44.628z" />
						<path class="u-fill-white" opacity="0" d="M43.415,98.342c0,0,48.283-68.927,109.133-68.927c65.886,0,97.983,67.914,97.983,67.914v3.716
              H42.401L43.415,98.342z" />
						<path class="u-fill-white" d="M-34.667,62.998c0,0,56-45.667,120.316-27.839C167.484,57.842,197,41.332,232.286,30.428
              c53.07-16.399,104.047,36.903,104.047,36.903l1.333,36.667l-372-2.954L-34.667,62.998z" />
					</svg>
				</figure>
				<!-- End SVG Background Section -->
			</div>
		</div>
		<!-- End Author Section -->

		<!-- Cubeportfolio Section -->
		<div class="container u-space-2-top u-space-3-bottom u-cubeportfolio">

			<!-- Content -->
			<div class="cbp mb-7" data-controls="#cubeFilter" data-animation="quicksand" data-x-gap="16" data-y-gap="16" data-load-more-selector="#cubeLoadMore" data-load-more-action="auto" data-load-items-amount="2" data-media-queries='[
              {"width": 1500, "cols": 4},
              {"width": 1100, "cols": 4},
              {"width": 800, "cols": 3},
              {"width": 480, "cols": 2},
              {"width": 300, "cols": 1}
            ]'>
				<!-- Item -->
				<div class="cbp-item rounded abstract">
					<div class="cbp-caption">
						<a class="cbp-lightbox u-media-viewer" href="assets/img/1920x1920/img2.jpg" data-title="RainbowClipping">
							<img src="assets/img/1920x1920/1.jpg" alt="Image Description">
							<span class="u-media-viewer__container">
								<span class="u-media-viewer__icon">
									<span class="fa fa-plus u-media-viewer__icon-inner"></span>
								</span>
							</span>
						</a>
					</div>
				</div>
				<!-- End Item -->

				<!-- Item -->
				<div class="cbp-item rounded branding">
					<div class="cbp-caption">
						<a class="cbp-lightbox u-media-viewer" href="assets/img/Colourcorrection/11.png" data-title="RainbowClipping">
							<img src="assets/img/Colourcorrection/1.png" alt="Image Description">
							<span class="u-media-viewer__container">
								<span class="u-media-viewer__icon">
									<span class="fa fa-plus u-media-viewer__icon-inner"></span>
								</span>
							</span>
						</a>
					</div>
				</div>
				<!-- End Item -->

				<!-- Item -->
				<div class="cbp-item rounded abstract">
					<div class="cbp-caption">
						<a class="cbp-lightbox u-media-viewer" href="assets/img/Colourcorrection/22.png" data-title="RainbowClipping">
							<img src="assets/img/Colourcorrection/2.png" alt="Image Description">
							<span class="u-media-viewer__container">
								<span class="u-media-viewer__icon">
									<span class="fa fa-plus u-media-viewer__icon-inner"></span>
								</span>
							</span>
						</a>
					</div>
				</div>
				<!-- End Item -->

				<!-- Item -->
				<div class="cbp-item rounded branding">
					<div class="cbp-caption">
						<a class="cbp-lightbox u-media-viewer" href="assets/img/Colourcorrection/33.png" data-title="RainbowClipping">
							<img src="assets/img/Colourcorrection/3.png" alt="Image Description">
							<span class="u-media-viewer__container">
								<span class="u-media-viewer__icon">
									<span class="fa fa-plus u-media-viewer__icon-inner"></span>
								</span>
							</span>
						</a>
					</div>
				</div>

				<!-- End Item -->
			</div>
			<!-- End Content -->

		</div>
		<!-- End Cubeportfolio Section -->

		<!-- Instafeed -->
		<div id="instaFeed" class="js-instagram row mx-gutters-2" data-user-id="4815936096" data-client-id="60648b9247324e87a4d7ab3aa7a58f6c" data-token="4815936096.1677ed0.1cbcaacad4de4ffa96aa15ec76e0e5ce" data-limit="4" data-template='<div class="col-md-3 mb-3 mb-sm-0"><a href="%7b%7blink%7d%7d.html" target="_blank"><img class="img-fluid w-100 rounded" src="%7b%7bimage%7d%7d.html" /></a></div>'></div>
		<!-- End Instafeed -->
		<!-- End Instagram -->

		<hr class="my-0">
	</main>

@endsection 