@extends('rainbow.layouts.master')

@section('title', '| Free Trail')

@section('content')
	<main id="content" role="main">
		<div class="container u-space-3">

			<div class="container">
				<div class="main_title">
					<h2>Our Free Trial Service</h2>
					<p class="job">The Best Photo Editing Service Company?
						Don’t Believe Any Commercial Brags. You Be The Judge..</p>
				</div>
				<div class="work_inner row">
					<div class="offset col-lg-2"></div>
					<div class="col-lg-8">
						<form class="form-horizontal mt-5" action="{{route('freetrials.update',['id'=>$freetrials->id])}}" method="post" enctype="multipart/form-data">
							{{csrf_field()}}
							<div class="form-row">
								<div class="form-group col-md-6">
									<label for="inputEmail4">Your Name:</label>
									<input type="text" class="form-control" id="name" name="name" placeholder="Enter your full name" required="" value="{{$freetrials->name}}">
								</div>
								<div class="form-group col-md-6">
									<label for="inputPassword4">Company Name: (Optional)</label>
									<input type="text" class="form-control" id="company" name="company" placeholder="Enter your company name" required="" value="{{$freetrials->company}}"> 
								</div>
								
								<div class="form-group col-md-6">
									<label for="inputPassword4">Your Phone:</label>
									<input type="number" class="form-control" id="phone" name="phone" placeholder="Enter your phone number" required="" value="{{$freetrials->phone}}">
								</div>
								<div class="form-group col-md-6">
									<label for="inputAddress">Address:</label>
									<input type="text" class="form-control" id="address" name="address" placeholder="Enter Your Address" required="" value="{{$freetrials->address}}">
								</div>
								<div class="form-group col-md-6">
									<label for="inputAddress"> Web Address:</label>
									<input type="text" class="form-control" id="website" name="website" placeholder="Enter Your Address" required="" value="{{$freetrials->website}}">
								</div>

								<div class="form-group col-md-12">
									<label for="inputAddress2">Job Title:</label>
									<p class="job">Please mention the job category in the box below one/multiple from the below job list.</p>
									<div class="form-row">
										<div class="form-group col-md-6">
											<div class="form-check">
												<input class="form-check-input" type="checkbox" id="gridCheck" name="job_title" value="Clipping path">
												<label class="form-check-label checkfont" for="gridCheck">
													Clipping path
												</label>
											</div>
											<div class="form-check">
												<input class="form-check-input" type="checkbox" id="gridCheck" name="job_title" value="Image Masking">
												<label class="form-check-label checkfont" for="gridCheck">
													Image Masking
												</label>
											</div>
											<div class="form-check">
												<input class="form-check-input" type="checkbox" id="gridCheck" name="job_title" value="Photo retouching">
												<label class="form-check-label checkfont" for="gridCheck">
													Photo retouching
												</label>
											</div>
											<div class="form-check">
												<input class="form-check-input" type="checkbox" id="gridCheck" name="job_title" value="Shadow creations">
												<label class="form-check-label checkfont" for="gridCheck">
													Shadow creations
												</label>
											</div>
											<div class="form-check">
												<input class="form-check-input" type="checkbox" id="gridCheck" name="job_title" value="Ghost mannequin">
												<label class="form-check-label checkfont" for="gridCheck">
													Ghost mannequin
												</label>
											</div>
											<div class="form-check">
												<input class="form-check-input" type="checkbox" id="gridCheck" name="job_title" value="Wedding Photo Retouching">
												<label class="form-check-label checkfont" for="gridCheck">
													Wedding Photo Retouching
												</label>
											</div>
										</div>
										<div class="form-group col-md-6">
											<div class="form-check">
												<input class="form-check-input" type="checkbox" id="gridCheck" name="job_title" value="Product Photo Editing">
												<label class="form-check-label checkfont" for="gridCheck">
													Product Photo Editing
												</label>
											</div>
											<div class="form-check">
												<input class="form-check-input" type="checkbox" id="gridCheck" name="job_title" value="Real estate photo editing">
												<label class="form-check-label checkfont" for="gridCheck">
													Real estate photo editing
												</label>
											</div>
											<div class="form-check">
												<input class="form-check-input" type="checkbox" id="gridCheck" name="job_title" value="Photo resizing/cropping">
												<label class="form-check-label checkfont" for="gridCheck">
													Photo resizing/cropping
												</label>
											</div>
											<div class="form-check">
												<input class="form-check-input" type="checkbox" id="gridCheck" name="job_title" value="Colour correction">
												<label class="form-check-label checkfont" for="gridCheck">
													Colour correction
												</label>
											</div>
											<div class="form-check">
												<input class="form-check-input" type="checkbox" id="gridCheck" name="job_title" value="Photo restoration">
												<label class="form-check-label checkfont" for="gridCheck">
													Photo restoration
												</label>
											</div>
											<div class="form-check">
												<input class="form-check-input" type="checkbox" id="gridCheck" name="job_title" value="Vector Illustration & Conversion">
												<label class="form-check-label checkfont" for="gridCheck">
													Vector Illustration & Conversion
												</label>
											</div>
										</div>
									</div>
								</div>	
								<div class="form-group col-md-12">
									<label for="file_images">File Upload (Upload limit - 120MB/image, 2 files only):</label>
									<input type="file" id="images" name="images[]" multiple required="required" value="{{$freetrials->images}}"/>
								</div>
								<form action="send" method="post">
									{{csrf_field()}}
									<div class="form-group col-md-12">
										<label for="inputAddress"> Email Address:</label>
										<input type="text" class="form-control" id="to" name="to" placeholder="Email" required="" value="{{$freetrials->to}}">
									</div>

									<div class="form-group col-md-12">
										<div class="input-group-prepend">
											<span class="input-group-text">Message:</span>
											<textarea name="message" id="message" placeholder="" required="" value="{{$freetrials->message}}"></textarea>
										</div>
									</div>
								</form>
							
							<input type="submit" value="SUBMIT">
						</form>
					</div> 
					<div class="offset col-lg-2"></div>
 
				</div>
				<!--work inner area-->
			</div>
		</div>
	</main>

	
@endsection 