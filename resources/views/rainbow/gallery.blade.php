@extends('rainbow.layouts.master')

@section('title', '| gallery')

@section('content')
	<main id="content" role="main">
		<div class="position-relative text-center u-space-3-top u-gradient-overlay-half-info-v1 u-bg-img-hero" style="background-image: url(assets/img/1920x800/img10.jpg);">
			<div class="row justify-content-md-center">
				<div class="col-md-8 col-lg-7 col-xl-11">
					<div class="service_color"> <br> <br> <br>
						<h1 class="display-6 text-white font-weight-normal mb-3 text-uppercase">our gallery</h1>
						<p>We have been providing various image manipulation and video editing services for the last 30 years all over the world. Basically, we do quality works on Adobe Photoshop based clipping path services for image manipulation. But we also have expertise knowledge on photoshop image masking, retouching, color restoration, post production, pre-press work, web design and more. Have a look at the portfolio of our work samples. (Please click on an image to see “before” and “after” versions of the same image)</p>
					</div>

					<div class="d-flex justify-content-center align-items-center mb-7">
						<ul class="list-inline mr-2 mb-0">
							<li class="list-inline-item mr-0">
								<img class="img-fluid u-sm-avatar u-sm-avatar--bordered rounded-circle" src="assets/img/32x32/img1.jpg" alt="Image Description">
							</li>
							<li class="list-inline-item ml-offset-3 mr-0">
								<img class="img-fluid u-sm-avatar u-sm-avatar--bordered rounded-circle" src="assets/img/32x32/img3.jpg" alt="Image Description">
							</li>
							<li class="list-inline-item ml-offset-3 mr-0">
								<img class="img-fluid u-sm-avatar u-sm-avatar--bordered rounded-circle" src="assets/img/32x32/img2.jpg" alt="Image Description">
							</li>
							<li class="list-inline-item ml-offset-3 mr-0">
								<img class="img-fluid u-sm-avatar u-sm-avatar--bordered rounded-circle" src="assets/img/32x32/img4.jpg" alt="Image Description">
							</li>
							<li class="list-inline-item ml-offset-3 mr-0">
								<img class="img-fluid u-sm-avatar u-sm-avatar--bordered rounded-circle" src="assets/img/32x32/img5.jpg" alt="Image Description">
							</li>
						</ul>
					</div>

					<img class="img-fluid u-xl-avatar u-xl-avatar--bordered rounded-circle mx-auto" src="assets/img/gallerylogo.png" alt="Image Description">
				</div>

				<figure class="position-absolute-bottom-0 z-index-minus-1">
					<svg preserveAspectRatio="none" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="100%" height="200px" viewBox="20 -20 300 100" style="margin-bottom: -8px;" xml:space="preserve">
						<path class="u-fill-white" opacity="0.4" d="M30.913,43.944c0,0,42.911-34.464,87.51-14.191c77.31,35.14,113.304-1.952,146.638-4.729
              c48.654-4.056,69.94,16.218,69.94,16.218v54.396H30.913V43.944z" />
						<path class="u-fill-white" opacity="0.4" d="M-35.667,44.628c0,0,42.91-34.463,87.51-14.191c77.31,35.141,113.304-1.952,146.639-4.729
              c48.653-4.055,69.939,16.218,69.939,16.218v54.396H-35.667V44.628z" />
						<path class="u-fill-white" opacity="0" d="M43.415,98.342c0,0,48.283-68.927,109.133-68.927c65.886,0,97.983,67.914,97.983,67.914v3.716
              H42.401L43.415,98.342z" />
						<path class="u-fill-white" d="M-34.667,62.998c0,0,56-45.667,120.316-27.839C167.484,57.842,197,41.332,232.286,30.428
              c53.07-16.399,104.047,36.903,104.047,36.903l1.333,36.667l-372-2.954L-34.667,62.998z" />
					</svg>
				</figure>
			</div>
		</div>

		<div class="container u-space-2-top u-space-3-bottom u-cubeportfolio">
			<h3>Clipping Path & Background Removal</h3>
			<div class="cbp mb-7" data-controls="#cubeFilter" data-animation="quicksand" data-x-gap="16" data-y-gap="16" data-load-more-selector="#cubeLoadMore" data-load-more-action="auto" data-load-items-amount="2" data-media-queries='[
              {"width": 1500, "cols": 4},
              {"width": 1100, "cols": 4},
              {"width": 800, "cols": 3},
              {"width": 480, "cols": 2},
			  {"width": 300, "cols": 1}]'>
			  
				<div class="cbp-item rounded abstract">
					<div class="cbp-caption">
						<a class="cbp-lightbox u-media-viewer" href="assets/img/ProductPhotoEditing/11.jpg" data-title="RainbowClipping">
							<img src="assets/img/ProductPhotoEditing/1.jpg" alt="Image Description">
							<span class="u-media-viewer__container">
								<span class="u-media-viewer__icon">
									<span class="fa fa-plus u-media-viewer__icon-inner"></span>
								</span>
							</span>
						</a>
					</div>
				</div>

				<div class="cbp-item rounded branding">
					<div class="cbp-caption">
						<a class="cbp-lightbox u-media-viewer" href="assets/img/ProductPhotoEditing/22.jpg" data-title="RainbowClipping">
							<img src="assets/img/ProductPhotoEditing/2.jpg" alt="Image Description">
							<span class="u-media-viewer__container">
								<span class="u-media-viewer__icon">
									<span class="fa fa-plus u-media-viewer__icon-inner"></span>
								</span>
							</span>
						</a>
					</div>
				</div>

				<div class="cbp-item rounded abstract">
					<div class="cbp-caption">
						<a class="cbp-lightbox u-media-viewer" href="assets/img/ProductPhotoEditing/33.jpg" data-title="RainbowClipping">
							<img src="assets/img/ProductPhotoEditing/3.jpg" alt="Image Description">
							<span class="u-media-viewer__container">
								<span class="u-media-viewer__icon">
									<span class="fa fa-plus u-media-viewer__icon-inner"></span>
								</span>
							</span>
						</a>
					</div>
				</div>

				<div class="cbp-item rounded branding">
					<div class="cbp-caption">
						<a class="cbp-lightbox u-media-viewer" href="assets/img/ProductPhotoEditing/44.jpg" data-title="RainbowClipping">
							<img src="assets/img/ProductPhotoEditing/4.jpg" alt="Image Description">
							<span class="u-media-viewer__container">
								<span class="u-media-viewer__icon">
									<span class="fa fa-plus u-media-viewer__icon-inner"></span>
								</span>
							</span>
						</a>
					</div>
				</div>
			</div>
		</div>

		<div class="container u-space-2-top u-space-3-bottom u-cubeportfolio">
			<h3> Image Masking </h3>
			<div class="cbp mb-7" data-controls="#cubeFilter" data-animation="quicksand" data-x-gap="16" data-y-gap="16" data-load-more-selector="#cubeLoadMore" data-load-more-action="auto" data-load-items-amount="2" data-media-queries='[
              {"width": 1500, "cols": 4},
              {"width": 1100, "cols": 4},
              {"width": 800, "cols": 3},
              {"width": 480, "cols": 2},
			  {"width": 300, "cols": 1}]'>
			  
			  <div class="cbp-item rounded abstract">
					<div class="cbp-caption">
						<a class="cbp-lightbox u-media-viewer" href="assets/img/WeddingPhotoRetouching/44.png" data-title="RainbowClipping">
							<img src="assets/img/WeddingPhotoRetouching/4.png" alt="Image Description">
							<span class="u-media-viewer__container">
								<span class="u-media-viewer__icon">
									<span class="fa fa-plus u-media-viewer__icon-inner"></span>
								</span>
							</span>
						</a>
					</div>
				</div>

				<div class="cbp-item rounded branding">
					<div class="cbp-caption">
						<a class="cbp-lightbox u-media-viewer" href="assets/img/WeddingPhotoRetouching/11.png" data-title="RainbowClipping">
							<img src="assets/img/WeddingPhotoRetouching/1.png" alt="Image Description">
							<span class="u-media-viewer__container">
								<span class="u-media-viewer__icon">
									<span class="fa fa-plus u-media-viewer__icon-inner"></span>
								</span>
							</span>
						</a>
					</div>
				</div>

				<div class="cbp-item rounded abstract">
					<div class="cbp-caption">
						<a class="cbp-lightbox u-media-viewer" href="assets/img/WeddingPhotoRetouching/22.png" data-title="RainbowClipping">
							<img src="assets/img/WeddingPhotoRetouching/2.png" alt="Image Description">
							<span class="u-media-viewer__container">
								<span class="u-media-viewer__icon">
									<span class="fa fa-plus u-media-viewer__icon-inner"></span>
								</span>
							</span>
						</a>
					</div>
				</div>

				<div class="cbp-item rounded branding">
					<div class="cbp-caption">
						<a class="cbp-lightbox u-media-viewer" href="assets/img/WeddingPhotoRetouching/33.png" data-title="RainbowClipping">
							<img src="assets/img/WeddingPhotoRetouching/3.png" alt="Image Description">
							<span class="u-media-viewer__container">
								<span class="u-media-viewer__icon">
									<span class="fa fa-plus u-media-viewer__icon-inner"></span>
								</span>
							</span>
						</a>
					</div>
				</div>
			</div>
		</div>

		<div class="container u-space-2-top u-space-3-bottom u-cubeportfolio">
			<h3>  Photo Retouching </h3>
			<div class="cbp mb-7" data-controls="#cubeFilter" data-animation="quicksand" data-x-gap="16" data-y-gap="16" data-load-more-selector="#cubeLoadMore" data-load-more-action="auto" data-load-items-amount="2" data-media-queries='[
              {"width": 1500, "cols": 4},
              {"width": 1100, "cols": 4},
              {"width": 800, "cols": 3},
              {"width": 480, "cols": 2},
			  {"width": 300, "cols": 1}]'>
			  
			  <div class="cbp-item rounded abstract">
					<div class="cbp-caption">
						<a class="cbp-lightbox u-media-viewer" href="assets/img/Photoretouching/55.png" data-title="RainbowClipping">
							<img src="assets/img/Photoretouching/5.png" alt="Image Description">
							<span class="u-media-viewer__container">
								<span class="u-media-viewer__icon">
									<span class="fa fa-plus u-media-viewer__icon-inner"></span>
								</span>
							</span>
						</a>
					</div>
				</div>

				<div class="cbp-item rounded branding">
					<div class="cbp-caption">
						<a class="cbp-lightbox u-media-viewer" href="assets/img/Photoretouching/11.png" data-title="RainbowClipping">
							<img src="assets/img/Photoretouching/1.png" alt="Image Description">
							<span class="u-media-viewer__container">
								<span class="u-media-viewer__icon">
									<span class="fa fa-plus u-media-viewer__icon-inner"></span>
								</span>
							</span>
						</a>
					</div>
				</div>

				<div class="cbp-item rounded abstract">
					<div class="cbp-caption">
						<a class="cbp-lightbox u-media-viewer" href="assets/img/Photoretouching/22.png" data-title="RainbowClipping">
							<img src="assets/img/Photoretouching/2.png" alt="Image Description">
							<span class="u-media-viewer__container">
								<span class="u-media-viewer__icon">
									<span class="fa fa-plus u-media-viewer__icon-inner"></span>
								</span>
							</span>
						</a>
					</div>
				</div>

				<div class="cbp-item rounded branding">
					<div class="cbp-caption">
						<a class="cbp-lightbox u-media-viewer" href="assets/img/Photoretouching/33.png" data-title="RainbowClipping">
							<img src="assets/img/Photoretouching/3.png" alt="Image Description">
							<span class="u-media-viewer__container">
								<span class="u-media-viewer__icon">
									<span class="fa fa-plus u-media-viewer__icon-inner"></span>
								</span>
							</span>
						</a>
					</div>
				</div>
			</div>
		</div>

		<div class="container u-space-2-top u-space-3-bottom u-cubeportfolio">
			<h3>  Image Manipulation/Ghost Mannequin </h3>
			<div class="cbp mb-7" data-controls="#cubeFilter" data-animation="quicksand" data-x-gap="16" data-y-gap="16" data-load-more-selector="#cubeLoadMore" data-load-more-action="auto" data-load-items-amount="2" data-media-queries='[
              {"width": 1500, "cols": 4},
              {"width": 1100, "cols": 4},
              {"width": 800, "cols": 3},
              {"width": 480, "cols": 2},
			  {"width": 300, "cols": 1}]'>
			  
			  	<div class="cbp-item rounded abstract">
					<div class="cbp-caption">
						<a class="cbp-lightbox u-media-viewer" href="assets/img/Goasmannequeen/11.png" data-title="RainbowClipping">
							<img src="assets/img/Goasmannequeen/1.png" alt="Image Description">
							<span class="u-media-viewer__container">
								<span class="u-media-viewer__icon">
									<span class="fa fa-plus u-media-viewer__icon-inner"></span>
								</span>
							</span>
						</a>
					</div>
				</div>

				<div class="cbp-item rounded branding">
					<div class="cbp-caption">
						<a class="cbp-lightbox u-media-viewer" href="assets/img/Goasmannequeen/22.png" data-title="RainbowClipping">
							<img src="assets/img/Goasmannequeen/2.png" alt="Image Description">
							<span class="u-media-viewer__container">
								<span class="u-media-viewer__icon">
									<span class="fa fa-plus u-media-viewer__icon-inner"></span>
								</span>
							</span>
						</a>
					</div>
				</div>

				<div class="cbp-item rounded abstract">
					<div class="cbp-caption">
						<a class="cbp-lightbox u-media-viewer" href="assets/img/Goasmannequeen/33.png" data-title="RainbowClipping">
							<img src="assets/img/Goasmannequeen/3.png" alt="Image Description">
							<span class="u-media-viewer__container">
								<span class="u-media-viewer__icon">
									<span class="fa fa-plus u-media-viewer__icon-inner"></span>
								</span>
							</span>
						</a>
					</div>
				</div>

				<div class="cbp-item rounded branding">
					<div class="cbp-caption">
						<a class="cbp-lightbox u-media-viewer" href="assets/img/Goasmannequeen/44.png" data-title="RainbowClipping">
							<img src="assets/img/Goasmannequeen/4.png" alt="Image Description">
							<span class="u-media-viewer__container">
								<span class="u-media-viewer__icon">
									<span class="fa fa-plus u-media-viewer__icon-inner"></span>
								</span>
							</span>
						</a>
					</div>
				</div>
			</div>
		</div>

		<div class="container u-space-2-top u-space-3-bottom u-cubeportfolio">
			<h3> Shadow Creations </h3>
			<div class="cbp mb-7" data-controls="#cubeFilter" data-animation="quicksand" data-x-gap="16" data-y-gap="16" data-load-more-selector="#cubeLoadMore" data-load-more-action="auto" data-load-items-amount="2" data-media-queries='[
              {"width": 1500, "cols": 4},
              {"width": 1100, "cols": 4},
              {"width": 800, "cols": 3},
              {"width": 480, "cols": 2},
			  {"width": 300, "cols": 1}]'>
			  
			  	<div class="cbp-item rounded abstract">
					<div class="cbp-caption">
						<a class="cbp-lightbox u-media-viewer" href="assets/img/Shadowcreations/11.png" data-title="RainbowClipping">
							<img src="assets/img/Shadowcreations/1.png" alt="Image Description">
							<span class="u-media-viewer__container">
								<span class="u-media-viewer__icon">
									<span class="fa fa-plus u-media-viewer__icon-inner"></span>
								</span>
							</span>
						</a>
					</div>
				</div>

				<div class="cbp-item rounded branding">
					<div class="cbp-caption">
						<a class="cbp-lightbox u-media-viewer" href="assets/img/Shadowcreations/22.png" data-title="RainbowClipping">
							<img src="assets/img/Shadowcreations/2.png" alt="Image Description">
							<span class="u-media-viewer__container">
								<span class="u-media-viewer__icon">
									<span class="fa fa-plus u-media-viewer__icon-inner"></span>
								</span>
							</span>
						</a>
					</div>
				</div>

				<div class="cbp-item rounded abstract">
					<div class="cbp-caption">
						<a class="cbp-lightbox u-media-viewer" href="assets/img/Shadowcreations/33.png" data-title="RainbowClipping">
							<img src="assets/img/Shadowcreations/3.png" alt="Image Description">
							<span class="u-media-viewer__container">
								<span class="u-media-viewer__icon">
									<span class="fa fa-plus u-media-viewer__icon-inner"></span>
								</span>
							</span>
						</a>
					</div>
				</div>

				<div class="cbp-item rounded branding">
					<div class="cbp-caption">
						<a class="cbp-lightbox u-media-viewer" href="assets/img/Shadowcreations/44.png" data-title="RainbowClipping">
							<img src="assets/img/Shadowcreations/4.png" alt="Image Description">
							<span class="u-media-viewer__container">
								<span class="u-media-viewer__icon">
									<span class="fa fa-plus u-media-viewer__icon-inner"></span>
								</span>
							</span>
						</a>
					</div>
				</div>
			</div>
		</div>

		<div class="container u-space-2-top u-space-3-bottom u-cubeportfolio">
			<h3> Color Corrections</h3>
			<div class="cbp mb-7" data-controls="#cubeFilter" data-animation="quicksand" data-x-gap="16" data-y-gap="16" data-load-more-selector="#cubeLoadMore" data-load-more-action="auto" data-load-items-amount="2" data-media-queries='[
              {"width": 1500, "cols": 4},
              {"width": 1100, "cols": 4},
              {"width": 800, "cols": 3},
              {"width": 480, "cols": 2},
			  {"width": 300, "cols": 1}]'>
			  
			  <div class="cbp-item rounded abstract">
					<div class="cbp-caption">
						<a class="cbp-lightbox u-media-viewer" href="assets/img/VectorIllustrationConversion/44.png" data-title="RainbowClipping">
							<img src="assets/img/VectorIllustrationConversion/4.png" alt="Image Description">
							<span class="u-media-viewer__container">
								<span class="u-media-viewer__icon">
									<span class="fa fa-plus u-media-viewer__icon-inner"></span>
								</span>
							</span>
						</a>
					</div>
				</div>

				<div class="cbp-item rounded branding">
					<div class="cbp-caption">
						<a class="cbp-lightbox u-media-viewer" href="assets/img/VectorIllustrationConversion/11.png" data-title="RainbowClipping">
							<img src="assets/img/VectorIllustrationConversion/1.png" alt="Image Description">
							<span class="u-media-viewer__container">
								<span class="u-media-viewer__icon">
									<span class="fa fa-plus u-media-viewer__icon-inner"></span>
								</span>
							</span>
						</a>
					</div>
				</div>

				<div class="cbp-item rounded abstract">
					<div class="cbp-caption">
						<a class="cbp-lightbox u-media-viewer" href="assets/img/VectorIllustrationConversion/22.png" data-title="RainbowClipping">
							<img src="assets/img/VectorIllustrationConversion/2.png" alt="Image Description">
							<span class="u-media-viewer__container">
								<span class="u-media-viewer__icon">
									<span class="fa fa-plus u-media-viewer__icon-inner"></span>
								</span>
							</span>
						</a>
					</div>
				</div>

				<div class="cbp-item rounded branding">
					<div class="cbp-caption">
						<a class="cbp-lightbox u-media-viewer" href="assets/img/VectorIllustrationConversion/33.png" data-title="RainbowClipping">
							<img src="assets/img/VectorIllustrationConversion/3.png" alt="Image Description">
							<span class="u-media-viewer__container">
								<span class="u-media-viewer__icon">
									<span class="fa fa-plus u-media-viewer__icon-inner"></span>
								</span>
							</span>
						</a>
					</div>
				</div>
			</div>
		</div>

		<div class="container u-space-2-top u-space-3-bottom u-cubeportfolio">
			<h3> Photo Restorations</h3>
			<div class="cbp mb-7" data-controls="#cubeFilter" data-animation="quicksand" data-x-gap="16" data-y-gap="16" data-load-more-selector="#cubeLoadMore" data-load-more-action="auto" data-load-items-amount="2" data-media-queries='[
              {"width": 1500, "cols": 4},
              {"width": 1100, "cols": 4},
              {"width": 800, "cols": 3},
              {"width": 480, "cols": 2},
			  {"width": 300, "cols": 1}]'>
			  
			  	<div class="cbp-item rounded abstract">
					<div class="cbp-caption">
						<a class="cbp-lightbox u-media-viewer" href="assets/img/Goasmannequeen/11.png" data-title="RainbowClipping">
							<img src="assets/img/Goasmannequeen/1.png" alt="Image Description">
							<span class="u-media-viewer__container">
								<span class="u-media-viewer__icon">
									<span class="fa fa-plus u-media-viewer__icon-inner"></span>
								</span>
							</span>
						</a>
					</div>
				</div>

				<div class="cbp-item rounded branding">
					<div class="cbp-caption">
						<a class="cbp-lightbox u-media-viewer" href="assets/img/Goasmannequeen/22.png" data-title="RainbowClipping">
							<img src="assets/img/Goasmannequeen/2.png" alt="Image Description">
							<span class="u-media-viewer__container">
								<span class="u-media-viewer__icon">
									<span class="fa fa-plus u-media-viewer__icon-inner"></span>
								</span>
							</span>
						</a>
					</div>
				</div>

				<div class="cbp-item rounded abstract">
					<div class="cbp-caption">
						<a class="cbp-lightbox u-media-viewer" href="assets/img/Goasmannequeen/33.png" data-title="RainbowClipping">
							<img src="assets/img/Goasmannequeen/3.png" alt="Image Description">
							<span class="u-media-viewer__container">
								<span class="u-media-viewer__icon">
									<span class="fa fa-plus u-media-viewer__icon-inner"></span>
								</span>
							</span>
						</a>
					</div>
				</div>

				<div class="cbp-item rounded branding">
					<div class="cbp-caption">
						<a class="cbp-lightbox u-media-viewer" href="assets/img/Goasmannequeen/44.png" data-title="RainbowClipping">
							<img src="assets/img/Goasmannequeen/4.png" alt="Image Description">
							<span class="u-media-viewer__container">
								<span class="u-media-viewer__icon">
									<span class="fa fa-plus u-media-viewer__icon-inner"></span>
								</span>
							</span>
						</a>
					</div>
				</div>
			</div>
		</div>

		<div class="container u-space-2-top u-space-3-bottom u-cubeportfolio">
			<h3> Logo Design & R2V </h3>
			<div class="cbp mb-7" data-controls="#cubeFilter" data-animation="quicksand" data-x-gap="16" data-y-gap="16" data-load-more-selector="#cubeLoadMore" data-load-more-action="auto" data-load-items-amount="2" data-media-queries='[
              {"width": 1500, "cols": 4},
              {"width": 1100, "cols": 4},
              {"width": 800, "cols": 3},
              {"width": 480, "cols": 2},
			  {"width": 300, "cols": 1}]'>
			  
			  	<div class="cbp-item rounded abstract">
					<div class="cbp-caption">
						<a class="cbp-lightbox u-media-viewer" href="assets/img/Shadowcreations/11.png" data-title="RainbowClipping">
							<img src="assets/img/Shadowcreations/1.png" alt="Image Description">
							<span class="u-media-viewer__container">
								<span class="u-media-viewer__icon">
									<span class="fa fa-plus u-media-viewer__icon-inner"></span>
								</span>
							</span>
						</a>
					</div>
				</div>

				<div class="cbp-item rounded branding">
					<div class="cbp-caption">
						<a class="cbp-lightbox u-media-viewer" href="assets/img/Shadowcreations/22.png" data-title="RainbowClipping">
							<img src="assets/img/Shadowcreations/2.png" alt="Image Description">
							<span class="u-media-viewer__container">
								<span class="u-media-viewer__icon">
									<span class="fa fa-plus u-media-viewer__icon-inner"></span>
								</span>
							</span>
						</a>
					</div>
				</div>

				<div class="cbp-item rounded abstract">
					<div class="cbp-caption">
						<a class="cbp-lightbox u-media-viewer" href="assets/img/Shadowcreations/33.png" data-title="RainbowClipping">
							<img src="assets/img/Shadowcreations/3.png" alt="Image Description">
							<span class="u-media-viewer__container">
								<span class="u-media-viewer__icon">
									<span class="fa fa-plus u-media-viewer__icon-inner"></span>
								</span>
							</span>
						</a>
					</div>
				</div>

				<div class="cbp-item rounded branding">
					<div class="cbp-caption">
						<a class="cbp-lightbox u-media-viewer" href="assets/img/Shadowcreations/44.png" data-title="RainbowClipping">
							<img src="assets/img/Shadowcreations/4.png" alt="Image Description">
							<span class="u-media-viewer__container">
								<span class="u-media-viewer__icon">
									<span class="fa fa-plus u-media-viewer__icon-inner"></span>
								</span>
							</span>
						</a>
					</div>
				</div>
			</div>
		</div>
		
		<div id="instaFeed" class="js-instagram row mx-gutters-2" data-user-id="4815936096" data-client-id="60648b9247324e87a4d7ab3aa7a58f6c" data-token="4815936096.1677ed0.1cbcaacad4de4ffa96aa15ec76e0e5ce" data-limit="4" data-template='<div class="col-md-3 mb-3 mb-sm-0"><a href="%7b%7blink%7d%7d.html" target="_blank"><img class="img-fluid w-100 rounded" src="%7b%7bimage%7d%7d.html" /></a></div>'></div>

		<hr class="my-0">
	</main>
	
	
@endsection  