@extends('rainbow.layouts.master')

@section('title', '| gallery')

@section('content')

	<main id="content" role="main">
		<!-- Author Section -->
		<div class="position-relative text-center u-space-3-top u-gradient-overlay-half-info-v1 u-bg-img-hero" style="background-image: url(assets/img/1920x800/img10.jpg);">
			<div class="row justify-content-md-center">
				<div class="col-md-8 col-lg-7 col-xl-11">
					<!-- Info -->
					<div class="service_color"> <br> <br> <br>
						<h1 class="display-6 text-white font-weight-normal mb-3">Payments</h1>
						<p style="text-align: justify;"></p>
					</div>
					<!-- End Info -->

					<!-- Avatar -->
					<img class="img-fluid u-xl-avatar u-xl-avatar--bordered rounded-circle mx-auto" src="assets/img/paypal.png" alt="Image Description">
					<!-- End Avatar -->
				</div>

				<!-- SVG Background -->
				<figure class="position-absolute-bottom-0 z-index-minus-1">
					<svg preserveAspectRatio="none" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="100%" height="200px" viewBox="20 -20 300 100" style="margin-bottom: -8px;" xml:space="preserve">
						<path class="u-fill-white" opacity="0.4" d="M30.913,43.944c0,0,42.911-34.464,87.51-14.191c77.31,35.14,113.304-1.952,146.638-4.729
              c48.654-4.056,69.94,16.218,69.94,16.218v54.396H30.913V43.944z" />
						<path class="u-fill-white" opacity="0.4" d="M-35.667,44.628c0,0,42.91-34.463,87.51-14.191c77.31,35.141,113.304-1.952,146.639-4.729
              c48.653-4.055,69.939,16.218,69.939,16.218v54.396H-35.667V44.628z" />
						<path class="u-fill-white" opacity="0" d="M43.415,98.342c0,0,48.283-68.927,109.133-68.927c65.886,0,97.983,67.914,97.983,67.914v3.716
              H42.401L43.415,98.342z" />
						<path class="u-fill-white" d="M-34.667,62.998c0,0,56-45.667,120.316-27.839C167.484,57.842,197,41.332,232.286,30.428
              c53.07-16.399,104.047,36.903,104.047,36.903l1.333,36.667l-372-2.954L-34.667,62.998z" />
					</svg>
				</figure>
				<!-- End SVG Background Section -->
			</div>
		</div>
		<!-- End Author Section -->

		<div class="position-relative u-gradient-half-info-v1 z-index-2">
			<div class="u-bg-img-hero-bottom" style="background-image: url(assets/img/bg-shapes/bg2.png);">
				<div class="container u-space-3-top u-space-4-bottom">
					<!-- Testimonials -->
					<div class="text-center">
						<div class="mb-4">
							<p style="font-size: 20px;"><strong>Please click on below PayPal icon for the payment</strong> then please put the total amount on “<strong>Price per item</strong>” Box and then click on continue button , <strong style="color:#CF472A;">no need to change the ‘Quantity’ box</strong>.</p>
						</div>
						<div class="col-md-12 col-lg-12 col-xl-11">
							<a href="#"><img src="assets/img/paypal-ban.png" alt="Nature" class="responsive"></a>
						</div>
						<div class="mb-8">
							<p style="text-align: justify;">clipping provides unique options for the client to pay for the services. There is no need to set up an account or any type of advance payment options to obtain the services from clipping. The client simply fills out a form providing the contact name, company information with full address and contact number. After a batch of work is done, an invoice is sent to the client. The client will have the option to pay by Paypal using a credit card or bank account or in the case of USA based client, a check may be sent to the following address:</p>
							<p style="text-align: center;">Color Experts International, Inc. 204 Greenhow Court SE, Leesburg, Virginia 20175, USA. Tel: (703) 203-6406 (USA)</p>
							<p style="text-align: center;">The invoice that is sent by clipping is simple and contains all the information necessary for making the payment.</p>
						</div>
					</div>
					<!-- End Testimonials -->

					<div class="container u-space-3">
						<div class="container">
							<div class="main_title">
								<h2 style="font-family: monospace; font-size:18px;">Get The Best Price In The Global Photo Editing Industry
								</h2>
								<p class="job">clipping provides unique options for the client to pay for the services</p>
							</div>
							<div class="work_inner row">
								<div class="offset col-lg-2"></div>
								<div class="col-lg-8"> 
									@if($message = Session::get('success'))
										<div class="w3-panel w3-green w3-display-container">
											<span onclick="this.parentElement.style.display='none'"
												class="w3-button w3-green w3-large w3-display-topright">$times;</span>
											{{!! $message !!}}
										</div>
										<?php Session::forget('success'); ?>
									@endif
									@if($message = Session::get('error'))
										<div class="w3-panel w3-red w3-display-container">
											<span onclick="this.parentElement.style.display='none'"
												class="w3-button w3-red w3-large w3-display-topright">$times;</span>
											{{!! $message !!}}
										</div>
										<?php Session::forget('error'); ?>        
									@endif
									<form class="#" method="POST" id="payment-form" action="{!! URL::to('paypal') !!}">
            							{{ csrf_field() }}
										<div class="form-row">
											<div class="form-group col-md-12">
												<div class="input-group-prepend">
													<span class="input-group-text">Payment:</span>
													<input type="text" class="form-control" id="amount" name="amount" placeholder="Please Enter Amount" required>
												</div>
											</div>		
											<input class="btn btn-sm btn-primary u-btn-primary transition-3d-hove" type="submit" value="Pay with PayPal">
										</div>
									</form>
								</div>
								<div class="offset col-lg-2"></div>
			
							</div>
							<!--work inner area-->
						</div>
					</div>	
				</div>

				<!-- SVG Quote -->
				<figure class="w-25 position-absolute-top-0 left-15x">
					<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 8 8" style="enable-background:new 0 0 8 8;" xml:space="preserve">
						<path class="u-fill-white" opacity=".1" d="M3,1.3C2,1.7,1.2,2.7,1.2,3.6c0,0.2,0,0.4,0.1,0.5c0.2-0.2,0.5-0.3,0.9-0.3c0.8,0,1.5,0.6,1.5,1.5c0,0.9-0.7,1.5-1.5,1.5
              C1.4,6.9,1,6.6,0.7,6.1C0.4,5.6,0.3,4.9,0.3,4.5c0-1.6,0.8-2.9,2.5-3.7L3,1.3z M7.1,1.3c-1,0.4-1.8,1.4-1.8,2.3
              c0,0.2,0,0.4,0.1,0.5c0.2-0.2,0.5-0.3,0.9-0.3c0.8,0,1.5,0.6,1.5,1.5c0,0.9-0.7,1.5-1.5,1.5c-0.7,0-1.1-0.3-1.4-0.8
              C4.4,5.6,4.4,4.9,4.4,4.5c0-1.6,0.8-2.9,2.5-3.7L7.1,1.3z" />
					</svg>
				</figure>
				<!-- End SVG Quote -->

				<!-- SVG Shape -->
				<figure class="position-absolute-top-0 z-index-minus-1">
					<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 1920 836" style="enable-background:new 0 0 1920 836;" xml:space="preserve">
						<linearGradient id="CTASVGID3" gradientUnits="userSpaceOnUse" x1="1829.465" y1="101.9586" x2="1829.0533" y2="650.0167">
							<stop class="u-stop-color-primary-darker" offset="2.363251e-06" />
							<stop class="u-stop-color-info-lighter" offset="1" />
						</linearGradient>
						<path fill="url(#CTASVGID3)" d="M1920,102.1c0,0-136.5-4.6-173.5,102.4s64,100,80,258s93.5,187.6,93.5,187.6V102.1z" />
					</svg>
				</figure>
				<!-- End SVG Shape -->
			</div>
		</div>
		<!-- End Title -->
		<!--Payment-->
		
		<!--Payment-->
	</main>

@endsection  