@extends('rainbow.layouts.master')

@section('title', '| Quote')

@section('content')
 
	<main id="content" role="main">

		<div class="container u-space-3">
			<div class="container">
				<div class="main_title">
					<h2 style="font-family: monospace; font-size:18px;">Get The Best Price In The Global Photo Editing Industry
					</h2>
					<p class="job">Upload your images for more accurate & Rainbow Clipping prices for our photo editing and retouching services </p>
				</div>
				<div class="work_inner row">
					<div class="offset col-lg-2"></div>
					<div class="col-lg-8">
					<form class="form-horizontal mt-5" action="{{route('quotes.update',['id'=>$quotes->id])}}" method="post" enctype="multipart/form-data">
							{{csrf_field()}}
							<div class="form-row">
								<div class="form-group col-md-6">
									<label for="inputEmail4">Your Name:</label>
									<input type="text" class="form-control" id="name" name="name" placeholder="Enter your full name" required="" value="{{$quotes->quotes}}">
								</div>
								<div class="form-group col-md-6">
									<label for="inputPassword4">Company Name: (Optional)</label>
									<input type="text" class="form-control" id="company" name="company" placeholder="Enter your company name" required="" value="{{$quotes->quotes}}">
								</div>
								<div class="form-group col-md-6">
									<label for="inputPassword4">Your Phone:</label>
									<input type="number" class="form-control" id="phone" name="phone" placeholder="Enter your phone number" required="" value="{{$quotes->quotes}}"> 
								</div>
								<div class="form-group col-md-6">
									<label for="inputAddress">Address:</label>
									<input type="text" class="form-control" id="address" name="address" placeholder="Enter Your Address" required=""  value="{{$quotes->quotes}}">
								</div>
								<div class="form-group col-md-6">
									<label for="inputAddress"> Web Address:</label>
									<input type="text" class="form-control" id="website" name="website" placeholder="Enter Your Address" required="" value="{{$quotes->quotes}}">
								</div> 

								<div class="form-group col-md-12">
									<label for="country">Country:</label>
									<select class="custom-select" id="conutry" name="country" value="{{ old('country') }}" required>
										<option selected>Select your country</option>
										<option value="AF" class=" ">Afghanistan</option>
										<option value="AL" class=" ">Albania</option>
										<option value="DZ" class=" ">Algeria</option>
										<option value="AS" class=" ">American Samoa</option>
										<option value="AD" class=" ">Andorra</option>
										<option value="AO" class=" ">Angola</option>
										<option value="AI" class=" ">Anguilla</option>
										<option value="AQ" class=" ">Antarctica</option>
										<option value="AG" class=" ">Antigua And Barbuda</option>
										<option value="AR" class=" ">Argentina</option>
										<option value="AM" class=" ">Armenia</option>
										<option value="AW" class=" ">Aruba</option>
										<option value="AU" class=" ">Australia</option>
										<option value="AT" class=" ">Austria</option>
										<option value="AZ" class=" ">Azerbaijan</option>
										<option value="BS" class=" ">Bahamas</option>
										<option value="BH" class=" ">Bahrain</option>
										<option value="BD" class=" ">Bangladesh</option>
										<option value="BB" class=" ">Barbados</option>
										<option value="BY" class=" ">Belarus</option>
										<option value="BE" class=" ">Belgium</option>
										<option value="BZ" class=" ">Belize</option>
										<option value="BJ" class=" ">Benin</option>
										<option value="BM" class=" ">Bermuda</option>
										<option value="BT" class=" ">Bhutan</option>
										<option value="BO" class=" ">Bolivia</option>
										<option value="BA" class=" ">Bosnia And Herzegovina</option>
										<option value="BW" class=" ">Botswana</option>
										<option value="BV" class=" ">Bouvet Island</option>
										<option value="BR" class=" ">Brazil</option>
										<option value="IO" class=" ">British Indian Ocean Territory</option>
										<option value="BN" class=" ">Brunei Darussalam</option>
										<option value="BG" class=" ">Bulgaria</option>
										<option value="BF" class=" ">Burkina Faso</option>
										<option value="BI" class=" ">Burundi</option>
										<option value="KH" class=" ">Cambodia</option>
										<option value="CM" class=" ">Cameroon</option>
										<option value="CA" class=" ">Canada</option>
										<option value="CV" class=" ">Cape Verde</option>
										<option value="KY" class=" ">Cayman Islands</option>
										<option value="CF" class=" ">Central African Republic</option>
										<option value="TD" class=" ">Chad</option>
										<option value="CL" class=" ">Chile</option>
										<option value="CN" class=" ">China</option>
										<option value="CX" class=" ">Christmas Island</option>
										<option value="CC" class=" ">Cocos (keeling) Islands</option>
										<option value="CO" class=" ">Colombia</option>
										<option value="KM" class=" ">Comoros</option>
										<option value="CG" class=" ">Congo</option>
										<option value="CD" class=" ">Congo, The Democratic Republic Of The</option>
										<option value="CK" class=" ">Cook Islands</option>
										<option value="CR" class=" ">Costa Rica</option>
										<option value="CI" class=" ">Cote D'ivoire</option>
										<option value="HR" class=" ">Croatia</option>
										<option value="CU" class=" ">Cuba</option>
										<option value="CY" class=" ">Cyprus</option>
										<option value="CZ" class=" ">Czech Republic</option>
										<option value="DK" class=" ">Denmark</option>
										<option value="DJ" class=" ">Djibouti</option>
										<option value="DM" class=" ">Dominica</option>
										<option value="DO" class=" ">Dominican Republic</option>
										<option value="TP" class=" ">East Timor</option>
										<option value="EC" class=" ">Ecuador</option>
										<option value="EG" class=" ">Egypt</option>
										<option value="SV" class=" ">El Salvador</option>
										<option value="GQ" class=" ">Equatorial Guinea</option>
										<option value="ER" class=" ">Eritrea</option>
										<option value="EE" class=" ">Estonia</option>
										<option value="ET" class=" ">Ethiopia</option>
										<option value="FK" class=" ">Falkland Islands (malvinas)</option>
										<option value="FO" class=" ">Faroe Islands</option>
										<option value="FJ" class=" ">Fiji</option>
										<option value="FI" class=" ">Finland</option>
										<option value="FR" class=" ">France</option>
										<option value="GF" class=" ">French Guiana</option>
										<option value="PF" class=" ">French Polynesia</option>
										<option value="TF" class=" ">French Southern Territories</option>
										<option value="GA" class=" ">Gabon</option>
										<option value="GM" class=" ">Gambia</option>
										<option value="GE" class=" ">Georgia</option>
										<option value="DE" class=" ">Germany</option>
										<option value="GH" class=" ">Ghana</option>
										<option value="GI" class=" ">Gibraltar</option>
										<option value="GR" class=" ">Greece</option>
										<option value="GL" class=" ">Greenland</option>
										<option value="GD" class=" ">Grenada</option>
										<option value="GP" class=" ">Guadeloupe</option>
										<option value="GU" class=" ">Guam</option>
										<option value="GT" class=" ">Guatemala</option>
										<option value="GN" class=" ">Guinea</option>
										<option value="GW" class=" ">Guinea-bissau</option>
										<option value="GY" class=" ">Guyana</option>
										<option value="HT" class=" ">Haiti</option>
										<option value="HM" class=" ">Heard Island And Mcdonald Islands</option>
										<option value="VA" class=" ">Holy See (vatican City State)</option>
										<option value="HN" class=" ">Honduras</option>
										<option value="HK" class=" ">Hong Kong</option>
										<option value="HU" class=" ">Hungary</option>
										<option value="IS" class=" ">Iceland</option>
										<option value="IN" class=" ">India</option>
										<option value="ID" class=" ">Indonesia</option>
										<option value="IR" class=" ">Iran, Islamic Republic Of</option>
										<option value="IQ" class=" ">Iraq</option>
										<option value="IE" class=" ">Ireland</option>
										<option value="IL" class=" ">Israel</option>
										<option value="IT" class=" ">Italy</option>
										<option value="JM" class=" ">Jamaica</option>
										<option value="JP" class=" ">Japan</option>
										<option value="JO" class=" ">Jordan</option>
										<option value="KZ" class=" ">Kazakstan</option>
										<option value="KE" class=" ">Kenya</option>
										<option value="KI" class=" ">Kiribati</option>
										<option value="KP" class=" ">Korea, Democratic People's Republic Of</option>
										<option value="KR" class=" ">Korea, Republic Of</option>
										<option value="KV" class=" ">Kosovo</option>
										<option value="KW" class=" ">Kuwait</option>
										<option value="KG" class=" ">Kyrgyzstan</option>
										<option value="LA" class=" ">Lao People's Democratic Republic</option>
										<option value="LV" class=" ">Latvia</option>
										<option value="LB" class=" ">Lebanon</option>
										<option value="LS" class=" ">Lesotho</option>
										<option value="LR" class=" ">Liberia</option>
										<option value="LY" class=" ">Libyan Arab Jamahiriya</option>
										<option value="LI" class=" ">Liechtenstein</option>
										<option value="LT" class=" ">Lithuania</option>
										<option value="LU" class=" ">Luxembourg</option>
										<option value="MO" class=" ">Macau</option>
										<option value="MK" class=" ">Macedonia, The Former Yugoslav Republic Of</option>
										<option value="MG" class=" ">Madagascar</option>
										<option value="MW" class=" ">Malawi</option>
										<option value="MY" class=" ">Malaysia</option>
										<option value="MV" class=" ">Maldives</option>
										<option value="ML" class=" ">Mali</option>
										<option value="MT" class=" ">Malta</option>
										<option value="MH" class=" ">Marshall Islands</option>
										<option value="MQ" class=" ">Martinique</option>
										<option value="MR" class=" ">Mauritania</option>
										<option value="MU" class=" ">Mauritius</option>
										<option value="YT" class=" ">Mayotte</option>
										<option value="MX" class=" ">Mexico</option>
										<option value="FM" class=" ">Micronesia, Federated States Of</option>
										<option value="MD" class=" ">Moldova, Republic Of</option>
										<option value="MC" class=" ">Monaco</option>
										<option value="MN" class=" ">Mongolia</option>
										<option value="MS" class=" ">Montserrat</option>
										<option value="ME" class=" ">Montenegro</option>
										<option value="MA" class=" ">Morocco</option>
										<option value="MZ" class=" ">Mozambique</option>
										<option value="MM" class=" ">Myanmar</option>
										<option value="NA" class=" ">Namibia</option>
										<option value="NR" class=" ">Nauru</option>
										<option value="NP" class=" ">Nepal</option>
										<option value="NL" class=" ">Netherlands</option>
										<option value="AN" class=" ">Netherlands Antilles</option>>
										<option value="NC" class=" ">New Caledonia</option>
										<option value="NZ" class=" ">New Zealand</option>
										<option value="NI" class=" ">Nicaragua</option>
										<option value="NE" class=" ">Niger</option>
										<option value="NG" class=" ">Nigeria</option>
										<option value="NU" class=" ">Niue</option>
										<option value="NF" class=" ">Norfolk Island</option>
										<option value="MP" class=" ">Northern Mariana Islands</option>
										<option value="NO" class=" ">Norway</option>
										<option value="OM" class=" ">Oman</option>
										<option value="PK" class=" ">Pakistan</option>
										<option value="PW" class=" ">Palau</option>
										<option value="PS" class=" ">Palestinian Territory, Occupied</option>
										<option value="PA" class=" ">Panama</option>
										<option value="PG" class=" ">Papua New Guinea</option>
										<option value="PY" class=" ">Paraguay</option>
										<option value="PE" class=" ">Peru</option>
										<option value="PH" class=" ">Philippines</option>
										<option value="PN" class=" ">Pitcairn</option>
										<option value="PL" class=" ">Poland</option>
										<option value="PT" class=" ">Portugal</option>
										<option value="PR" class=" ">Puerto Rico</option>
										<option value="QA" class=" ">Qatar</option>
										<option value="RE" class=" ">Reunion</option>
										<option value="RO" class=" ">Romania</option>
										<option value="RU" class=" ">Russian Federation</option>
										<option value="RW" class=" ">Rwanda</option>
										<option value="SH" class=" ">Saint Helena</option>
										<option value="KN" class=" ">Saint Kitts And Nevis</option>
										<option value="LC" class=" ">Saint Lucia</option>
										<option value="PM" class=" ">Saint Pierre And Miquelon</option>
										<option value="VC" class=" ">Saint Vincent And The Grenadines</option>
										<option value="WS" class=" ">Samoa</option>
										<option value="SM" class=" ">San Marino</option>
										<option value="ST" class=" ">Sao Tome And Principe</option>
										<option value="SA" class=" ">Saudi Arabia</option>
										<option value="SN" class=" ">Senegal</option>
										<option value="RS" class=" ">Serbia</option>
										<option value="SC" class=" ">Seychelles</option>
										<option value="SL" class=" ">Sierra Leone</option>
										<option value="SG" class=" ">Singapore</option>
										<option value="SK" class=" ">Slovakia</option>
										<option value="SI" class=" ">Slovenia</option>
										<option value="SB" class=" ">Solomon Islands</option>
										<option value="SO" class=" ">Somalia</option>
										<option value="ZA" class=" ">South Africa</option>
										<option value="GS" class=" ">South Georgia And The South Sandwich Islands</option>
										<option value="ES" class=" ">Spain</option>
										<option value="LK" class=" ">Sri Lanka</option>
										<option value="SD" class=" ">Sudan</option>
										<option value="SR" class=" ">Suriname</option>
										<option value="SJ" class=" ">Svalbard And Jan Mayen</option>
										<option value="SZ" class=" ">Swaziland</option>
										<option value="SE" class=" ">Sweden</option>
										<option value="CH" class=" ">Switzerland</option>
										<option value="SY" class=" ">Syrian Arab Republic</option>
										<option value="TW" class=" ">Taiwan, Province Of China</option>
										<option value="TJ" class=" ">Tajikistan</option>
										<option value="TZ" class=" ">Tanzania, United Republic Of</option>
										<option value="TH" class=" ">Thailand</option>
										<option value="TG" class=" ">Togo</option>
										<option value="TK" class=" ">Tokelau</option>
										<option value="TO" class=" ">Tonga</option>
										<option value="TT" class=" ">Trinidad And Tobago</option>
										<option value="TN" class=" ">Tunisia</option>
										<option value="TR" class=" ">Turkey</option>
										<option value="TM" class=" ">Turkmenistan</option>
										<option value="TC" class=" ">Turks And Caicos Islands</option>
										<option value="TV" class=" ">Tuvalu</option>
										<option value="UG" class=" ">Uganda</option>
										<option value="UA" class=" ">Ukraine</option>
										<option value="AE" class=" ">United Arab Emirates</option>>
										<option value="GB" class=" ">United Kingdom</option>
										<option value="US" class=" ">United States</option>
										<option value="UM" class=" ">United States Minor Outlying Islands</option>
										<option value="UY" class=" ">Uruguay</option>
										<option value="UZ" class=" ">Uzbekistan</option>
										<option value="VU" class=" ">Vanuatu</option>
										<option value="VE" class=" ">Venezuela</option>
										<option value="VN" class=" ">Viet Nam</option>
										<option value="VG" class=" ">Virgin Islands, British</option>
										<option value="VI" class=" ">Virgin Islands, U.s.</option>
										<option value="WF" class=" ">Wallis And Futuna</option>
										<option value="EH" class=" ">Western Sahara</option>
										<option value="YE" class=" ">Yemen</option>
										<option value="ZM" class=" ">Zambia</option>
										<option value="ZW" class=" ">Zimbabwe</option>
									</select>
								</div>

								<div class="form-group col-md-12">
									<label for="inputAddress2">Job Title:</label>
									<p class="job">Please mention the job category in the box below one/multiple from the below job list.</p>
									<div class="form-row">
										<div class="form-group col-md-6">
											<div class="form-check">
												<input class="form-check-input" type="checkbox" id="gridCheck" name="job_title" value="Clipping path">
												<label class="form-check-label checkfont" for="gridCheck">
													Clipping path
												</label>
											</div>
											<div class="form-check">
												<input class="form-check-input" type="checkbox" id="gridCheck" name="job_title" value="Image Masking">
												<label class="form-check-label checkfont" for="gridCheck">
													Image Masking
												</label>
											</div>
											<div class="form-check">
												<input class="form-check-input" type="checkbox" id="gridCheck" name="job_title" value="Photo retouching">
												<label class="form-check-label checkfont" for="gridCheck">
													Photo retouching
												</label>
											</div>
											<div class="form-check">
												<input class="form-check-input" type="checkbox" id="gridCheck" name="job_title" value="Shadow creations">
												<label class="form-check-label checkfont" for="gridCheck">
													Shadow creations
												</label>
											</div>
											<div class="form-check">
												<input class="form-check-input" type="checkbox" id="gridCheck" name="job_title" value="Ghost mannequin">
												<label class="form-check-label checkfont" for="gridCheck">
													Ghost mannequin
												</label>
											</div>
											<div class="form-check">
												<input class="form-check-input" type="checkbox" id="gridCheck" name="job_title" value="Wedding Photo Retouching">
												<label class="form-check-label checkfont" for="gridCheck">
													Wedding Photo Retouching
												</label>
											</div>
										</div>
										<div class="form-group col-md-6">
											<div class="form-check">
												<input class="form-check-input" type="checkbox" id="gridCheck" name="job_title" value="Product Photo Editing">
												<label class="form-check-label checkfont" for="gridCheck">
													Product Photo Editing
												</label>
											</div>
											<div class="form-check">
												<input class="form-check-input" type="checkbox" id="gridCheck" name="job_title" value="Real estate photo editing">
												<label class="form-check-label checkfont" for="gridCheck">
													Real estate photo editing
												</label>
											</div>
											<div class="form-check">
												<input class="form-check-input" type="checkbox" id="gridCheck" name="job_title" value="Photo resizing/cropping">
												<label class="form-check-label checkfont" for="gridCheck">
													Photo resizing/cropping
												</label>
											</div>
											<div class="form-check">
												<input class="form-check-input" type="checkbox" id="gridCheck" name="job_title" value="Colour correction">
												<label class="form-check-label checkfont" for="gridCheck">
													Colour correction
												</label>
											</div>
											<div class="form-check">
												<input class="form-check-input" type="checkbox" id="gridCheck" name="job_title" value="Photo restoration">
												<label class="form-check-label checkfont" for="gridCheck">
													Photo restoration
												</label>
											</div>
											<div class="form-check">
												<input class="form-check-input" type="checkbox" id="gridCheck" name="job_title" value="Vector Illustration & Conversion">
												<label class="form-check-label checkfont" for="gridCheck">
													Vector Illustration & Conversion
												</label>
											</div>
										</div>
									</div>
								</div>
									<div class="form-group col-md-12">
											<label for="file_images">File Upload (Upload limit - 120MB/image, 2 files only):</label>
											<input type="file" id="images" name="images[]" multiple required value="{{$quotes->quotes}}"/>
									</div>

										<form action="send" method="post">
											{{csrf_field()}}
											<div class="form-group col-md-12">
												<label for="inputAddress"> Email Address:</label>
												<input type="text" class="form-control" id="to" name="to" placeholder="Email" required="" value="{{$quotes->quotes}}">
											</div>

											<div class="form-group col-md-12">
												<div class="input-group-prepend">
													<span class="input-group-text">Message:</span>
													<textarea name="message" id="message" placeholder="" required=""></textarea value="{{$quotes->quotes}}">
												</div>
											</div>
										</form>
								<input type="submit" value="SUBMIT">
							</div>
						</form>
					</div>
					<div class="offset col-lg-2"></div>
 
				</div>
			</div>
		</div>
	</main>
	
	@endsection 