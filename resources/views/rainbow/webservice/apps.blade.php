@extends('rainbow.layouts.master')

@section('title', '| Android Apps')

@section('content')

	<main id="content" role="main">
		<!-- Author Section -->
		<div class="position-relative text-center u-space-3-top u-gradient-overlay-half-info-v1 u-bg-img-hero" style="background-image: url(assets/img/1920x800/img10.jpg);">
			<div class="row justify-content-md-center">
				<div class="col-md-8 col-lg-7 col-xl-11">
					<!-- Info -->
					<div class="service_color">
					<br><br>
						<h1 class="display-6 text-white font-weight-normal mb-3">App Development</h1>
                        <p style="text-align: justify;"> We create native applications. Starting with strategy, perfecting design, and implementing it all during development.</p>
                    </div>
					<!-- End Info -->

					<!-- Followers -->
					<div class="d-flex justify-content-center align-items-center mb-7">
						<!-- Followers List -->
						<ul class="list-inline mr-2 mb-0">
							<li class="list-inline-item mr-0">
								<img class="img-fluid u-sm-avatar u-sm-avatar--bordered rounded-circle" src="assets/img/32x32/img1.jpg" alt="Image Description">
							</li>
							<li class="list-inline-item ml-offset-3 mr-0">
								<img class="img-fluid u-sm-avatar u-sm-avatar--bordered rounded-circle" src="assets/img/32x32/img3.jpg" alt="Image Description">
							</li>
							<li class="list-inline-item ml-offset-3 mr-0">
								<img class="img-fluid u-sm-avatar u-sm-avatar--bordered rounded-circle" src="assets/img/32x32/img2.jpg" alt="Image Description">
							</li>
							<li class="list-inline-item ml-offset-3 mr-0">
								<img class="img-fluid u-sm-avatar u-sm-avatar--bordered rounded-circle" src="assets/img/32x32/img4.jpg" alt="Image Description">
							</li>
							<li class="list-inline-item ml-offset-3 mr-0">
								<img class="img-fluid u-sm-avatar u-sm-avatar--bordered rounded-circle" src="assets/img/32x32/img5.jpg" alt="Image Description">
							</li>
						</ul>
						<!-- End Followers List -->
					</div>
					<!-- End Followers -->

					<!-- Avatar -->
					<img class="img-fluid u-xl-avatar u-xl-avatar--bordered rounded-circle mx-auto" src="assets/img/cli.jpg" alt="Image Description">
					<!-- End Avatar -->
				</div>

				<!-- SVG Background -->
				<figure class="position-absolute-bottom-0 z-index-minus-1">
					<svg preserveAspectRatio="none" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="100%" height="200px" viewBox="20 -20 300 100" style="margin-bottom: -8px;" xml:space="preserve">
						<path class="u-fill-white" opacity="0.4" d="M30.913,43.944c0,0,42.911-34.464,87.51-14.191c77.31,35.14,113.304-1.952,146.638-4.729
              c48.654-4.056,69.94,16.218,69.94,16.218v54.396H30.913V43.944z" />
						<path class="u-fill-white" opacity="0.4" d="M-35.667,44.628c0,0,42.91-34.463,87.51-14.191c77.31,35.141,113.304-1.952,146.639-4.729
              c48.653-4.055,69.939,16.218,69.939,16.218v54.396H-35.667V44.628z" />
						<path class="u-fill-white" opacity="0" d="M43.415,98.342c0,0,48.283-68.927,109.133-68.927c65.886,0,97.983,67.914,97.983,67.914v3.716
              H42.401L43.415,98.342z" />
						<path class="u-fill-white" d="M-34.667,62.998c0,0,56-45.667,120.316-27.839C167.484,57.842,197,41.332,232.286,30.428
              c53.07-16.399,104.047,36.903,104.047,36.903l1.333,36.667l-372-2.954L-34.667,62.998z" />
					</svg>
				</figure>
				<!-- End SVG Background Section -->
			</div>
		</div>
		<!-- End Author Section -->

		<!-- Cubeportfolio Section -->
		<div class="container u-space-2-top u-space-3-bottom u-cubeportfolio">

			<!-- Content -->
			<div class="cbp mb-7" data-controls="#cubeFilter" data-animation="quicksand" data-x-gap="16" data-y-gap="16" data-load-more-selector="#cubeLoadMore" data-load-more-action="auto" data-load-items-amount="2" data-media-queries='[
              {"width": 1500, "cols": 4},
              {"width": 1100, "cols": 4},
              {"width": 800, "cols": 3},
              {"width": 480, "cols": 2},
              {"width": 300, "cols": 1}
            ]'>
				<!-- Item -->
				<div class="cbp-item rounded abstract">
					<div class="cbp-caption">
						<a class="cbp-lightbox u-media-viewer" href="assets/img/1920x1920/img2.jpg" data-title="RainbowClipping">
							<img src="assets/img/1920x1920/1.jpg" alt="Image Description">
							<span class="u-media-viewer__container">
								<span class="u-media-viewer__icon">
									<span class="fa fa-plus u-media-viewer__icon-inner"></span>
								</span>
							</span>
						</a>
					</div>
				</div>
				<!-- End Item -->

				<!-- Item -->
				<div class="cbp-item rounded branding">
					<div class="cbp-caption">
						<a class="cbp-lightbox u-media-viewer" href="assets/img/1920x1920/img10.jpg" data-title="RainbowClipping">
							<img src="assets/img/1920x1920/4.jpg" alt="Image Description">
							<span class="u-media-viewer__container">
								<span class="u-media-viewer__icon">
									<span class="fa fa-plus u-media-viewer__icon-inner"></span>
								</span>
							</span>
						</a>
					</div>
				</div>
				<!-- End Item -->

				<!-- Item -->
				<div class="cbp-item rounded abstract">
					<div class="cbp-caption">
						<a class="cbp-lightbox u-media-viewer" href="assets/img/1920x1920/img5.jpg" data-title="RainbowClipping">
							<img src="assets/img/1920x1920/2.jpg" alt="Image Description">
							<span class="u-media-viewer__container">
								<span class="u-media-viewer__icon">
									<span class="fa fa-plus u-media-viewer__icon-inner"></span>
								</span>
							</span>
						</a>
					</div>
				</div>
				<!-- End Item -->

				<!-- Item -->
				<div class="cbp-item rounded branding">
					<div class="cbp-caption">
						<a class="cbp-lightbox u-media-viewer" href="assets/img/1920x1920/img4.jpg" data-title="RainbowClipping">
							<img src="assets/img/1920x1920/3.jpg" alt="Image Description">
							<span class="u-media-viewer__container">
								<span class="u-media-viewer__icon">
									<span class="fa fa-plus u-media-viewer__icon-inner"></span>
								</span>
							</span>
						</a>
					</div>
				</div>

				<!-- End Item -->
			</div>
			<!-- End Content -->

		</div>
		<!-- End Cubeportfolio Section -->

		<!-- Instafeed -->
		<div id="instaFeed" class="js-instagram row mx-gutters-2" data-user-id="4815936096" data-client-id="60648b9247324e87a4d7ab3aa7a58f6c" data-token="4815936096.1677ed0.1cbcaacad4de4ffa96aa15ec76e0e5ce" data-limit="4" data-template='<div class="col-md-3 mb-3 mb-sm-0"><a href="%7b%7blink%7d%7d.html" target="_blank"><img class="img-fluid w-100 rounded" src="%7b%7bimage%7d%7d.html" /></a></div>'></div>
		<!-- End Instafeed -->
		<!-- End Instagram -->

		<hr class="my-0">
	</main>
	
	<!-- Account Sidebar Navigation -->
	<aside id="sidebarContent" class="u-sidebar u-unfold--css-animation u-unfold--hidden" aria-labelledby="sidebarNavToggler">
		<div class="u-sidebar__scroller">
			<div class="u-sidebar__container">
				<div class="u-header-sidebar__footer-offset">
					<!-- Toggle Button -->
					<div class="d-flex align-items-center pt-4 px-7">
						<button type="button" class="close ml-auto" aria-controls="sidebarContent" aria-haspopup="true" aria-expanded="false" data-unfold-event="click" data-unfold-hide-on-scroll="false" data-unfold-target="#sidebarContent" data-unfold-type="css-animation" data-unfold-animation-in="fadeInRight" data-unfold-animation-out="fadeOutRight" data-unfold-duration="500">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<!-- End Toggle Button -->

					<!-- Content -->
					<div class="js-scrollbar u-sidebar__body">
						<div class="u-sidebar__content u-header-sidebar__content">
							<form class="js-validate">
								<!-- Login -->
								<div id="login" data-target-group="idForm">
									<!-- Title -->
									<header class="text-center mb-7">
										<h2 class="h4 mb-0">Welcome Back!</h2>
										<p>Login to manage your account.</p>
									</header>
									<!-- End Title -->

									<!-- Input -->
									<div class="js-form-message mb-4">
										<div class="js-focus-state input-group u-form">
											<div class="input-group-prepend u-form__prepend">
												<span class="input-group-text u-form__text">
													<span class="fa fa-user u-form__text-inner"></span>
												</span>
											</div>
											<input type="email" class="form-control u-form__input" name="email" required placeholder="Email" aria-label="Email" data-msg="Please enter a valid email address." data-error-class="u-has-error" data-success-class="u-has-success">
										</div>
									</div>
									<!-- End Input -->

									<!-- Input -->
									<div class="js-form-message mb-2">
										<div class="js-focus-state input-group u-form">
											<div class="input-group-prepend u-form__prepend">
												<span class="input-group-text u-form__text">
													<span class="fa fa-lock u-form__text-inner"></span>
												</span>
											</div>
											<input type="password" class="form-control u-form__input" name="password" required placeholder="Password" aria-label="Password" data-msg="Your password is invalid. Please try again." data-error-class="u-has-error" data-success-class="u-has-success">
										</div>
									</div>
									<!-- End Input -->

									<div class="clearfix mb-4">
										<a class="js-animation-link float-right small u-link-muted" href="javascript:;" data-target="#forgotPassword" data-link-group="idForm" data-animation-in="slideInUp">Forgot Password?</a>
									</div>

									<div class="mb-2">
										<button type="submit" class="btn btn-block btn-primary u-btn-primary transition-3d-hover">Login</button>
									</div>

									<div class="text-center mb-4">
										<span class="small text-muted">Do not have an account?</span>
										<a class="js-animation-link small" href="javascript:;" data-target="#signup" data-link-group="idForm" data-animation-in="slideInUp">Signup
										</a>
									</div>

								</div>

								<!-- Signup -->
								<div id="signup" style="display: none; opacity: 0;" data-target-group="idForm">
									<!-- Title -->
									<header class="text-center mb-7">
										<h2 class="h4 mb-0">Welcome to Front.</h2>
										<p>Fill out the form to get started.</p>
									</header>
									<!-- End Title -->

									<!-- Input -->
									<div class="js-form-message mb-4">
										<div class="js-focus-state input-group u-form">
											<div class="input-group-prepend u-form__prepend">
												<span class="input-group-text u-form__text">
													<span class="fa fa-user u-form__text-inner"></span>
												</span>
											</div>
											<input type="email" class="form-control u-form__input" name="email" required placeholder="Email" aria-label="Email" data-msg="Please enter a valid email address." data-error-class="u-has-error" data-success-class="u-has-success">
										</div>
									</div>
									<!-- End Input -->

									<!-- Input -->
									<div class="js-form-message mb-4">
										<div class="js-focus-state input-group u-form">
											<div class="input-group-prepend u-form__prepend">
												<span class="input-group-text u-form__text">
													<span class="fa fa-lock u-form__text-inner"></span>
												</span>
											</div>
											<input type="password" class="form-control u-form__input" name="password" required placeholder="Password" aria-label="Password" data-msg="Your password is invalid. Please try again." data-error-class="u-has-error" data-success-class="u-has-success">
										</div>
									</div>
									<!-- End Input -->

									<!-- Input -->
									<div class="js-form-message mb-4">
										<div class="js-focus-state input-group u-form">
											<div class="input-group-prepend u-form__prepend">
												<span class="input-group-text u-form__text">
													<span class="fa fa-key u-form__text-inner"></span>
												</span>
											</div>
											<input type="password" class="form-control u-form__input" name="confirmPassword" required placeholder="Confirm Password" aria-label="Confirm Password" data-msg="Password does not match the confirm password." data-error-class="u-has-error" data-success-class="u-has-success">
										</div>
									</div>
									<!-- End Input -->

									<div class="mb-2">
										<button type="submit" class="btn btn-block btn-primary u-btn-primary transition-3d-hover">Get Started</button>
									</div>

									<div class="text-center mb-4">
										<span class="small text-muted">Already have an account?</span>
										<a class="js-animation-link small" href="javascript:;" data-target="#login" data-link-group="idForm" data-animation-in="slideInUp">Login
										</a>
									</div>
								</div>
								<!-- End Signup -->

								<!-- Forgot Password -->
								<div id="forgotPassword" style="display: none; opacity: 0;" data-target-group="idForm">
									<!-- Title -->
									<header class="text-center mb-7">
										<h2 class="h4 mb-0">Recover Password.</h2>
										<p>Enter your email address and an email with instructions will be sent to you.</p>
									</header>
									<!-- End Title -->

									<!-- Input -->
									<div class="js-form-message mb-4">
										<div class="js-focus-state input-group u-form">
											<div class="input-group-prepend u-form__prepend">
												<span class="input-group-text u-form__text">
													<span class="fa fa-envelope u-inner-form__text"></span>
												</span>
											</div>
											<input type="email" class="form-control u-form__input" name="email" required placeholder="Your email" aria-label="Your email" data-msg="Please enter a valid email address." data-error-class="u-has-error" data-success-class="u-has-success">
										</div>
									</div>
									<!-- End Input -->

									<div class="mb-2">
										<button type="submit" class="btn btn-block btn-primary u-btn-primary transition-3d-hover">Recover Password</button>
									</div>

									<div class="text-center mb-4">
										<span class="small text-muted">Remember your password?</span>
										<a class="js-animation-link small" href="javascript:;" data-target="#login" data-link-group="idForm" data-animation-in="slideInUp">Login
										</a>
									</div>
								</div>
								<!-- End Forgot Password -->
							</form>
						</div>
					</div>
					<!-- End Content -->
				</div>

				<!-- Footer -->
				<footer class="u-sidebar__footer u-sidebar__footer--account">
					<ul class="list-inline mb-0">
						<!--<li class="list-inline-item pr-3">
							<a class="u-sidebar__footer--account__text" href="pri">Privacy</a>
						</li>
						<li class="list-inline-item pr-3">
							<a class="u-sidebar__footer--account__text" href="https://htmlstream.com/preview/front-v1.3/html/pages/terms.html">Terms</a>
						</li>
						<li class="list-inline-item">
							<a class="u-sidebar__footer--account__text" href="https://htmlstream.com/preview/front-v1.3/html/pages/help.html">
								<i class="fa fa-info-circle"></i>
							</a>
						</li>-->
					</ul>

					<!-- SVG Background Shape -->
					<div class="position-absolute-bottom-0">
						<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 300 126.5" style="margin-bottom: -5px; enable-background:new 0 0 300 126.5;" xml:space="preserve">
							<path class="u-fill-primary" opacity=".6" d="M0,58.9c0-0.9,5.1-2,5.8-2.2c6-0.8,11.8,2.2,17.2,4.6c4.5,2.1,8.6,5.3,13.3,7.1C48.2,73.3,61,73.8,73,69
                c43-16.9,40-7.9,84-2.2c44,5.7,83-31.5,143-10.1v69.8H0C0,126.5,0,59,0,58.9z" />
							<path class="u-fill-primary" d="M300,68.5v58H0v-58c0,0,43-16.7,82,5.6c12.4,7.1,26.5,9.6,40.2,5.9c7.5-2.1,14.5-6.1,20.9-11
                c6.2-4.7,12-10.4,18.8-13.8c7.3-3.8,15.6-5.2,23.6-5.2c16.1,0.1,30.7,8.2,45,16.1c13.4,7.4,28.1,12.2,43.3,11.2
                C282.5,76.7,292.7,74.4,300,68.5z" />

						</svg>
					</div>
					<!-- End SVG Background Shape -->
				</footer>
				<!-- End Footer -->
			</div>
		</div>
	</aside>
@endsection
