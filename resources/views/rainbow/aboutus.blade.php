@extends('rainbow.layouts.master')

@section('title', '| About us')

@section('content')
	<main id="content" role="main">
		<!-- Author Section -->
		<div class="position-relative text-center u-space-3-top u-gradient-overlay-half-info-v1 u-bg-img-hero" style="background-image: url(assets/img/1920x800/img10.jpg);">
			<div class="row justify-content-md-center">
				<div class="col-md-8 col-lg-7 col-xl-11">
					<!-- Info -->
					<div class="service_color"> <br> <br> <br>
						<h1 class="display-6 text-white font-weight-normal mb-3">ABOUT US</h1>
						<p style="text-align: justify;"></p>
					</div>
					<!-- End Info -->

					<!-- Avatar -->
					<img class="img-fluid u-xl-avatar u-xl-avatar--bordered rounded-circle mx-auto" src="assets/img/about-us-15.jpg" alt="Image Description">
					<!-- End Avatar -->
				</div>

				<!-- SVG Background -->
				<figure class="position-absolute-bottom-0 z-index-minus-1">
					<svg preserveAspectRatio="none" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="100%" height="200px" viewBox="20 -20 300 100" style="margin-bottom: -8px;" xml:space="preserve">
						<path class="u-fill-white" opacity="0.4" d="M30.913,43.944c0,0,42.911-34.464,87.51-14.191c77.31,35.14,113.304-1.952,146.638-4.729
              c48.654-4.056,69.94,16.218,69.94,16.218v54.396H30.913V43.944z" />
						<path class="u-fill-white" opacity="0.4" d="M-35.667,44.628c0,0,42.91-34.463,87.51-14.191c77.31,35.141,113.304-1.952,146.639-4.729
              c48.653-4.055,69.939,16.218,69.939,16.218v54.396H-35.667V44.628z" />
						<path class="u-fill-white" opacity="0" d="M43.415,98.342c0,0,48.283-68.927,109.133-68.927c65.886,0,97.983,67.914,97.983,67.914v3.716
              H42.401L43.415,98.342z" />
						<path class="u-fill-white" d="M-34.667,62.998c0,0,56-45.667,120.316-27.839C167.484,57.842,197,41.332,232.286,30.428
              c53.07-16.399,104.047,36.903,104.047,36.903l1.333,36.667l-372-2.954L-34.667,62.998z" />
					</svg>
				</figure>
				<!-- End SVG Background Section -->
			</div>
		</div>
		<!-- End Author Section -->

		<div class="position-relative u-gradient-half-info-v1 z-index-2">
			<div class="u-bg-img-hero-bottom" style="background-image: url(assets/img/bg-shapes/bg2.png);">
				<div class="container u-space-2-top u-space-4-bottom">
					<!-- Testimonials -->
					<div class="text-center"> 
						<div class="about_us mb-8">
							<h2 style="text-align: justify;"><strong>Who are we?</strong></h2>
							<p style="text-align: justify;"> Rainbow Clipping design is the one of the uppermost excellence photoshop image editing service provider outsourcing company in Asia region.</p>
							<p style="text-align: justify;">It was founded in June, 2018 as an outsourcing service provider. However, The Rainbow Clipping design is a sister concern company of reputed software company of Link Vision Software Solution Ltd in Bangladesh. As per client requirements, we process images such changing background, Image cleansing, resizing accordingly.</p>
							<h2 style="text-align: justify;"><strong> Why choose us? </strong></h2>
							<h3 style="text-align: justify;"><strong> “Commitment & Best Quality Service in Lowest Price with Client Satisfaction” </strong></h3>
							<p style="text-align: justify;">Clipping has the most advanced state-of-the-art technology of graphic design, an excellent team of more than 250 experienced and professional graphic designers and responsible customer service executives. Our clients send their files using our secured FTP server. Then we examine their files and give them a price quote. We strictly maintain a 3 step quality checking procedure through the workflow and the time frame required by customers to complete our jobs.</p>
							<p style="text-align: justify;"><strong> Experience & Expert Team Member. </strong></p>
							<ul>
								<li><p style="text-align: justify;"> Team Leader: We have 12 experienced and dedicated designers in a very high educational background.  </p></li>
								<li><p style="text-align: justify;"> Team Member: 15 professional’s designer members are dedicated to delivery services to the clients. </p></li>
							</ul>
							<p style="text-align: justify;"><strong> Best Infrastructure. </strong></p>
							<ul>
								<li> <p style="text-align: justify;"> 2200sft office space for employee arrangement, </p> </li>
								<li> <p style="text-align: justify;"> We have great software support from our mother software company (Link Vision Software Solution Ltd) </p> </li>
								<li> <p style="text-align: justify;"> Enough high configured computers, laptops </p> </li>
								<li> <p style="text-align: justify;"> Training center for new designer,  </p> </li>
								<li> <p style="text-align: justify;"> Expert consultant, </p> </li>
								<li> <p style="text-align: justify;"> 2 high bandwidth internet service providers as a backup connection </p> </li>
								<li> <p style="text-align: justify;"> Strong power generator & File backup system, </p> </li>
								<li> <p style="text-align: justify;"> Capacity to work in 24 hours a day by 3 shift in 365 days in a year. </p> </li>
							</ul>
							<p style="text-align: justify;"><strong> Quick Response with highest quality. </strong></p>
							<ul>
								<li><p style="text-align: justify;"> Our 15 professional’s designer are committed to delivery best image editing services to the client within time frame.</p></li>
							</ul>
							<p style="text-align: justify;"><strong> No sign up requires. </strong></p>
							<ul>
								<li><p style="text-align: justify;"> Provide email address with your company information and get connect to us.</p></li>
							</ul>
							<p style="text-align: justify;"><strong> Offering Lowest Price. </strong></p>
							<ul>
								<li><p style="text-align: justify;"> We have great capacity to resource management & can provide service in lowest price to client. </p></li>
							</ul>
							<p style="text-align: justify;"><strong> Payment once a month. </strong></p>
							<ul>
								<li><p style="text-align: justify;"> After finished assigned task, we will send our invoices & client can choose to pay Paypal, Credit Card or Wire transfer. </p></li>
							</ul>
							<p style="text-align: justify;"><strong> First Verify our works </strong></p>
							<ul>
								<li><p style="text-align: justify;"> We offer three (3) images free of charge for new clients for verify our working quality, if you satisfied our service, then you will decide. </p></li>
							</ul>
							<p>&nbsp;</p>
						</div>
					</div>
					<!-- End Testimonials -->
				</div>

				<!-- SVG Quote -->
				<figure class="w-25 position-absolute-top-0 left-15x">
					<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 8 8" style="enable-background:new 0 0 8 8;" xml:space="preserve">
						<path class="u-fill-white" opacity=".1" d="M3,1.3C2,1.7,1.2,2.7,1.2,3.6c0,0.2,0,0.4,0.1,0.5c0.2-0.2,0.5-0.3,0.9-0.3c0.8,0,1.5,0.6,1.5,1.5c0,0.9-0.7,1.5-1.5,1.5
              C1.4,6.9,1,6.6,0.7,6.1C0.4,5.6,0.3,4.9,0.3,4.5c0-1.6,0.8-2.9,2.5-3.7L3,1.3z M7.1,1.3c-1,0.4-1.8,1.4-1.8,2.3
              c0,0.2,0,0.4,0.1,0.5c0.2-0.2,0.5-0.3,0.9-0.3c0.8,0,1.5,0.6,1.5,1.5c0,0.9-0.7,1.5-1.5,1.5c-0.7,0-1.1-0.3-1.4-0.8
              C4.4,5.6,4.4,4.9,4.4,4.5c0-1.6,0.8-2.9,2.5-3.7L7.1,1.3z" />
					</svg>
				</figure>
				<!-- End SVG Quote -->

				<!-- SVG Shape -->
				<figure class="position-absolute-top-0 z-index-minus-1">
					<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 1920 836" style="enable-background:new 0 0 1920 836;" xml:space="preserve">
						<linearGradient id="CTASVGID3" gradientUnits="userSpaceOnUse" x1="1829.465" y1="101.9586" x2="1829.0533" y2="650.0167">
							<stop class="u-stop-color-primary-darker" offset="2.363251e-06" />
							<stop class="u-stop-color-info-lighter" offset="1" />
						</linearGradient>
						<path fill="url(#CTASVGID3)" d="M1920,102.1c0,0-136.5-4.6-173.5,102.4s64,100,80,258s93.5,187.6,93.5,187.6V102.1z" />
					</svg>
				</figure>
				<!-- End SVG Shape -->
			</div>
		</div>


	</main>

@endsection  
  