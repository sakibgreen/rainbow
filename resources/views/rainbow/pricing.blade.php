@extends('rainbow.layouts.master')

@section('title', '| gallery')

@section('content')
	<main id="content" role="main">
		<!-- Author Section -->
		<div class="position-relative text-center u-space-3-top u-gradient-overlay-half-info-v1 u-bg-img-hero" style="background-image: url(assets/img/1920x800/img10.jpg);">
			<div class="row justify-content-md-center">
				<div class="col-md-8 col-lg-7 col-xl-11">
					<!-- Info -->
					<div class="service_color"> <br> <br> <br>
						<h1 class="display-6 text-white font-weight-normal mb-3">Welcome to Price Information</h1>
						<p style="text-align: justify;"></p>
					</div>
					<!-- End Info -->

					<!-- Followers -->
					<div class="d-flex justify-content-center align-items-center mb-7">
						<!-- Followers List -->
						<ul class="list-inline mr-2 mb-0">
							<li class="list-inline-item mr-0">
								<img class="img-fluid u-sm-avatar u-sm-avatar--bordered rounded-circle" src="assets/img/32x32/img1.jpg" alt="Image Description">
							</li>
							<li class="list-inline-item ml-offset-3 mr-0">
								<img class="img-fluid u-sm-avatar u-sm-avatar--bordered rounded-circle" src="assets/img/32x32/img3.jpg" alt="Image Description">
							</li>
							<li class="list-inline-item ml-offset-3 mr-0">
								<img class="img-fluid u-sm-avatar u-sm-avatar--bordered rounded-circle" src="assets/img/32x32/img2.jpg" alt="Image Description">
							</li>
							<li class="list-inline-item ml-offset-3 mr-0">
								<img class="img-fluid u-sm-avatar u-sm-avatar--bordered rounded-circle" src="assets/img/32x32/img4.jpg" alt="Image Description">
							</li>
							<li class="list-inline-item ml-offset-3 mr-0">
								<img class="img-fluid u-sm-avatar u-sm-avatar--bordered rounded-circle" src="assets/img/32x32/img5.jpg" alt="Image Description">
							</li>
						</ul>
						<!-- End Followers List -->
					</div>
					<!-- End Followers -->

					<!-- Avatar -->
					<img class="img-fluid u-xl-avatar u-xl-avatar--bordered rounded-circle mx-auto" src="assets/img/cash.png" alt="Image Description">
					<!-- End Avatar -->
				</div>

				<!-- SVG Background -->
				<figure class="position-absolute-bottom-0 z-index-minus-1">
					<svg preserveAspectRatio="none" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="#" width="100%" height="200px" viewBox="20 -20 300 100" style="margin-bottom: -8px;" xml:space="preserve">
						<path class="u-fill-white" opacity="0.4" d="M30.913,43.944c0,0,42.911-34.464,87.51-14.191c77.31,35.14,113.304-1.952,146.638-4.729
              c48.654-4.056,69.94,16.218,69.94,16.218v54.396H30.913V43.944z" />
						<path class="u-fill-white" opacity="0.4" d="M-35.667,44.628c0,0,42.91-34.463,87.51-14.191c77.31,35.141,113.304-1.952,146.639-4.729
              c48.653-4.055,69.939,16.218,69.939,16.218v54.396H-35.667V44.628z" />
						<path class="u-fill-white" opacity="0" d="M43.415,98.342c0,0,48.283-68.927,109.133-68.927c65.886,0,97.983,67.914,97.983,67.914v3.716
              H42.401L43.415,98.342z" />
						<path class="u-fill-white" d="M-34.667,62.998c0,0,56-45.667,120.316-27.839C167.484,57.842,197,41.332,232.286,30.428
              c53.07-16.399,104.047,36.903,104.047,36.903l1.333,36.667l-372-2.954L-34.667,62.998z" />
					</svg>
				</figure>
				<!-- End SVG Background Section -->
			</div>
		</div>
	</main>
	<section class="pricing py-5">
		<div class="container">
			<div class="row">
				<!-- Free Tier -->
				<div class="col-lg-4">
					<div class="card mb-5 mb-lg-0">
						<div class="card-body">
							<h6 class="card-price text-center"><span class="period">Clipping Path Services</span></h6>
							<h5 class="card-price text-center"><span class="period">Start From</span></h5>
							<hr>
							<ul class="fa-ul">
								<li>Basic Paths <strong style="color: black;">:$0.25</strong></li>
								<li>Simple Paths <strong style="color: black;">:$0.49 </strong></li>
								<li>Compaund Paths <strong style="color: black;">:$0.99</strong></li>
								<li>Complex Paths <strong style="color: black;">:$2.50</strong></li>
							</ul>
							<a href="{{ url('/freetrial') }}" class="btn btn-block btn-primary text-uppercase">Get start</a>
						</div>
					</div>
				</div>

				<!-- Plus Tier -->
				<div class="col-lg-4">
					<div class="card mb-5 mb-lg-0">
						<div class="card-body">

							<h6 class="card-price text-center"><span class="period">Image Retouching</span></h6>
							<h5 class="card-price text-center"><span class="period">Start From</span></h5>
							<hr>
							<ul class="fa-ul">
								<li>Basic Paths <strong style="color: black;">:$0.49</strong></li>
								<li>Simple Paths <strong style="color: black;">:$1.25 </strong></li>
								<li>Compaund Paths <strong style="color: black;">:$2.25</strong></li>
								<li>Complex Paths <strong style="color: black;">:$4.00</strong></li>
							</ul>
							<a href="{{ url('/freetrial') }}" class="btn btn-block btn-primary text-uppercase">Get start</a>
						</div>
					</div>
				</div>

				<!-- Pro Tier -->
				<div class="col-lg-4">
					<div class="card">
						<div class="card-body">

							<h6 class="card-price text-center"><span class="period">Ghost Mannequin Effects</span></h6>
							<h5 class="card-price text-center"><span class="period">Start From</span></h5>							
							<hr>
							<ul class="fa-ul">
								<li>Basic Paths <strong style="color: black;">:$0.49</strong></li>
								<li>Simple Paths <strong style="color: black;">:$1.25 </strong></li>
								<li>Compaund Paths <strong style="color: black;">:$1.75</strong></li>
								<li>Complex Paths <strong style="color: black;">:$2.50</strong></li>
							</ul>
							<a href="{{ url('/freetrial') }}" class="btn btn-block btn-primary text-uppercase">Get start </a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="pricing py-5"> 
		<div class="container">
			<div class="row">
				<!-- Free Tier -->
				<div class="col-lg-4">
					<div class="card mb-5 mb-lg-0">
						<div class="card-body">
							<h6 class="card-price text-center"><span class="period">Color Correction Services</span></h6>
							<h5 class="card-price text-center"><span class="period">Start From</span></h5>							
							<hr>
							<ul class="fa-ul">
								<li>Basic Paths <strong style="color: black;">:$0.39</strong></li>
								<li>Simple Paths <strong style="color: black;">:$0.59 </strong></li>
								<li>Compaund Paths <strong style="color: black;">:$1.30</strong></li>
								<li>Complex Paths <strong style="color: black;">:$2.75</strong></li>
							</ul>
							<a href="{{ url('/freetrial') }}" class="btn btn-block btn-primary text-uppercase">Get start </a>
						</div>
					</div>
				</div>

				<!-- Plus Tier -->
				<div class="col-lg-4">
					<div class="card mb-5 mb-lg-0">
						<div class="card-body">
							<h6 class="card-price text-center"><span class="period">Logo Design</span></h6>
							<h5 class="card-price text-center"><span class="period">Start From</span></h5>
							<hr>
							<ul class="fa-ul">
								<li>Basic Paths <strong style="color: black;">:$3.99</strong></li>
								<li>Simple Paths <strong style="color: black;">:$4.75 </strong></li>
								<li>Compaund Paths <strong style="color: black;">:$6.59</strong></li>
								<li>Complex Paths <strong style="color: black;">:$10.00</strong></li>
							</ul>
							<a href="{{ url('/freetrial') }}" class="btn btn-block btn-primary text-uppercase"> Get start</a>
						</div>
					</div>
				</div>

				<!-- Pro Tier -->
				<div class="col-lg-4">
					<div class="card">
						<div class="card-body">

							<h6 class="card-price text-center"><span class="period">Photo Restoration Services</span></h6>
							<h5 class="card-price text-center"><span class="period">Start From</span></h5>
							<hr>
							<ul class="fa-ul">
								<li>Basic Paths <strong style="color: black;">:$2.75</strong></li>
								<li>Simple Paths <strong style="color: black;">:$4.99 </strong></li>
								<li>Compaund Paths <strong style="color: black;">:$12.99</strong></li>
								<li>Complex Paths <strong style="color: black;">:$17.99</strong></li>
							</ul>
							<a href="{{ url('/freetrial') }}" class="btn btn-block btn-primary text-uppercase"> Get start</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="pricing py-5">
		<div class="container">
			<div class="row">
				<!-- Free Tier -->
				<div class="col-lg-4">
					<div class="card mb-5 mb-lg-0">
						<div class="card-body">

							<h6 class="card-price text-center"><span class="period">Image Masking Services</span></h6>
							<h5 class="card-price text-center"><span class="period">Start From</span></h5>
							<hr>
							<ul class="fa-ul">
								<li>Basic Paths <strong style="color: black;">:$0.75</strong></li>
								<li>Simple Paths <strong style="color: black;">:$1.20 </strong></li>
								<li>Compaund Paths <strong style="color: black;">:$1.59</strong></li>
								<li>Complex Paths <strong style="color: black;">:$2.25</strong></li>
							</ul>
							<a href="{{ url('/freetrial') }}" class="btn btn-block btn-primary text-uppercase"> Get start</a>
						</div>
					</div>
				</div>

				<!-- Plus Tier -->
				<div class="col-lg-4">
					<div class="card mb-5 mb-lg-0">
						<div class="card-body">

							<h6 class="card-price text-center"><span class="period">Vector Drawing Services</span></h6>
							<h5 class="card-price text-center"><span class="period">Start From</span></h5>
							<hr>
							<ul class="fa-ul">
								<li>Basic Paths <strong style="color: black;">:$2.29</strong></li>
								<li>Simple Paths <strong style="color: black;">:$3.25</strong></li>
								<li>Compaund Paths <strong style="color: black;">:$4.00</strong></li>
								<li>Complex Paths <strong style="color: black;">:$6.00</strong></li>
							</ul>
							<a href="{{ url('/freetrial') }}" class="btn btn-block btn-primary text-uppercase">Get start </a>
						</div>
					</div>
				</div>

				<!-- Pro Tier -->
				<div class="col-lg-4">
					<div class="card">
						<div class="card-body">
							<h6 class="card-price text-center"><span class="period">Photoshop Shadow Services</span></h6>
							<h5 class="card-price text-center"><span class="period">Start From</span></h5>
							<hr>
							<ul class="fa-ul">
								<li>Basic Paths <strong style="color: black;">:$0.25</strong></li>
								<li>Simple Paths <strong style="color: black;">:$0.49 </strong></li>
								<li>Compaund Paths <strong style="color: black;">:$0.49</strong></li>
								<li>Complex Paths <strong style="color: black;">:$1.00</strong></li>
							</ul>
							<a href="{{ url('/freetrial') }}" class="btn btn-block btn-primary text-uppercase"> Get start</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	
@section('content')