	<!-- Favicon -->
	<link rel="shortcut icon" href="assets/img/fav.png">

	<!-- Google Fonts -->
	<link href="http://fonts.googleapis.com/css?family=Poppins:300,400,500,600" rel="stylesheet">

	<!-- CSS Global Compulsory -->
	<link rel="stylesheet" href="assets/css/bootstrap.css">

	<!-- CSS Implementing Plugins -->
	<link rel="stylesheet" href="assets/css/font-awesome.min.css">
	<link rel="stylesheet" href="assets/css/animate.min.css">
	<link rel="stylesheet" href="assets/css/hs.megamenu.css">
	<link rel="stylesheet" href="assets/css/jquery.mCustomScrollbar.css">
	<link rel="stylesheet" href="assets/css/cubeportfolio.min.css">

	<!-- CSS Front Template -->
	<link rel="stylesheet" href="assets/css/front.css">
	<link rel="stylesheet" href="assets/css/linkvision.css">
	