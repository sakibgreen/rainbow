<!-- Go to Top -->
<a class="js-go-to u-go-to" href="#" data-position='{"bottom": 15, "right": 15 }' data-type="fixed" data-offset-top="400" data-compensation="#header" data-show-effect="slideInUp" data-hide-effect="slideOutDown">
		<span class="fa fa-arrow-up u-go-to__inner"></span>
	</a>
	<!-- End Go to Top -->

	<!-- JS Global Compulsory -->
	<script src="assets/js/jquery.min.js"></script>
	<script src="assets/js/jquery-migrate.min.js"></script>
	<script src="assets/js/popper.min.js"></script>
	<script src="assets/js/bootstrap.min.js"></script>

	<!-- JS Implementing Plugins -->
	<script src="assets/js/hs.megamenu.js"></script>
	<script src="assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
	<script src="assets/js/jquery.validate.min.js"></script>
	<script src="assets/js/jquery.fancybox.min.js"></script>
	<script src="assets/vendor/cubeportfolio/js/jquery.cubeportfolio.min.js"></script>
	<script src="assets/js/slick.js"></script>

	<!-- JS Front -->
	<script src="assets/js/hs.core.js"></script>
	<script src="assets/js/hs.header.js"></script>
	<script src="assets/js/hs.unfold.js"></script>
	<script src="assets/js/hs.focus-state.js"></script>
	<script src="assets/js/hs.malihu-scrollbar.js"></script>
	<script src="assets/js/hs.validation.js"></script>
	<script src="assets/js/hs.fancybox.js"></script>
	<script src="assets/js/hs.slick-carousel.js"></script>
	<script src="assets/js/hs.cubeportfolio.js"></script>
	<script src="assets/js/hs.focus-state.js"></script>
	<script src="assets/js/hs.instagram.js"></script>
	<script src="assets/js/hs.go-to.js"></script>
	<script src="assets/js/hs.show-animation.js"></script>

	<!-- JS Plugins Init. -->
	<script src="assets/js/jquery.countup.js"></script>
	<script src="assets/js/jquery.waypoints.min.js"></script>
	<script>
		$(window).on('load', function() {
			// initialization of HSMegaMenu component
			$('.js-mega-menu').HSMegaMenu({
				event: 'hover',
				pageContainer: $('.container'),
				breakpoint: 767,
				hideTimeOut: 0
			});
		});

		$(document).on('ready', function() {
			// initialization of header
			$.HSCore.components.HSHeader.init($('#header'));

			// initialization of unfold component
			$.HSCore.components.HSUnfold.init($('[data-unfold-target]'), {
				afterOpen: function() {
					$(this).find('input[type="search"]').focus();
				}
			});

			// initialization of malihu scrollbar
			$.HSCore.components.HSMalihuScrollBar.init($('.js-scrollbar'));

			$.HSCore.components.HSCubeportfolio.init('.cbp');

			// initialization of forms
			$.HSCore.helpers.HSFocusState.init();

			// initialization of form validation
			$.HSCore.components.HSValidation.init('.js-validate', {
				rules: {
					confirmPassword: {
						equalTo: '#password'
					}
				}
			});

			// initialization of show animations
			$.HSCore.components.HSShowAnimation.init('.js-animation-link');

			// initialization of fancybox
			$.HSCore.components.HSFancyBox.init('.js-fancybox');

			// initialization of text animation (typing)
			var typed = new Typed(".u-text-animation.u-text-animation--typing", {
				strings: ["more professional.", "perfect in every way.", "astonishing."],
				typeSpeed: 60,
				loop: true,
				backSpeed: 25,
				backDelay: 1500
			});
			$.HSCore.components.HSInstagram.init('#instaFeed', {
				after: function() {
					// initialization of masonry.js
					var $grid = $('.js-instagram').masonry({
						percentPosition: true
					});

					// initialization of images loaded
					$grid.imagesLoaded().progress(function() {
						$grid.masonry();
					});
				}
			});

			// initialization of slick carousel
			$.HSCore.components.HSSlickCarousel.init('.js-slick-carousel');

			// initialization of go to
			$.HSCore.components.HSGoTo.init('.js-go-to');
		});
	</script>

	<!--client chart js-->
	<script>
		$('.counter').countUp();
	</script>