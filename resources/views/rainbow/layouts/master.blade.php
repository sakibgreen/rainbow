<!DOCTYPE html>
<html lang="en">
  
<head>
	<!-- Title -->
	<title>Rainbow Clipping</title>

	<!-- Required Meta Tags Always Come First -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  @include('rainbow.layouts.includes.css')

</head>
<body>
	<!-- Skippy -->
	<a id="skippy" class="sr-only sr-only-focusable u-skippy" href="#content">
		<div class="container">
			<span class="u-skiplink-text">Skip to main content</span>
		</div>
	</a>
    <!-- End Skippy -->
    @include('rainbow.layouts.header')
      @yield('content')
    @include('rainbow.layouts.footer') 
    <!-- <div class="control-sidebar-bg"></div>
  </div> -->
  @include('rainbow.layouts.includes.js')


</body>
  

</html>
