	<!-- ========== HEADER ========== -->
	<header id="header" class="u-header u-header--abs-top-md u-header--bg-transparent u-header--show-hide-md" data-header-fix-moment="500" data-header-fix-effect="slide">
		<div class="u-header__section">
			<!-- Topbar -->
			<div class="container u-header__hide-content pt-2">
				<div class="d-flex align-items-center mb-0">
					<!-- Logo -->
					<a class="navbar-brand u-header__navbar-brand u-header__navbar-brand-top-space" href="{{ url('/') }}" aria-label="Front">
						<img src="assets/img/rnblogo.png" alt="Logo">
					</a> 
					<!-- End Logo -->
					<div class="ml-auto">
						<!-- Links -->
						<div class="d-none d-sm-inline-block ml-sm-auto">
							<ul class="list-inline mb-0">
								<li class="list-inline-item mr-0"><a class="google" href="https://aboutme.google.com/u/1/?referer=gplus" target="_blank"> info@rainbowclipping.design</a></li><br>
								<li class="list-inline-item mr-0"><a class="google" target="_blank"><i class="fa fa-contact">+8801877713277</i></a></li><br>
								<!-- Authentication Links -->
								@if (Auth::guest())
									<li><a href="{{ route('login') }}">Login</a></li>
								@else
									<li class="dropdown">
										<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
											{{ Auth::user()->name }} <span class="caret"></span>
										</a>

										<ul class="dropdown-menu" role="menu">
											<li>
													@role('Admin') {{-- Laravel-permission blade helper --}}
													<a href="{{ url('/backend') }}"><i class="fa fa-btn fa-unlock"></i>Admin</a>
													@endrole
													<a href="{{ route('logout') }}"
														onclick="event.preventDefault();
																document.getElementById('logout-form').submit();">
														Logout
													</a>

													<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
														{{ csrf_field() }}
													</form>
											</li>
										</ul>
									</li>
								@endif
							</ul>
						</div>
						<!-- End Links -->
					</div>

					<ul class="list-inline ml-2 mb-0">
						<!-- Account Login -->
						<li class="list-inline-item mr-0"><a class="twitter" href="https://twitter.com/rainbowclipping" target="_blank"><i class="fa fa-twitter"></i></a></li>
                        <li class="list-inline-item mr-0"><a class="linkedin" href="https://linkedin.com/rainbowclipping/" target="_blank"><i class="fa fa-linkedin"></i></a></li>
                        <li class="list-inline-item mr-0"><a class="facebook" href="http://www.facebook.com/rainbowclipping" target="_blank"><i class="fa fa-facebook"></i></a></li>
                        <li class="list-inline-item mr-0"><a class="pinterest" href="https://www.pinterest.com/sakibgreen/" target="_blank"><i class="fa fa-pinterest"></i></a></li>
						<!-- End Account Login -->
					</ul>
				</div>
			</div>
			<!-- End Topbar -->

			<div id="logoAndNav" class="container">
				<!-- Nav -->
				<nav class="js-mega-menu navbar navbar-expand-md u-header__navbar">
					<!-- Responsive Toggle Button -->
					<button type="button" class="navbar-toggler btn u-hamburger" aria-label="Toggle navigation" aria-expanded="false" aria-controls="navBar" data-toggle="collapse" data-target="#navBar">
						<span id="hamburgerTrigger" class="u-hamburger__box">
							<span class="u-hamburger__inner"></span>
						</span>
					</button>
					<!-- End Responsive Toggle Button -->

					<!-- Navigation -->
					<div id="navBar" class="collapse navbar-collapse py-0">
						<ul class="navbar-nav u-header__navbar-nav ml-lg-auto">
							<!-- Home -->
							<li class="nav-item">
								<a class="nav-link u-header__nav-link" href="{{ url('/') }}">
									Home
								</a>
							</li>
							<!-- End Home -->
							<!-- About us -->
							<li class="nav-item">
								<a class="nav-link u-header__nav-link" href="{{ url('/aboutus') }}">
									About Us
								</a>
							</li>
							<!-- End about us -->
							<!--services -->
							<li class="nav-item hs-has-mega-menu u-header__nav-item" data-event="hover" data-animation-in="slideInUp" data-animation-out="fadeOut" data-position="right">
								<a id="worksMegaMenu" class="nav-link u-header__nav-link" href="javascript:;" aria-haspopup="true" aria-expanded="false">
									Services
									<span class="fa fa-angle-down u-header__nav-link-icon"></span>
								</a>

								<!-- Works - Mega Menu -->
								<div class="hs-mega-menu w-100 u-header__sub-menu u-header__mega-menu-wrapper-v1" aria-labelledby="worksMegaMenu">
									<div class="row">
										<div class="col-sm-6 col-lg-3 mb-3 mb-lg-0">
											<strong class="d-block mb-2">Remove Background From Images</strong>

											<!-- Links -->
											<ul class="list-unstyled u-list">
												<li><a class="nav-link u-list__link py-2 px-0" href="{{ url('/clippingpath') }}">Clipping Path Service</a></li>
												<li><a class="nav-link u-list__link py-2 px-0" href="{{url('/imagemasking')}}">Image Masking Service</a></li>
												<li><a class="nav-link u-list__link py-2 px-0" href="{{url('/shadowcreation')}}">Shadow creations Service</a></li>
											</ul>
											<!-- End Links --> 
										</div>

										<div class="col-sm-6 col-lg-3 mb-3 mb-lg-0">
											<strong class="d-block mb-2">Professinal Image Retouching</strong>

											<!-- Links -->
											<ul class="list-unstyled u-list">
												<li><a class="nav-link u-list__link py-2 px-0" href="{{url('/ghost')}}">Ghost mannequin</a></li>
												<li><a class="nav-link u-list__link py-2 px-0" href="{{url('/wedding')}}">Wedding Photo Retouching</a></li>
												<li><a class="nav-link u-list__link py-2 px-0" href="{{url('/photoretouching')}}">Photo Retouching</a></li>
											</ul>
											<!-- End Links -->
										</div>

										<div class="col-sm-6 col-lg-3 mb-3 mb-sm-0">
											<strong class="d-block mb-2">Phography Post Production</strong>

											<!-- Links -->
											<ul class="list-unstyled u-list">
												<li><a class="nav-link u-list__link py-2 px-0" href="{{url('/product')}}">Product Photo Editing</a></li>
												<li><a class="nav-link u-list__link py-2 px-0" href="{{url('/realestate')}}">Real Estate Photo Editing</a></li>
												<li><a class="nav-link u-list__link py-2 px-0" href="{{url('/photocroping')}}">Photo resizing/cropping</a></li>
											</ul>
											<!-- End Links -->
										</div>

										<div class="col-sm-6 col-lg-3">
											<strong class="d-block mb-2">Creative Editing Services</strong>

											<!-- Links -->
											<ul class="list-unstyled u-list">
												<li><a class="nav-link u-list__link py-2 px-0" href="{{url('/vector')}}">Vector-Conversion-Illustration</a></li>
												<li><a class="nav-link u-list__link py-2 px-0" href="{{url('/color')}}">Colour correction</a></li>
												<li><a class="nav-link u-list__link py-2 px-0" href="{{url('/photorestoration')}}">Photo restoration</a></li>
												<li><a class="nav-link u-list__link py-2 px-0" href="{{url('/logodesign')}}">Logo Design</a></li>
											</ul>
											<!-- End Links -->
										</div>
									</div>
								</div>
								<!-- End Works - Mega Menu -->
							</li>
							<!-- End Works -->

							<!--Web services -->
							<li class="nav-item hs-has-mega-menu u-header__nav-item" data-event="hover" data-animation-in="slideInUp" data-animation-out="fadeOut" data-position="right">
								<a id="worksMegaMenu" class="nav-link u-header__nav-link" href="javascript:;" aria-haspopup="true" aria-expanded="false">
									Web Services
									<span class="fa fa-angle-down u-header__nav-link-icon"></span>
								</a>
								<!-- Works - Mega Menu -->
								<div class="hs-mega-menu w-100 u-header__sub-menu u-header__mega-menu-wrapper-v1" aria-labelledby="worksMegaMenu">
									<div class="row">
										
										<div class="col-sm-12 col-lg-6">
											<strong class="d-block mb-2"> Web Services</strong>

											<!-- Links -->
											<ul class="list-unstyled u-list">
												<li><a class="nav-link u-list__link py-2 px-0" href="{{url('/webdesign')}}">Web Design </a></li>
												<li><a class="nav-link u-list__link py-2 px-0" href="{{url('/apps')}}">App Development</a></li>
												<li><a class="nav-link u-list__link py-2 px-0" href="{{url('/software')}}">Application Development</a></li>
												<li><a class="nav-link u-list__link py-2 px-0" href="{{url('/ecommerce')}}">Digital Marketing</a></li>
											</ul>
											<!-- End Links -->
										</div>

									</div>
								</div>
								<!-- End Works - Mega Menu -->
							</li>
							<!-- End Web Works -->
							
							<!--gallaries-->
							<li class="nav-item">
								<a class="nav-link u-header__nav-link" href="{{ url('/gallery') }}">
									Galleries
								</a>
							</li>

							<!-- End gallaries -->
							<!-- Price -->
							<li class="nav-item">
								<a class="nav-link u-header__nav-link" href="{{ url('/pricing') }}">
									Price
								</a>
							</li>
							<!-- End Price-->

							<!-- Payment -->
							<li class="nav-item">
								<a class="nav-link u-header__nav-link" href="{{ url('/paypal') }}">
									Payment
								</a>
							</li>
							<!-- End payment -->
							<!--upload-->
							<li class="nav-item">
								<a class="nav-link u-header__nav-link" href="{{ url('/upload') }}">
									Upload Files
								</a>
							</li>
							<!--end of upload -->
							<!-- Button -->
							<li class="nav-item d-none d-md-inline-block pl-3 pr-0">
								<a class="btn btn-sm btn-primary u-btn-primary transition-3d-hove" href="{{ url('/freetrial') }}">
									Free Trial
								</a>
							</li>
							<li class="nav-item d-none d-md-inline-block pl-3 pr-0">
								<a class="btn btn-sm btn-primary u-btn-primary transition-3d-hove" href="{{ url('/quote') }}">
									Get Quote
								</a>
							</li>
							<!-- End Button -->
						</ul>
					</div>
					<!-- End Navigation -->
				</nav>
				<!-- End Nav -->
			</div>
		</div>
	</header>
	<!-- ========== END HEADER ========== -->  