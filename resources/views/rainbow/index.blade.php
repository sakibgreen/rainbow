<!DOCTYPE html>
<html lang="en"> 

<head>
	<!-- Title -->
	<title>Rainbow Clipping</title>

	<!-- Required Meta Tags Always Come First -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<!-- Favicon -->
	<link rel="shortcut icon" href="assets/img/fav.png">

	<!-- Google Fonts -->
	<link href="http://fonts.googleapis.com/css?family=Poppins:300,400,500,600" rel="stylesheet">

	<!-- CSS Global Compulsory -->
	<link rel="stylesheet" href="assets/css/bootstrap.css">

	<!-- CSS Implementing Plugins -->
	<link rel="stylesheet" href="assets/css/font-awesome.min.css">
	<link rel="stylesheet" href="assets/css/animate.min.css">
	<link rel="stylesheet" href="assets/css/hs.megamenu.css">
	<link rel="stylesheet" href="assets/css/jquery.mCustomScrollbar.css">
	<link rel="stylesheet" href="assets/css/slick.css">

	<!-- CSS Front Template -->
	<link rel="stylesheet" href="assets/css/front.css">
	<link rel="stylesheet" href="assets/css/linkvision.css">

	<!-- Fix Internet Explorer ______________________________________>
	<!--[if lt IE 9]>
			<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
			<script src="vendor/html5shiv.js"></script>
			<script src="vendor/respond.js"></script>
		<![endif]-->
</head>

<body>
	<!-- Skippy -->
	<a id="skippy" class="sr-only sr-only-focusable u-skippy" href="#content">
		<div class="container">
			<span class="u-skiplink-text">Skip to main content</span>
		</div>
	</a>
	<!-- End Skippy -->

	<!-- ========== HEADER ========== -->
	<header id="header" class="u-header u-header--abs-top-md u-header--bg-transparent u-header--show-hide-md" data-header-fix-moment="500" data-header-fix-effect="slide">
		<div class="u-header__section">
			<!-- Topbar -->
			<div class="container u-header__hide-content pt-2">
				<div class="d-flex align-items-center mb-0">
					<!-- Logo -->
					<a class="navbar-brand u-header__navbar-brand u-header__navbar-brand-top-space" href="{{ url('/') }}" aria-label="Front">
						<img src="assets/img/rnblogo.png" alt="Logo">
					</a> 
					<!-- End Logo -->
					<div class="ml-auto">
						<!-- Links -->
						<div class="d-none d-sm-inline-block ml-sm-auto">
							<ul class="list-inline mb-0">
								<!-- Authentication Links -->
								<li class="list-inline-item mr-0"><a class="google" href="https://aboutme.google.com/u/1/?referer=gplus" target="_blank"> info@rainbowclipping.design</a></li><br>
								<li class="list-inline-item mr-0"><a class="google" target="_blank"><i class="fa fa-contact">+8801877713277</i></a></li><br>
								@if (Auth::guest())
									<li><a href="{{ route('login') }}">Login</a></li> 
								@else
									<li class="dropdown">
										<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
											{{ Auth::user()->name }} <span class="caret"></span>
										</a>

										<ul class="dropdown-menu" role="menu">
											<li>
													@role('Admin') {{-- Laravel-permission blade helper --}}
													<a href="{{ url('/backend') }}"><i class="fa fa-btn fa-unlock"></i>Admin</a>
													@endrole
													<a href="{{ route('logout') }}"
														onclick="event.preventDefault();
																document.getElementById('logout-form').submit();">
														Logout
													</a>

													<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
														{{ csrf_field() }}
													</form>
											</li>
										</ul>
									</li>
								@endif
							</ul>
						</div>
						<!-- End Links -->
					</div>

					<ul class="list-inline ml-2 mb-0">
						<!-- Account Login -->
						<li class="list-inline-item mr-0"><a class="twitter" href="https://twitter.com/rainbowclipping" target="_blank"><i class="fa fa-twitter"></i></a></li>
                        <li class="list-inline-item mr-0"><a class="linkedin" href="https://linkedin.com/rainbowclipping/" target="_blank"><i class="fa fa-linkedin"></i></a></li>
                        <li class="list-inline-item mr-0"><a class="facebook" href="http://www.facebook.com/rainbowclipping" target="_blank"><i class="fa fa-facebook"></i></a></li>
                        <li class="list-inline-item mr-0"><a class="pinterest" href="https://www.pinterest.com/sakibgreen/" target="_blank"><i class="fa fa-pinterest"></i></a></li>
						<!-- End Account Login -->
					</ul>
				</div>
			</div>
			<!-- End Topbar -->

			<div id="logoAndNav" class="container">
				<!-- Nav -->
				<nav class="js-mega-menu navbar navbar-expand-md u-header__navbar">
					<!-- Responsive Toggle Button -->
					<button type="button" class="navbar-toggler btn u-hamburger" aria-label="Toggle navigation" aria-expanded="false" aria-controls="navBar" data-toggle="collapse" data-target="#navBar">
						<span id="hamburgerTrigger" class="u-hamburger__box">
							<span class="u-hamburger__inner"></span>
						</span>
					</button>
					<!-- End Responsive Toggle Button -->

					<!-- Navigation -->
					<div id="navBar" class="collapse navbar-collapse py-0">
						<ul class="navbar-nav u-header__navbar-nav ml-lg-auto">
							<!-- Home -->
							<li class="nav-item">
								<a class="nav-link u-header__nav-link" href="{{ url('/') }}">
									Home
								</a>
							</li>
							<!-- End Home -->
							<!-- About us -->
							<li class="nav-item">
								<a class="nav-link u-header__nav-link" href="{{ url('/aboutus') }}">
									About Us
								</a>
							</li>
							<!-- End about us -->

							<!--services -->
							<li class="nav-item hs-has-mega-menu u-header__nav-item" data-event="hover" data-animation-in="slideInUp" data-animation-out="fadeOut" data-position="right">
								<a id="worksMegaMenu" class="nav-link u-header__nav-link" href="javascript:;" aria-haspopup="true" aria-expanded="false">
									Services
									<span class="fa fa-angle-down u-header__nav-link-icon"></span>
								</a>

								<!-- Works - Mega Menu -->
								<div class="hs-mega-menu w-100 u-header__sub-menu u-header__mega-menu-wrapper-v1" aria-labelledby="worksMegaMenu">
									<div class="row">
										<div class="col-sm-6 col-lg-3 mb-3 mb-lg-0">
											<strong class="d-block mb-2">Remove Background From Images</strong>

											<!-- Links -->
											<ul class="list-unstyled u-list">
												<li><a class="nav-link u-list__link py-2 px-0" href="{{ url('/clippingpath') }}">Clipping Path Service</a></li>
												<li><a class="nav-link u-list__link py-2 px-0" href="{{url('/imagemasking')}}">Image Masking Service</a></li>
												<li><a class="nav-link u-list__link py-2 px-0" href="{{url('/shadowcreation')}}">Shadow creations Service</a></li>
											</ul>
											<!-- End Links --> 
										</div>

										<div class="col-sm-6 col-lg-3 mb-3 mb-lg-0">
											<strong class="d-block mb-2">Professinal Image Retouching</strong>

											<!-- Links -->
											<ul class="list-unstyled u-list">
												<li><a class="nav-link u-list__link py-2 px-0" href="{{url('/ghost')}}">Ghost mannequin</a></li>
												<li><a class="nav-link u-list__link py-2 px-0" href="{{url('/wedding')}}">Wedding Photo Retouching</a></li>
												<li><a class="nav-link u-list__link py-2 px-0" href="{{url('/photoretouching')}}">Photo Retouching</a></li>
											</ul>
											<!-- End Links -->
										</div>

										<div class="col-sm-6 col-lg-3 mb-3 mb-sm-0">
											<strong class="d-block mb-2">Phography Post Production</strong>

											<!-- Links -->
											<ul class="list-unstyled u-list">
												<li><a class="nav-link u-list__link py-2 px-0" href="{{url('/product')}}">Product Photo Editing</a></li>
												<li><a class="nav-link u-list__link py-2 px-0" href="{{url('/realestate')}}">Real Estate Photo Editing</a></li>
												<li><a class="nav-link u-list__link py-2 px-0" href="{{url('/photocroping')}}">Photo resizing/cropping</a></li>
											</ul>
											<!-- End Links -->
										</div>

										<div class="col-sm-6 col-lg-3">
											<strong class="d-block mb-2">Creative Editing Services</strong>

											<!-- Links -->
											<ul class="list-unstyled u-list">
												<li><a class="nav-link u-list__link py-2 px-0" href="{{url('/vector')}}">Vector-Conversion-Illustration</a></li>
												<li><a class="nav-link u-list__link py-2 px-0" href="{{url('/color')}}">Colour correction</a></li>
												<li><a class="nav-link u-list__link py-2 px-0" href="{{url('/photorestoration')}}">Photo restoration</a></li>
												<li><a class="nav-link u-list__link py-2 px-0" href="{{url('/logodesign')}}">Logo Design</a></li>
											</ul>
											<!-- End Links -->
										</div>
									</div>
								</div>
								<!-- End Works - Mega Menu -->
							</li>
							<!-- End Works -->

							<!--Web services -->
							<li class="nav-item hs-has-mega-menu u-header__nav-item" data-event="hover" data-animation-in="slideInUp" data-animation-out="fadeOut" data-position="right">
								<a id="worksMegaMenu" class="nav-link u-header__nav-link" href="javascript:;" aria-haspopup="true" aria-expanded="false">
									Web Services
									<span class="fa fa-angle-down u-header__nav-link-icon"></span>
								</a>
								<!-- Works - Mega Menu -->
								<div class="hs-mega-menu w-100 u-header__sub-menu u-header__mega-menu-wrapper-v1" aria-labelledby="worksMegaMenu">
									<div class="row">
										
										<div class="col-sm-12 col-lg-6">
											<strong class="d-block mb-2"> Web Services</strong>

											<!-- Links -->
											<ul class="list-unstyled u-list">
												<li><a class="nav-link u-list__link py-2 px-0" href="{{url('/webdesign')}}">Web Design </a></li>
												<li><a class="nav-link u-list__link py-2 px-0" href="{{url('/apps')}}">App Development</a></li>
												<li><a class="nav-link u-list__link py-2 px-0" href="{{url('/software')}}">Application Development</a></li>
												<li><a class="nav-link u-list__link py-2 px-0" href="{{url('/ecommerce')}}">Digital Marketing</a></li>
											</ul>
											<!-- End Links -->
										</div>

									</div>
								</div>
								<!-- End Works - Mega Menu -->
							</li>
							<!-- End Web Works -->
							
							<!--gallaries-->
							<li class="nav-item">
								<a class="nav-link u-header__nav-link" href="{{ url('/gallery') }}">
									Galleries
								</a>
							</li>

							<!-- End gallaries -->
							<!-- Price -->
							<li class="nav-item">
								<a class="nav-link u-header__nav-link" href="{{ url('/pricing') }}">
									Price
								</a>
							</li>
							<!-- End Price-->

							<!-- Payment -->
							<li class="nav-item">
								<a class="nav-link u-header__nav-link" href="{{ url('/paypal') }}">
									Payment
								</a>
							</li>
							<!-- End payment -->
							<!--upload-->
							<li class="nav-item">
								<a class="nav-link u-header__nav-link" href="{{ url('/upload') }}">
									Upload Files
								</a>
							</li>
							<!--end of upload -->
							<!-- Button -->
							<li class="nav-item d-none d-md-inline-block pl-3 pr-0">
								<a class="btn btn-sm btn-primary u-btn-primary transition-3d-hove" href="{{ url('/freetrial') }}">
									Free Trial
								</a>
							</li>
							<li class="nav-item d-none d-md-inline-block pl-3 pr-0">
								<a class="btn btn-sm btn-primary u-btn-primary transition-3d-hove" href="{{ url('/quote') }}">
									Get Quote
								</a>
							</li>
							<!-- End Button -->
						</ul>
					</div>
					<!-- End Navigation -->
				</nav>
				<!-- End Nav -->
			</div>
		</div>
	</header>
	<!-- ========== END HEADER ========== -->

	<!-- ========== MAIN CONTENT ========== -->
	<main id="content" role="main">
		<!-- Hero Section -->
		<div class="Modern-Slider">
			<!-- Item -->
			<div class="item">
				<div class="img-fill">
					<img src="assets/img/1920x800/slider1.jpg" alt="">
					<div class="info">
						<div>
							<h3>Welcome to our image editing service<h3>
						</div>
					</div>
				</div>
			</div>
			<!-- // Item -->
			<!-- Item -->
			<div class="item">
				<div class="img-fill">
					<img src="assets/img/1920x800/slider2.png" alt="">
					<div class="info">
						<div>
							<h3> E-commerce photo editing service</h3>
   							<h3>Price start from $0.25 per image<h3>
						</div>
					</div>
				</div>
			</div>
			<!-- // Item -->
			<!-- Item -->
			<div class="item">
				<div class="img-fill">
					<img src="assets/img/1920x800/slider3.png" alt="">
					<div class="info">
						<div>
							<h3> Shadow creation service With best quality and lowest price<h3>
						</div>
					</div>
				</div>
			</div>
			<!-- // Item -->
			<!-- Item -->
			<div class="item">
				<div class="img-fill">
					<img src="assets/img/1920x800/slider4.png" alt="">
					<div class="info">
						<div>
							<h3>Ghost manniquin...</h3>
							<h3> You will get the special price for bulk images</h3>
						</div>
					</div>
				</div>
			</div>
			<!-- // Item -->
			<!-- Item -->
			<div class="item">
				<div class="img-fill">
					<img src="assets/img/1920x800/slider5.png" alt="">
					<div class="info">
						<div>
							<h3>Hair Masking Let's start with $0.45<h3>
						</div>
					</div>
				</div>
			</div>
			<!-- // Item -->
			<!-- Item -->
			<div class="item">
				<div class="img-fill">
					<img src="assets/img/1920x800/slider7.png" alt="">
					<div class="info">
						<div>
							<h3>Photo retouching service It's our popular service... start from $0.49</h3>
						</div>
					</div>
				</div>
			</div>
			<!-- // Item -->
			<!-- Item -->
			<div class="item">
				<div class="img-fill">
					<img src="assets/img/1920x800/slider6.png" alt="">
					<div class="info">
						<div>
						<h3>Photo resizing Big quantity <br> <strong>Low price<strong> <h3>							
						</div>
					</div>
				</div>
			</div>
			<!-- // Item -->
			<!-- Item -->
			<div class="item">
				<div class="img-fill">
					<img src="assets/img/1920x800/slider8.jpg" alt="">
					<div class="info">
						<div>
							<h3> Realstat photo editing <h3>							
						</div>
					</div>
				</div>
			</div>
			<!-- // Item -->
		</div>
		<!-- End Hero Section -->
		<!-- Icon Blocks Section -->
		<div class="container u-space-1" id="imagepro">
			<!--popular service start-->
			<div class="u-gradient-half-primary-v2">
				<div class="container u-space-1 px-lg-7">
					<!-- Title -->
					<div class="w-md-80 w-lg-100 text-center mx-auto">
						<h2>Our Popular Services</h2>
						<p>Rainbow Clipping has been providing best quality professional photo editing services for customers. With a perfect blend of experience, skill, dedication and punctuality.</p>
					</div>
					<!-- End Title -->

					<!-- News Carousel -->
					<div class="js-slick-carousel u-slick u-slick--equal-height u-slick--gutters-3" data-slides-show="3" data-slides-scroll="3" data-arrows-classes="d-none d-lg-inline-block u-slick__arrow u-slick__arrow--offset u-slick__arrow-centered--y rounded-circle" data-arrow-left-classes="fa fa-arrow-left u-slick__arrow-inner u-slick__arrow-inner--left" data-arrow-right-classes="fa fa-arrow-right u-slick__arrow-inner u-slick__arrow-inner--right" data-pagi-classes="text-center u-slick__pagination mt-7 mb-0" data-responsive='[{
						"breakpoint": 992,
						"settings": {
							"slidesToShow": 2
						}
						}, {
						"breakpoint": 768,
						"settings": {
							"slidesToShow": 1
						}
						}, {
						"breakpoint": 554,
						"settings": {
							"slidesToShow": 1
						}
						}]'>
						<!-- service part one -->
						<a class="js-slide u-bg-img-hero u-gradient-overlay-half-dark-v1 transition-3d-hover rounded-pseudo my-4" style="background-image: url('assets/img/remove/before.png');">
							<article class="align-self-end w-100 text-center p-6">
								<h3 class="fontservice"></h3>
								<div class="mt-4">
									<strong class="fontservice">Before</strong>
								</div>
							</article>
						</a>
						<!--  end of before -->
						<!-- content -->
						<div class="js-slide position-relative bg-white shadow-sm transition-3d-hover rounded my-4">
							<article class="align-self-end w-100 text-center p-6">
								<h5> <strong class="d-block u-text-light mb-2"> Image Background Removing</strong> </h5> <br>
								<h3 class="h6 font-weight-normal ">" Rainbow Clippng provides background remove service with clipping paths for removing the unwanted background and object from a product image and to replace a new white or colored background. "</h3>
								<div class="my-4"></div>
								<a class="btn btn-sm u-btn-primary--air u-btn-wide transition-3d-hover" href="{{url('/clippingpath')}}">Read More</a>
							</article>
						</div>
						<!-- End content -->
						<!-- after -->
						<a class="js-slide u-bg-img-hero u-gradient-overlay-half-dark-v1 transition-3d-hover rounded-pseudo my-4" style="background-image: url('assets/img/remove/after.png');">
							<article class="align-self-end w-100 text-center p-6">
								<h3 class="fontservice"></h3>
								<div class="mt-4">
									<strong class="fontservice">After</strong>
								</div>
							</article>
						</a>
						<!-- End part one -->

						<!-- service part two -->
						<a class="js-slide u-bg-img-hero u-gradient-overlay-half-dark-v1 transition-3d-hover rounded-pseudo my-4" style="background-image: url('assets/img/restroration/before.png');">
							<article class="align-self-end w-100 text-center p-6">
								<h3 class="fontservice"></h3>
								<div class="mt-4">
									<strong class="fontservice">Before</strong>
								</div>
							</article>
						</a>
						<!-- End after -->
						<!--content-->
						<div class="js-slide position-relative bg-white shadow-sm transition-3d-hover rounded my-4">
							<article class="align-self-end w-100 text-center p-6">
								<strong class="d-block u-text-light mb-2">Image Restroration</strong>
								<h3 class="h6 font-weight-normal">"Digital images can be stored for several years in the form of soft copy, but it is not possible to store images in bromide or printed form. Because these images were printed on special paper coating on which there is a mixture of ink and chemicals "</h3>
								<div class="my-4"></div>
								<a class="btn btn-sm u-btn-primary--air u-btn-wide transition-3d-hover" href="{{url('/photorestoration')}}">Read More</a>
							</article>
						</div>
						<!-- End content -->
						<!-- before -->
						<a class="js-slide u-bg-img-hero u-gradient-overlay-half-dark-v1 transition-3d-hover rounded-pseudo my-4" style="background-image: url('assets/img/restroration/after.png');">
							<article class="align-self-end w-100 text-center p-6">
								<h3 class="fontservice"></h3>
								<div class="mt-4">
									<strong class="fontservice">After</strong>
								</div>
							</article>
						</a>
						<!-- end part two -->

						<!-- sevice part three -->
						<a class="js-slide u-bg-img-hero u-gradient-overlay-half-dark-v1 transition-3d-hover rounded-pseudo my-4" style="background-image: url('assets/img/clipping/before.png');">
							<article class="align-self-end w-100 text-center p-6">
								<h3 class="fontservice"></h3>
								<div class="mt-4">
									<strong class="fontservice">Before</strong>
								</div>
							</article>
						</a>
						<!-- End before -->
						<!-- content -->
						<div class="js-slide position-relative bg-white shadow-sm transition-3d-hover rounded my-4">
							<article class="align-self-end w-100 text-center p-6">
								<strong class="d-block u-text-light mb-2">Clipping Path</strong>
								<h3 class="h6 font-weight-normal">" Clipping path service is an Adobe Photoshop Pen tool based photo manipulation service which is widely used to remove image background "</h3>
								<div class="my-4"></div>
								<a class="btn btn-sm u-btn-primary--air u-btn-wide transition-3d-hover" href="{{url('/clippingpath')}}">Read More</a>
							</article>
						</div>
						<!-- End content -->
						<!-- after -->
						<a class="js-slide u-bg-img-hero u-gradient-overlay-half-dark-v1 transition-3d-hover rounded-pseudo my-4" style="background-image: url('assets/img/clipping/after.png');">
							<article class="align-self-end w-100 text-center p-6">
								<h3 class="h4 text-white"></h3>
								<div class="mt-4">
									<strong class="fontservice">After</strong>
								</div>
							</article>
						</a>
						<!-- End part three -->

						<!-- service part four -->
						<a class="js-slide u-bg-img-hero u-gradient-overlay-half-dark-v1 transition-3d-hover rounded-pseudo my-4" style="background-image: url('assets/img/shadow/before.png');">
							<article class="align-self-end w-100 text-center p-6">
								<h3 class="h4 text-white"></h3>
								<div class="mt-4">
									<strong class="fontservice">Before</strong>
								</div>
							</article>
						</a>
						<!-- End Before -->
						<!-- content -->
						<div class="js-slide position-relative bg-white shadow-sm transition-3d-hover rounded my-4">
							<article class="align-self-end w-100 text-center p-6">
								<strong class="d-block u-text-light mb-2">Photo Shadow</strong>
								<h3 class="h6 font-weight-normal">" Photoshop shadow service is one of the most important image manipulation services. There are some products which look unusual and unattractive "</h3>
								<div class="my-4">
								</div>
								<a class="btn btn-sm u-btn-primary--air u-btn-wide transition-3d-hover" href="{{url('/shadowcreation')}}">Read More</a>
							</article>
						</div>
						<!-- End content -->
						<!-- after -->
						<a class="js-slide u-bg-img-hero u-gradient-overlay-half-dark-v1 transition-3d-hover rounded-pseudo my-4" style="background-image: url('assets/img/shadow/after.png');">
							<article class="align-self-end w-100 text-center p-6">
								<h3 class="h4 text-white"></h3>
								<div class="mt-4">
									<strong class="fontservice">After</strong>
								</div>
							</article>
						</a>
						<!-- End part four -->

						<!-- service part five -->
						<a class="js-slide u-bg-img-hero u-gradient-overlay-half-dark-v1 transition-3d-hover rounded-pseudo my-4" style="background-image: url('assets/img/colorcorrection/before.png');">
							<article class="align-self-end w-100 text-center p-6">
								<h3 class="h4 text-white"></h3>
								<div class="mt-4">
									<strong class="fontservice">Before</strong>
								</div>
							</article>
						</a>
						<!-- End Blog News -->
						<!-- Blog News -->
						<div class="js-slide position-relative bg-white shadow-sm transition-3d-hover rounded my-4">
							<article class="align-self-end w-100 text-center p-6">
							<strong class="d-block u-text-light mb-2">Photoshop Color Correcton</strong>
								<h3 class="h6 font-weight-normal">" Color correction is one of the most popular and essential Photoshop based editing services. This service is applied to different types of photography like model photography, fashion photography, natural photography etc. "</h3>
								<div class="my-4">
								</div>
								<a class="btn btn-sm u-btn-primary--air u-btn-wide transition-3d-hover" href="{{url('/color')}}">Read More</a>
							</article>
						</div>
						<!-- End Blog News -->
						<!-- Blog News -->
						<a class="js-slide u-bg-img-hero u-gradient-overlay-half-dark-v1 transition-3d-hover rounded-pseudo my-4" style="background-image: url('assets/img/colorcorrection/after.png');">
							<article class="align-self-end w-100 text-center p-6">
								<h3 class="h4 text-white"></h3>
								<div class="mt-4">
									<strong class="fontservice">After</strong>
								</div>
							</article>
						</a>
						<!-- End part five -->

						<!-- service part six -->
						<a class="js-slide u-bg-img-hero u-gradient-overlay-half-dark-v1 transition-3d-hover rounded-pseudo my-4" style="background-image: url('assets/img/ghost/before.png');">
							<article class="align-self-end w-100 text-center p-6">
								<h3 class="h4 text-white"></h3>
								<div class="mt-4">
									<strong class="fontservice">Before</strong>
								</div>
							</article>
						</a>
						<!-- before -->
						<!-- content -->
						<div class="js-slide position-relative bg-white shadow-sm transition-3d-hover rounded my-4">
							<article class="align-self-end w-100 text-center p-6">
							<strong class="d-block u-text-light mb-2">Ghost Mannequin Service</strong>
								<h3 class="h6 font-weight-normal">"Ghost Mannequin (Neck joint) is most commonly used product photo editing service. It is mainly used in garments item."</h3>
								<div class="my-4">
								</div>
								<a class="btn btn-sm u-btn-primary--air u-btn-wide transition-3d-hover" href="{{url('/ghost')}}">Read More</a>
							</article>
						</div>
						<!-- End content -->
						<!-- after -->
						<a class="js-slide u-bg-img-hero u-gradient-overlay-half-dark-v1 transition-3d-hover rounded-pseudo my-4" style="background-image: url('assets/img/ghost/after.png');">
							<article class="align-self-end w-100 text-center p-6">
								<h3 class="h4 text-white"></h3>
								<div class="mt-4">
									<strong class="fontservice">After</strong>
								</div>
							</article>
						</a>
						<!-- End part six -->

						<!-- service part seven -->
						<a class="js-slide u-bg-img-hero u-gradient-overlay-half-dark-v1 transition-3d-hover rounded-pseudo my-4" style="background-image: url('assets/img/masking/before.png');">
							<article class="align-self-end w-100 text-center p-6">
								<h3 class="h4 text-white"></h3>
								<div class="mt-4">
									<strong class="fontservice">Before</strong>
								</div>
							</article>
						</a>
						<!-- End before -->
						<!-- content -->
						<div class="js-slide position-relative bg-white shadow-sm transition-3d-hover rounded my-4">
							<article class="align-self-end w-100 text-center p-6">
							<strong class="d-block u-text-light mb-2">Image Masking Services</strong>			
								<h3 class="h6 font-weight-normal">" To achieve greater perfection in extracting things like hair and fur from its background, Advanced or Complex Layer Masking is the best solution. "</h3>
								<div class="my-4">
								</div>
								<a class="btn btn-sm u-btn-primary--air u-btn-wide transition-3d-hover" href="{{url('/imagemasking')}}">Read More</a>
							</article>
						</div>
						<!-- End content -->
						<!-- after -->
						<a class="js-slide u-bg-img-hero u-gradient-overlay-half-dark-v1 transition-3d-hover rounded-pseudo my-4" style="background-image: url('assets/img/masking/after.png');">
							<article class="align-self-end w-100 text-center p-6">
								<h3 class="h4 text-white"></h3>
								<div class="mt-4">
									<strong class="fontservice">After</strong>
								</div>
							</article>
						</a>
						<!-- End part seven -->

						<!-- service part eight -->
						<a class="js-slide u-bg-img-hero u-gradient-overlay-half-dark-v1 transition-3d-hover rounded-pseudo my-4" style="background-image: url('assets/img/wedding/before.png');">
							<article class="align-self-end w-100 text-center p-6">
								<h3 class="h4 text-white"></h3>
								<div class="mt-4">
									<strong class="fontservice">Before</strong>
								</div>
							</article>
						</a>
						<!-- End Before -->
						<!-- Blog News -->
						<div class="js-slide position-relative bg-white shadow-sm transition-3d-hover rounded my-4">
							<article class="align-self-end w-100 text-center p-6">
							<strong class="d-block u-text-light mb-2">Wedding Photo Retouching</strong>
								<h3 class="h6 font-weight-normal">" A wedding is an important life event of everyone’s life. So, the photos of such an event are expected to be gorgeous and glamorous. When you get all the photos from your wedding photographer after the wedding, you may need further retouching and photo editing to make your memories stand out."</h3>
								<div class="my-4">
								</div>
								<a class="btn btn-sm u-btn-primary--air u-btn-wide transition-3d-hover" href="{{url('/wedding')}}">Read More</a>
							</article>
						</div>
						<!-- End Blog News -->
						<!-- Blog News -->
						<a class="js-slide u-bg-img-hero u-gradient-overlay-half-dark-v1 transition-3d-hover rounded-pseudo my-4" style="background-image: url(assets/img/wedding/after.png);">
							<article class="align-self-end w-100 text-center p-6">
								<h3 class="h4 text-white"></h3>
								<div class="mt-4">
									<strong class="fontservice">After</strong>
								</div>
							</article>
						</a>

						<!-- service part eight -->
						<a class="js-slide u-bg-img-hero u-gradient-overlay-half-dark-v1 transition-3d-hover rounded-pseudo my-4" style="background-image: url('assets/img/logo/before.png');">
							<article class="align-self-end w-100 text-center p-6">
								<h3 class="h4 text-white"></h3>
								<div class="mt-4">
									<strong class="fontservice">Before</strong>
								</div>
							</article>
						</a>
						<!-- End Before -->
						<!-- Blog News -->
						<div class="js-slide position-relative bg-white shadow-sm transition-3d-hover rounded my-4">
							<article class="align-self-end w-100 text-center p-6">
							<strong class="d-block u-text-light mb-2">Vector Path Service</strong>
								<h3 class="h6 font-weight-normal">" A wedding is an important life event of everyone’s life. So, the photos of such an event are expected to be gorgeous and glamorous. When you get all the photos from your wedding photographer after the wedding, you may need further retouching and photo editing to make your memories stand out."</h3>
								<div class="my-4">
								</div>
								<a class="btn btn-sm u-btn-primary--air u-btn-wide transition-3d-hover" href="{{url('/logodesign')}}">Read More</a>
							</article>
						</div>
						<!-- End Blog News -->
						<!-- Blog News -->
						<a class="js-slide u-bg-img-hero u-gradient-overlay-half-dark-v1 transition-3d-hover rounded-pseudo my-4" style="background-image: url(assets/img/logo/after.png);">
							<article class="align-self-end w-100 text-center p-6">
								<h3 class="h4 text-white"></h3>
								<div class="mt-4">
									<strong class="fontservice">After</strong>
								</div>
							</article>
						</a>
						<!-- End part eight -->
					</div>
					<!-- End News Carousel -->
				</div>
			</div>
			<!--end of popular service-->

			<!-- Title -->
			<div class="w-md-80 w-lg-100 text-center mx-auto mb-9">
				<span class="u-label u-label--sm u-label--success mb-2">What we do?</span>
				<h2> Our Image Editing Working Process</h2>
				<p>Have a quick look of our services. <span class="font-weight-normal">This Visual Representation Will Help You To Get A Better Understanding Of Our Rainbow Clipping Photo Editing Service Process..</span></p>
			</div>
			<!-- End Title -->
			<!--Our image editing working process-->
			<div class="row">
				<div class="col-md-4 mb-7">
					<!-- Icon Blocks -->
					<div class="text-center px-lg-3">
						<span class="u-icon u-icon-danger--air u-icon--xl rounded-circle mb-7">
							<img src="assets/img/9.png" alt="">
						</span>
						<h3 class="h5">Request quote/ Free trial</h3>
					</div>
					<!-- End Icon Blocks -->
				</div>

				<div class="col-md-4 mb-7">
					<!-- Icon Blocks -->
					<div class="text-center px-lg-3">
						<span class="u-icon u-icon-danger--air u-icon--xl rounded-circle mb-7">
							<img src="assets/img/10.png" alt="">
						</span>
						<h3 class="h5">Get an Email </h3>
					</div>
					<!-- End Icon Blocks -->
				</div>

				<div class="col-md-4 mb-7">
					<!-- Icon Blocks -->
					<div class="text-center px-lg-3">
						<span class="u-icon u-icon-danger--air u-icon--xl rounded-circle mb-7">
							<img src="assets/img/11.png" alt="">
						</span>
						<h3 class="h5">Upload files</h3>
					</div>
					<!-- End Icon Blocks -->
				</div>

				<div class="col-md-4 mb-7">
					<!-- Icon Blocks -->
					<div class="text-center px-lg-3">
						<span class="u-icon u-icon-danger--air u-icon--xl rounded-circle mb-7">
							<img src="assets/img/12.png" alt="">
						</span>
						<h3 class="h5"> Turnaround time</h3>
					</div>
					<!-- End Icon Blocks -->
				</div>

				<div class="col-md-4 mb-7">
					<!-- Icon Blocks -->
					<div class="text-center px-lg-3">
						<span class="u-icon u-icon-danger--air u-icon--xl rounded-circle mb-7">
							<img src="assets/img/13.png" alt="">
						</span>
						<h3 class="h5"> File delivery</h3>
					</div>
					<!-- End Icon Blocks -->
				</div>

				<div class="col-md-4 mb-7">
					<!-- Icon Blocks -->
					<div class="text-center px-lg-3">
						<span class="u-icon u-icon-danger--air u-icon--xl rounded-circle mb-7">
							<img src="assets/img/14.png" alt="">
						</span>
						<h3 class="h5"> Make payment</h3>
					</div>
					<!-- End Icon Blocks -->
				</div>

			</div>
			<!--end of image editing part-->

			<!--   Service feature -->
			<div class="container u-space-1">

				<div class="container">
					<div class="w-md-80 w-lg-100 text-center mx-auto">
						<h2>Our Service Features</h2>
						<p>We have been providing professional image manipulation, photo retouching, and photo editing services for you. Here are the key features of our team to provide you the highest quality of work.</p>
					</div>
					<div class="work_inner row">
						<div class="col-lg-3">
							<div class="work_item">
								<i><img src="assets/img/1.png" alt=""></i>
								<a href="#">
									<h4>Easy Payment System</h4>
								</a>
								<p class="text-justify">Our payment system is secure and hassle free. Payment can be completed via PayPal or by using a bank account or check (for US).</p>
							</div>
						</div>
						<div class="col-lg-3">
							<div class="work_item">
								<i><img src="assets/img/2.png" alt=""></i>
								<a href="#">
									<h4>Secured File Transfer</h4>
								</a>
								<p class="text-justify">We use secure FTP such as Hightail, we-transfer, Dropbox which enable you to send files up to 500 GB. It is a quick and hassle-free system.</p>
							</div>
						</div>
						<div class="col-lg-3">
							<div class="work_item">
								<i><img src="assets/img/3.png" alt=""></i>
								<a href="#">
									<h4>High Volume Discount</h4>
								</a>
								<p class="text-justify">We offer amazing discount offer for a large volume of images. You can send sample images for free trial to judge our service & quality.</p>
							</div>
						</div>
						<div class="col-lg-3">
							<div class="work_item">
								<i><img src="assets/img/4.png" alt=""></i>
								<a href="#">
									<h4>Rush Service</h4>
								</a>
								<p class="text-justify">If you need edited images within a short time, our rush service is the best solution for you. Reasonable extra charge is applicable.
								</p>
							</div>
						</div>
					</div>
					<div class="work_inner row">
						<div class="col-lg-3">
							<div class="work_item">
								<i><img src="assets/img/5.png" alt=""></i>
								<a href="#">
									<h4>Most Competitive Price</h4>
								</a>
								<p class="text-justify">We know the value of your money and our work strategy is designed for the proper utilization of that. We offer competitive most prices.</p>
							</div>
						</div>
						<div class="col-lg-3">
							<div class="work_item">
								<i><img src="assets/img/6.png" alt=""></i>
								<a href="#">
									<h4>Our Order Processing</h4>
								</a>
								<p class="text-justify">We are capable of handling our order of images which may contain more than 5000 images. Get all the images within the required time.
								</p>
							</div>
						</div>
						<div class="col-lg-3">
							<div class="work_item">
								<i><img src="assets/img/7.png" alt=""></i>
								<a href="#">
									<h4>On Time Delivery</h4>
								</a>
								<p class="text-justify">Time is very much important for your project. For timely delivery, our experienced and skilled graphic designers work with full dedication.
								</p>
							</div>
						</div>
						<div class="col-lg-3">
							<div class="work_item">
								<i><img src="assets/img/8.png" alt=""></i>
								<a href="#">
									<h4>100% Quality Ensured</h4>
								</a>
								<p class="text-justify">3 step quality assurance process is conducted & clients’ instruction is fully followed to ensure the finest quality & glamorous images.
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- End service feature -->
			<!-- Pricing  Section start -->
			<div class="u-gradient-half-primary-v2">
				<div class="container  px-lg-7">

					<!-- End Title -->
					<div class="w-md-80 w-lg-100 text-center mx-auto mb-6">
						<h2>Prices For Our Services</h2>
						<p>We offer competitive price for professional image manipulation and retouching & photo editing services which is very much affordable.</p>
					</div>

					<!-- News Carousel -->
					<div class="js-slick-carousel u-slick u-slick--equal-height u-slick--gutters-3" data-slides-show="3" data-slides-scroll="1" data-arrows-classes="d-none d-lg-inline-block u-slick__arrow u-slick__arrow--offset u-slick__arrow-centered--y rounded-circle" data-arrow-left-classes="fa fa-arrow-left u-slick__arrow-inner u-slick__arrow-inner--left" data-arrow-right-classes="fa fa-arrow-right u-slick__arrow-inner u-slick__arrow-inner--right" data-pagi-classes="text-center u-slick__pagination mt-3 mb-0" data-responsive='[{
						"breakpoint": 992,
						"settings": {
							"slidesToShow": 2
						}
						}, {
						"breakpoint": 768,
						"settings": {
							"slidesToShow": 1
						}
						}, {
						"breakpoint": 554,
						"settings": {
							"slidesToShow": 1
						}
						}]'>
						<!-- Pricing chart 1 -->
						<div class="style">
							<div class="js-slide position-relative bg-white shadow-sm transition-3d-hover rounded my-8">
								<article class="align-self-end w-100 text-center p-6">

									<strong class="d-block ">Clipping Path Services</strong>
									<p>Start From</p>

									<ul class="cd-pricing-features pricing">
										<li>Basic  <strong style="color: black;">:$0.25</strong></li>
										<li>Simple  <strong style="color: black;">:$0.49 </strong></li>
										<li>Compound  <strong style="color: black;">:$0.99</strong></li>
										<li>Complex  <strong style="color: black;">:$6.90</strong></li>
									</ul>
									<a class="btn btn-sm u-btn-primary--air u-btn-wide transition-3d-hover" href="">Read More</a>
								</article>
							</div>
						</div>
						<!-- End of one -->

						<!-- pricing two -->
						<div class="style">
							<div class="js-slide position-relative bg-white shadow-sm transition-3d-hover rounded my-8">
								<article class="align-self-end w-100 text-center p-6">

									<strong class="d-block ">Image Retouching Services</strong>
									<p>Start From</p>

									<ul class="cd-pricing-features pricing">
										<li>Basic  <strong style="color: black;">:$0.49</strong></li>
										<li>Simple  <strong style="color: black;">:$1.25 </strong></li>
										<li>Compound  <strong style="color: black;">:$2.25</strong></li>
										<li>Complex  <strong style="color: black;">:$4.00</strong></li>
									</ul>
									<a class="btn btn-sm u-btn-primary--air u-btn-wide transition-3d-hover" href="">Read More</a>
								</article>
							</div>
						</div>
						<!-- End two -->

						<!-- pricing three -->
						<div class="style">
							<div class="js-slide position-relative bg-white shadow-sm transition-3d-hover rounded my-8">
								<article class="align-self-end w-100 text-center p-6">

									<strong class="d-block ">Ghost Mannequin Effects</strong>
									<p>Start From</p>

									<ul class="cd-pricing-features pricing">
										<li>Basic  <strong style="color: black;">:$0.49</strong></li>
										<li>Simple  <strong style="color: black;">:$1.25 </strong></li>
										<li>Compound  <strong style="color: black;">:$1.75</strong></li>
										<li>Complex  <strong style="color: black;">:$2.50</strong></li>
									</ul>
									<a class="btn btn-sm u-btn-primary--air u-btn-wide transition-3d-hover" href="">Read More</a>
								</article>
							</div>
						</div>
						<!-- End three -->

						<!-- pricing four -->
						<div class="style">
							<div class="js-slide position-relative bg-white shadow-sm transition-3d-hover rounded my-8">
								<article class="align-self-end w-100 text-center p-6">

									<strong class="d-block ">Color Correction Services</strong>
									<p>Start From</p>

									<ul class="cd-pricing-features pricing">
										<li>Basic  <strong style="color: black;">:$0.39</strong></li>
										<li>Simple  <strong style="color: black;">:$0.59 </strong></li>
										<li>Compound  <strong style="color: black;">:$1.30</strong></li>
										<li>Complex  <strong style="color: black;">:$2.75</strong></li>
									</ul>
									<a class="btn btn-sm u-btn-primary--air u-btn-wide transition-3d-hover" href="">Read More</a>
								</article>
							</div>
						</div>
						<!-- End four -->

						<!-- pricing five -->
						<div class="style">
							<div class="js-slide position-relative bg-white shadow-sm transition-3d-hover rounded my-8">
								<article class="align-self-end w-100 text-center p-6">

									<strong class="d-block ">Image Masking Services</strong>
									<p>Start From</p>

									<ul class="cd-pricing-features pricing">
										<li>Basic  <strong style="color: black;">:$0.75</strong></li>
										<li>Simple  <strong style="color: black;">:$1.20</strong></li>
										<li>Compound  <strong style="color: black;">:$1.59</strong></li>
										<li>Complex  <strong style="color: black;">:$2.25</strong></li>
									</ul>
									<a class="btn btn-sm u-btn-primary--air u-btn-wide transition-3d-hover" href="">Read More</a>
								</article>

							</div>
						</div>
						<!-- End of five -->
						<!-- pricing six -->
						<div class="style">
							<div class="js-slide position-relative bg-white shadow-sm transition-3d-hover rounded my-8">
								<article class="align-self-end w-100 text-center p-6">

									<strong class="d-block ">Image Restoration Services</strong>
									<p>Start From</p>

									<ul class="cd-pricing-features pricing">
										<li>Basic  <strong style="color: black;">:$2.75</strong></li>
										<li>Simple  <strong style="color: black;">:$4.99 </strong></li>
										<li>Compound  <strong style="color: black;">:$12.99</strong></li>
										<li>Complex  <strong style="color: black;">:$17.99</strong></li>
									</ul>
									<a class="btn btn-sm u-btn-primary--air u-btn-wide transition-3d-hover" href="">Read More</a>
								</article>
							</div>
						</div>
						<!-- End of six -->

						<!-- pricing seven -->

						<div class="style">
							<div class="js-slide position-relative bg-white shadow-sm transition-3d-hover rounded my-8">
								<article class="align-self-end w-100 text-center p-6">

									<strong class="d-block ">Logo Design Services</strong>
									<p>Start From</p>

									<ul class="cd-pricing-features pricing">
										<li>Basic  <strong style="color: black;">:$3.99</strong></li>
										<li>Simple  <strong style="color: black;">:$4.75 </strong></li>
										<li>Compound  <strong style="color: black;">:$6.59</strong></li>
										<li>Complex  <strong style="color: black;">:$10.00</strong></li>
									</ul>
									<a class="btn btn-sm u-btn-primary--air u-btn-wide transition-3d-hover" href="">Read More</a>
								</article>
							</div>
						</div>
						<!-- End seven -->
						<!-- pricing eight -->
						<div class="style">
							<div class="js-slide position-relative bg-white shadow-sm transition-3d-hover rounded my-8">
								<article class="align-self-end w-100 text-center p-6">

									<strong class="d-block ">Vector Drawing Services</strong>
									<p>Start From</p>

									<ul class="cd-pricing-features pricing">
										<li>Basic  <strong style="color: black;">:$2.29</strong></li>
										<li>Simple  <strong style="color: black;">:$3.25 </strong></li>
										<li>Compound  <strong style="color: black;">:$4.00</strong></li>
										<li>Complex  <strong style="color: black;">:$6.00</strong></li>
									</ul>
									<a class="btn btn-sm u-btn-primary--air u-btn-wide transition-3d-hover" href="">Read More</a>
								</article>
							</div>
						</div>
						<!-- End of eight -->

						<!-- pricing nine -->
						<div class="style">
							<div class="js-slide position-relative bg-white shadow-sm transition-3d-hover rounded my-8">
								<article class="align-self-end w-100 text-center p-6">

									<strong class="d-block ">Photoshop Shadow Services</strong>
									<p>Start From</p>

									<ul class="cd-pricing-features pricing">
										<li>Basic  <strong style="color: black;">:$0.25</strong></li>
										<li>Simple  <strong style="color: black;">:$0.49 </strong></li>
										<li>Compound  <strong style="color: black;">:$0.49</strong></li>
										<li>Complex  <strong style="color: black;">:$1.00</strong></li>
									</ul>
									<a class="btn btn-sm u-btn-primary--air u-btn-wide transition-3d-hover" href="">Read More</a>
								</article>
							</div>
						</div>
						<!-- End of nine -->
					</div>
					<!-- End News Carousel -->
				</div>
			</div>
			<!-- End pricing  Section -->
		</div>
		<!--clients-->
		<!-- <div class="container u-space-1">

			<div class="row">
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 stats-grid stats-grid-1">
					<div class="counter">8,000</div>
					<div class="stat-info">
						<h4>Happy Clients</h4>
					</div>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 stats-grid stats-grid-2">
					<div class="counter">55,190
					</div>
					<div class="stat-info">
						<h4>Projects Finished

						</h4>
					</div>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 stats-grid stats-grid-3">
					<div class="counter">915,300
					</div>
					<div class="stat-info">
						<h4>Hours of work</h4>
					</div>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 stats-grid stats-grid-4">
					<div class="counter">2000</div>
					<div class="stat-info">
						<h4>Files Completed

						</h4>
					</div>
				</div>
			</div>
		</div> -->

		<div class="contact" id="contact">
			<div class="container">
				<h3>Contact Us</h3>
				<div class=" contact-form">
					<form class="form-horizontal mt-5" action="{{URL::to('indices-save')}}" method="post" enctype="multipart/form-data">
							{{csrf_field()}}
						<div class="row contact-bothside-agileinfo">
							<form action="send" method="post">
								{{csrf_field()}}
								<div class="col-md-6 col-sm-6 col-xs-6 form-right form-left">
									<input type="text" id="name" name="name" placeholder="Enter your name" required="">
								</div>
								<div class="col-md-6 col-sm-6 col-xs-6 form-right ">
									<input type="text" id="subject" name="subject" placeholder="Subject" required="">
								</div>
								<div class="col-md-6 col-sm-6 col-xs-6 form-right form-left">
									<input type="text" id="phone" name="phone" placeholder="Phone" required="">
								</div>
								<div class="col-md-6 col-sm-6 col-xs-6 form-right ">
									<input type="email" id="to" name="to" placeholder="Email" required="">
								</div>
							
								<textarea name="message" id="message" placeholder="Message" required=""></textarea>
							</form>
							<input type="submit" value="SUBMIT">
						</div>
					</form>
				</div>
			</div>
		</div>

	</main>
	<!-- ========== END MAIN CONTENT ========== -->
	<!--footer--->

	<footer>
		<div class="social-icons">

			<ul>
				<li><a href="https://www.facebook.com/sakibgreen1"><span class="fa fa-facebook"></span></a></li>
				<li><a href="#"><span class="fa fa-rss"></span></a></li>
				<li><a href="#"><span class="fa fa-vk"></span></a></li>
			</ul>
		</div>

		<p> Copyright© 2018. All Rights Reserved | <a href="http://linkvisionsoft.com/">Linkvision Software Solution ltd. </a></p>
	</footer>

	<!-- ========== END FOOTER ========== -->
	<!-- Go to Top -->
	<a class="js-go-to u-go-to" href="#" data-position='{"bottom": 15, "right": 15 }' data-type="fixed" data-offset-top="400" data-compensation="#header" data-show-effect="slideInUp" data-hide-effect="slideOutDown">
		<span class="fa fa-arrow-up u-go-to__inner"></span>
	</a>
	<!-- End Go to Top -->

	<!-- JS Global Compulsory -->
	<script src="assets/js/jquery.min.js"></script>
	<script src="assets/js/jquery-migrate.min.js"></script>
	<script src="assets/js/popper.min.js"></script>
	<script src="assets/js/bootstrap.min.js"></script>

	<!-- JS Implementing Plugins -->
	<script src="assets/js/hs.megamenu.js"></script>
	<script src="assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
	<script src="assets/js/jquery.validate.min.js"></script>
	<script src="assets/js/jquery.fancybox.min.js"></script>
	<script src="assets/js/typed.min.js"></script>
	<script src="assets/js/slick.js"></script>

	<!-- JS Front -->
	<script src="assets/js/hs.core.js"></script>
	<script src="assets/js/hs.header.js"></script>
	<script src="assets/js/hs.unfold.js"></script>
	<script src="assets/js/hs.focus-state.js"></script>
	<script src="assets/js/hs.malihu-scrollbar.js"></script>
	<script src="assets/js/hs.validation.js"></script>
	<script src="assets/js/hs.fancybox.js"></script>
	<script src="assets/js/hs.slick-carousel.js"></script>
	<script src="assets/js/hs.show-animation.js"></script>
	<script src="assets/js/hs.go-to.js"></script>
	<!--clients chart js-->
	<script src="assets/js/jquery.countup.js"></script>
	<script src="assets/js/jquery.waypoints.min.js"></script>

	<!-- JS Plugins Init. -->
<script>
	$(window).on('load', function() {
		// initialization of HSMegaMenu component
		$('.js-mega-menu').HSMegaMenu({
			event: 'hover',
			pageContainer: $('.container'),
			breakpoint: 767,
			hideTimeOut: 0
		});
	});

	$(document).on('ready', function() {
		// initialization of header
		$.HSCore.components.HSHeader.init($('#header'));

		// initialization of unfold component
		$.HSCore.components.HSUnfold.init($('[data-unfold-target]'), {
			afterOpen: function() {
				$(this).find('input[type="search"]').focus();
			}
		});

		// initialization of malihu scrollbar
		$.HSCore.components.HSMalihuScrollBar.init($('.js-scrollbar'));

		// initialization of forms
		$.HSCore.helpers.HSFocusState.init();

		// initialization of form validation
		$.HSCore.components.HSValidation.init('.js-validate', {
			rules: {
				confirmPassword: {
					equalTo: '#password'
				}
			}
		});

		// initialization of show animations
		$.HSCore.components.HSShowAnimation.init('.js-animation-link');

		// initialization of fancybox
		$.HSCore.components.HSFancyBox.init('.js-fancybox');

		// initialization of text animation (typing)
		

		// initialization of slick carousel
		$.HSCore.components.HSSlickCarousel.init('.js-slick-carousel');

		// initialization of go to
		$.HSCore.components.HSGoTo.init('.js-go-to');
	});
</script>

<!--client chart js-->
<script>
	$('.counter').countUp();
</script>

</body>


</html>

