<!DOCTYPE html>
<html lang="en"> 

<head>
	<!-- Title -->
	<title>Rainbow Clipping</title>

	<!-- Required Meta Tags Always Come First -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<!-- Favicon -->
	<link rel="shortcut icon" href="assets/img/fav.png">

	<!-- Google Fonts -->
	<link href="http://fonts.googleapis.com/css?family=Poppins:300,400,500,600" rel="stylesheet">

	<!-- CSS Global Compulsory -->
	<link rel="stylesheet" href="assets/css/bootstrap.css">

	<!-- CSS Implementing Plugins -->
	<link rel="stylesheet" href="assets/css/font-awesome.min.css">
	<link rel="stylesheet" href="assets/css/animate.min.css">
	<link rel="stylesheet" href="assets/css/hs.megamenu.css">
	<link rel="stylesheet" href="assets/css/jquery.mCustomScrollbar.css">
	<link rel="stylesheet" href="assets/css/slick.css">

	<!-- CSS Front Template -->
	<link rel="stylesheet" href="assets/css/front.css">
	<link rel="stylesheet" href="assets/css/linkvision.css">

	<!-- Fix Internet Explorer ______________________________________>
	<!--[if lt IE 9]>
			<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
			<script src="vendor/html5shiv.js"></script>
			<script src="vendor/respond.js"></script>
		<![endif]-->
</head>

<body>
	<!-- Skippy -->
	<a id="skippy" class="sr-only sr-only-focusable u-skippy" href="#content">
		<div class="container">
			<span class="u-skiplink-text">Skip to main content</span>
		</div>
	</a>
	<!-- End Skippy -->

	<!-- ========== HEADER ========== -->
	<header id="header" class="u-header u-header--abs-top-md u-header--bg-transparent u-header--show-hide-md" data-header-fix-moment="500" data-header-fix-effect="slide">
		<!-- Search -->
		<!-- <div id="searchPushTop" class="u-search-push-top">
			<div class="container position-relative">
				<div class="u-search-push-top__content mx-auto">
					
					<button type="button" class="close u-search-push-top__close-btn" aria-haspopup="true" aria-expanded="false" aria-controls="searchPushTop" data-unfold-type="jquery-slide" data-unfold-target="#searchPushTop">
						<span aria-hidden="true">&times;</span>
					</button>
				
					<form class="js-focus-state input-group input-group-lg u-form u-form--lg u-form--no-brd">
						<input type="search" class="form-control u-form__input" placeholder="Search Front" aria-label="Search Front">
						<div class="input-group-append">
							<button type="button" class="btn btn-primary u-btn-primary">Search</button>
						</div>
					</form>
					
				</div>
			</div>
		</div> -->
		<!-- End Search -->

		<div class="u-header__section">
			<!-- Topbar -->
			<div class="container u-header__hide-content pt-2">
				<div class="d-flex align-items-center mb-0">
					<div class="ml-auto">
						<!-- Links -->
						<div class="d-none d-sm-inline-block ml-sm-auto">
							<ul class="list-inline mb-0">
								<!-- Authentication Links -->
								@if (Auth::guest())
									<li><a href="{{ route('login') }}">Login</a></li>
								@else
									<li class="dropdown">
										<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
											{{ Auth::user()->name }} <span class="caret"></span>
										</a>

										<ul class="dropdown-menu" role="menu">
											<li>
													@role('Admin') {{-- Laravel-permission blade helper --}}
													<a href="{{ url('/backend') }}"><i class="fa fa-btn fa-unlock"></i>Admin</a>
													@endrole
													<a href="{{ route('logout') }}"
														onclick="event.preventDefault();
																document.getElementById('logout-form').submit();">
														Logout
													</a>

													<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
														{{ csrf_field() }}
													</form>
											</li>
										</ul>
									</li>
								@endif
							</ul>
						</div>
						<!-- End Links -->
					</div>

					<ul class="list-inline ml-2 mb-0">
						<!-- Search -->
						<!-- <li class="list-inline-item">
							<a class="btn btn-xs u-btn--icon u-btn-text-secondary" href="javascript:;" role="button" aria-haspopup="true" aria-expanded="false" aria-controls="searchPushTop" data-unfold-type="jquery-slide" data-unfold-target="#searchPushTop">
								<span class="fa fa-search u-btn--icon__inner"></span>
							</a>
						</li> -->
						<!-- End Search -->

						<!-- Shopping Cart -->
						<!-- <li class="list-inline-item position-relative">
							<a id="shoppingCartDropdownInvoker" class="btn btn-xs u-btn--icon u-btn-text-secondary" href="javascript:;" role="button" aria-controls="shoppingCartDropdown" aria-haspopup="true" aria-expanded="false" data-unfold-event="hover" data-unfold-target="#shoppingCartDropdown" data-unfold-type="css-animation" data-unfold-duration="300" data-unfold-delay="300" data-unfold-hide-on-scroll="true" data-unfold-animation-in="slideInUp" data-unfold-animation-out="fadeOut">
								<span class="fa fa-shopping-cart u-btn--icon__inner"></span>
							</a>

							<div id="shoppingCartDropdown" class="text-center u-unfold right-0 p-7 mt-2" aria-labelledby="shoppingCartDropdownInvoker" style="min-width: 250px;">
								<span class="u-shopping-cart-icon mb-4">
									<span class="fa fa-shopping-basket u-shopping-cart-icon__inner"></span>
								</span>
								<span class="d-block">Your Cart is Empty</span>
							</div>
						</li> -->
						<!-- End Shopping Cart -->

						<!-- Account Login -->
						<li class="list-inline-item">	
							<a id="sidebarNavToggler" class="btn btn-xs u-btn--icon u-btn-text-secondary u-sidebar__toggler" href="javascript:;" role="button" aria-controls="sidebarContent" aria-haspopup="true" aria-expanded="false" data-unfold-event="click" data-unfold-hide-on-scroll="false" data-unfold-target="#sidebarContent" data-unfold-type="css-animation" data-unfold-animation-in="fadeInRight" data-unfold-animation-out="fadeOutRight" data-unfold-duration="500">
								<span class="fa fa-user-circle u-btn--icon__inner font-size-14"></span>
							</a>
						</li>
						<!-- End Account Login -->
					</ul>
				</div>
			</div>
			<!-- End Topbar -->

			<div id="logoAndNav" class="container">
				<!-- Nav -->
				<nav class="js-mega-menu navbar navbar-expand-md u-header__navbar">
					<!-- Logo -->
					<a class="navbar-brand u-header__navbar-brand u-header__navbar-brand-top-space" href="{{ url('/') }}" aria-label="Front">
						<img src="assets/img/rnblogo.png" alt="Logo">
					</a> 
					<!-- End Logo -->

					<!-- Responsive Toggle Button -->
					<button type="button" class="navbar-toggler btn u-hamburger" aria-label="Toggle navigation" aria-expanded="false" aria-controls="navBar" data-toggle="collapse" data-target="#navBar">
						<span id="hamburgerTrigger" class="u-hamburger__box">
							<span class="u-hamburger__inner"></span>
						</span>
					</button>
					<!-- End Responsive Toggle Button -->

					<!-- Navigation -->
					<div id="navBar" class="collapse navbar-collapse py-0">
						<ul class="navbar-nav u-header__navbar-nav ml-lg-auto">
							<!-- Home -->
							<li class="nav-item">
								<a class="nav-link u-header__nav-link" href="{{ url('/') }}">
									Home
								</a>
							</li>
							<!-- End Home -->
							<!-- About us -->
							<li class="nav-item">
								<a class="nav-link u-header__nav-link" href="{{ url('/aboutus') }}">
									About Us
								</a>
							</li>
							<!-- End about us -->
							<!--services -->
							<li class="nav-item hs-has-mega-menu u-header__nav-item" data-event="hover" data-animation-in="slideInUp" data-animation-out="fadeOut" data-position="right">
								<a id="worksMegaMenu" class="nav-link u-header__nav-link" href="javascript:;" aria-haspopup="true" aria-expanded="false">
									Services
									<span class="fa fa-angle-down u-header__nav-link-icon"></span>
								</a>

								<!-- Works - Mega Menu -->
								<div class="hs-mega-menu w-100 u-header__sub-menu u-header__mega-menu-wrapper-v1" aria-labelledby="worksMegaMenu">
									<div class="row">
										<div class="col-sm-4 col-lg-2 mb-3 mb-lg-0">
											<strong class="d-block mb-2">Remove Background From Images</strong>

											<!-- Links -->
											<ul class="list-unstyled u-list">
												<li><a class="nav-link u-list__link py-2 px-0" href="{{ url('/clippingpath') }}">Clipping Path Service</a></li>
												<li><a class="nav-link u-list__link py-2 px-0" href="{{url('/imagemasking')}}">Image Masking Service</a></li>
												<li><a class="nav-link u-list__link py-2 px-0" href="{{url('/shadowcreation')}}">Shadow creations Service</a></li>
											</ul>
											<!-- End Links --> 
										</div>

										<div class="col-sm-4 col-lg-2 mb-3 mb-lg-0">
											<strong class="d-block mb-2">Professinal Image Retouching</strong>

											<!-- Links -->
											<ul class="list-unstyled u-list">
												<li><a class="nav-link u-list__link py-2 px-0" href="{{url('/ghost')}}">Ghost mannequin</a></li>
												<li><a class="nav-link u-list__link py-2 px-0" href="{{url('/wedding')}}">Wedding Photo Retouching</a></li>
												<li><a class="nav-link u-list__link py-2 px-0" href="{{url('/photoretouching')}}">Photo Retouching</a></li>
											</ul>
											<!-- End Links -->
										</div>

										<div class="col-sm-4 col-lg-2 mb-3 mb-sm-0">
											<strong class="d-block mb-2">Phography Post Production</strong>

											<!-- Links -->
											<ul class="list-unstyled u-list">
												<li><a class="nav-link u-list__link py-2 px-0" href="{{url('/product')}}">Product Photo Editing</a></li>
												<li><a class="nav-link u-list__link py-2 px-0" href="{{url('/realestate')}}">Real Estate Photo Editing</a></li>
												<li><a class="nav-link u-list__link py-2 px-0" href="{{url('/photocroping')}}">Photo resizing/cropping</a></li>
											</ul>
											<!-- End Links -->
										</div>

										<div class="col-sm-4 col-lg-2">
											<strong class="d-block mb-2">Creative Editing Services</strong>

											<!-- Links -->
											<ul class="list-unstyled u-list">
												<li><a class="nav-link u-list__link py-2 px-0" href="{{url('/vector')}}">Vector-Conversion-Illustration</a></li>
												<li><a class="nav-link u-list__link py-2 px-0" href="{{url('/color')}}">Colour correction</a></li>
												<li><a class="nav-link u-list__link py-2 px-0" href="{{url('/photorestoration')}}">Photo restoration</a></li>
												<li><a class="nav-link u-list__link py-2 px-0" href="{{url('/logodesign')}}">Logo Design</a></li>
											</ul>
											<!-- End Links -->
										</div>

										<div class="col-sm-4 col-lg-2">
											<strong class="d-block mb-2"> Web Services</strong>

											<!-- Links -->
											<ul class="list-unstyled u-list">
												<li><a class="nav-link u-list__link py-2 px-0" href="{{url('/webdesign')}}">Web Design & Development</a></li>
												<li><a class="nav-link u-list__link py-2 px-0" href="{{url('/apps')}}">App Development</a></li>
												<li><a class="nav-link u-list__link py-2 px-0" href="{{url('/software')}}">Application Development</a></li>
												<li><a class="nav-link u-list__link py-2 px-0" href="{{url('/ecommerce')}}">Digital Marketing</a></li>
											</ul>
											<!-- End Links -->
										</div>

									</div>
								</div>
								<!-- End Works - Mega Menu -->
							</li>
							<!-- End Works -->
							<!--gallaries-->
							<li class="nav-item">
								<a class="nav-link u-header__nav-link" href="{{ url('/gallery') }}">
									Galleries
								</a>
							</li>

							<!-- End gallaries -->
							<!-- Price -->
							<li class="nav-item">
								<a class="nav-link u-header__nav-link" href="{{ url('/pricing') }}">
									Price
								</a>
							</li>
							<!-- End Price-->

							<!-- Payment -->
							<li class="nav-item">
								<a class="nav-link u-header__nav-link" href="{{ url('/paypal') }}">
									Payment
								</a>
							</li>
							<!-- End payment -->
							<!--upload-->
							<li class="nav-item">
								<a class="nav-link u-header__nav-link" href="{{ url('/upload') }}">
									Upload Files
								</a>
							</li>
							<!--end of upload -->
							<!-- Button -->
							<li class="nav-item d-none d-md-inline-block pl-3 pr-0">
								<a class="btn btn-sm btn-primary u-btn-primary transition-3d-hove" href="{{ url('/freetrial') }}">
									Free Trial
								</a>
							</li>
							<li class="nav-item d-none d-md-inline-block pl-3 pr-0">
								<a class="btn btn-sm btn-primary u-btn-primary transition-3d-hove" href="{{ url('/quote') }}">
									Get Quote
								</a>
							</li>
							<!-- End Button -->
						</ul>
					</div>
					<!-- End Navigation -->
				</nav>
				<!-- End Nav -->
			</div>
		</div>
	</header>
	<!-- ========== END HEADER ========== -->

	<!-- ========== MAIN CONTENT ========== -->
	<main id="content" role="main">
		<div class="contact" id="contact">
			<div class="container">
				<h3>Contact Us</h3>
				<div class="contact-form">
					<form class="form-horizontal mt-5" action="{{route('indices.update',['id'=>$indices->id])}}" method="post" enctype="multipart/form-data">
							{{csrf_field()}}
						<div class="row contact-bothside-agileinfo">
							<div class="col-md-6 col-sm-6 col-xs-6 form-right form-left">
								<input type="text" id="name" name="name" placeholder="Enter your name" required="" value="{{$indices->indices}}">
							</div>
							<div class="col-md-6 col-sm-6 col-xs-6 form-right">
								<input type="text" id="subject" name="subject" placeholder="Subject" required="" value="{{$indices->indices}}">
							</div>
							<div class="col-md-6 col-sm-6 col-xs-6 form-right form-left">
								<input type="text" id="phone" name="phone" placeholder="Phone" required="" value="{{$indices->indices}}">
							</div>
							<form action="send" method="post">
								{{csrf_field()}}
								<div class="col-md-6 col-sm-6 col-xs-6 form-right ">
									<input type="email" id="to" name="to" placeholder="Email" required="" value="{{$indices->indices}}">
								</div>
								<textarea name="message" id="message" placeholder="Message" required="" value="{{$indices->indices}}"></textarea>
							</form>
							<input type="submit" value="SUBMIT">
						</div>
					</form>
				</div> 
			</div>
		</div>
	</main>
	<!-- ========== END MAIN CONTENT ========== -->

	<!-- Go to Top -->
	<a class="js-go-to u-go-to" href="#" data-position='{"bottom": 15, "right": 15 }' data-type="fixed" data-offset-top="400" data-compensation="#header" data-show-effect="slideInUp" data-hide-effect="slideOutDown">
		<span class="fa fa-arrow-up u-go-to__inner"></span>
	</a>
	<!-- End Go to Top -->

	<!-- JS Global Compulsory -->
	<script src="assets/js/jquery.min.js"></script>
	<script src="assets/js/jquery-migrate.min.js"></script>
	<script src="assets/js/popper.min.js"></script>
	<script src="assets/js/bootstrap.min.js"></script>

	<!-- JS Implementing Plugins -->
	<script src="assets/js/hs.megamenu.js"></script>
	<script src="assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
	<script src="assets/js/jquery.validate.min.js"></script>
	<script src="assets/js/jquery.fancybox.min.js"></script>
	<script src="assets/js/typed.min.js"></script>
	<script src="assets/js/slick.js"></script>

	<!-- JS Front -->
	<script src="assets/js/hs.core.js"></script>
	<script src="assets/js/hs.header.js"></script>
	<script src="assets/js/hs.unfold.js"></script>
	<script src="assets/js/hs.focus-state.js"></script>
	<script src="assets/js/hs.malihu-scrollbar.js"></script>
	<script src="assets/js/hs.validation.js"></script>
	<script src="assets/js/hs.fancybox.js"></script>
	<script src="assets/js/hs.slick-carousel.js"></script>
	<script src="assets/js/hs.show-animation.js"></script>
	<script src="assets/js/hs.go-to.js"></script>
	<!--clients chart js-->
	<script src="assets/js/jquery.countup.js"></script>
	<script src="assets/js/jquery.waypoints.min.js"></script>

	<!-- JS Plugins Init. -->
<script>
	$(window).on('load', function() {
		// initialization of HSMegaMenu component
		$('.js-mega-menu').HSMegaMenu({
			event: 'hover',
			pageContainer: $('.container'),
			breakpoint: 767,
			hideTimeOut: 0
		});
	});

	$(document).on('ready', function() {
		// initialization of header
		$.HSCore.components.HSHeader.init($('#header'));

		// initialization of unfold component
		$.HSCore.components.HSUnfold.init($('[data-unfold-target]'), {
			afterOpen: function() {
				$(this).find('input[type="search"]').focus();
			}
		});

		// initialization of malihu scrollbar
		$.HSCore.components.HSMalihuScrollBar.init($('.js-scrollbar'));

		// initialization of forms
		$.HSCore.helpers.HSFocusState.init();

		// initialization of form validation
		$.HSCore.components.HSValidation.init('.js-validate', {
			rules: {
				confirmPassword: {
					equalTo: '#password'
				}
			}
		});

		// initialization of show animations
		$.HSCore.components.HSShowAnimation.init('.js-animation-link');

		// initialization of fancybox
		$.HSCore.components.HSFancyBox.init('.js-fancybox');

		// initialization of text animation (typing)
		

		// initialization of slick carousel
		$.HSCore.components.HSSlickCarousel.init('.js-slick-carousel');

		// initialization of go to
		$.HSCore.components.HSGoTo.init('.js-go-to');
	});
</script>

<!--client chart js-->
<script>
	$('.counter').countUp();
</script>

</body>


</html>
