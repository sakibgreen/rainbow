<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="au theme template">
    <meta name="author" content="Hau Nguyen">
    <meta name="keywords" content="au theme template">

    <!-- Title Page-->
    <title>Admin 
            @yield('title')
    </title>
    
    @include('layouts.partials.css')
    <!-- Fontfaces CSS-->
    

</head>

<body class="animsition">
    <div class="page-wrapper">
    @include('layouts.top-navbar')
    @include('layouts.navber')
    @yield('content')
    @include('layouts.footer')
           
    </div>
    </div>

    <!-- Jquery JS-->
    @include('layouts.partials.js')
    
</body>

</html>
<!-- end document-->