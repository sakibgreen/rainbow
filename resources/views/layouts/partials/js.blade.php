<script src="{{asset('admin/assers/vendor/jquery-3.2.1.min.js')}}"></script>
    <!-- Bootstrap JS-->
    <script src="{{asset('admin/assers/vendor/bootstrap-4.1/popper.min.js')}}"></script>
    <script src="{{asset('admin/assers/vendor/bootstrap-4.1/bootstrap.min.js')}}"></script>
    <!-- Vendor JS       -->
    <script src="{{asset('admin/assers/vendor/slick/slick.min.js')}}">
    </script>
    <script src="{{asset('admin/assers/vendor/wow/wow.min.js')}}"></script>
    <script src="{{asset('admin/assers/vendor/animsition/animsition.min.js')}}"></script>
    <script src="{{asset('admin/assers/vendor/bootstrap-progressbar/bootstrap-progressbar.min.js')}}">
    </script>
    <script src="{{asset('admin/assers/vendor/counter-up/jquery.waypoints.min.js')}}"></script>
    <script src="{{asset('admin/assers/vendor/counter-up/jquery.counterup.min.js')}}">
    </script>
    <script src="{{asset('admin/assers/vendor/circle-progress/circle-progress.min.js')}}"></script>
    <script src="{{asset('admin/assers/vendor/perfect-scrollbar/perfect-scrollbar.js')}}"></script>
    <script src="{{asset('admin/assers/vendor/chartjs/Chart.bundle.min.js')}}"></script>
    <script src="{{asset('admin/assers/vendor/select2/select2.min.js')}}">
    </script>

    <!-- Main JS-->
    <script src="{{asset('admin/assers/js/main.js')}}"></script>
