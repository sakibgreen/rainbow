<?php
 
namespace App\Http\Controllers;

use DB;
use Auth;
use Session;
use App\User;
use App\Quote; 

use Mail;
use App\Mail\SendMail;
use Illuminate\Http\Request;

class QuoteController extends Controller
{

    // public function __construct() {
    //     $this->middleware(['auth', 'clearance'])->except('create','edit');
    // }
    
    public function create()
    {
    	return view('rainbow.quote');
    }

  
    public function store(Request $request)
    {
        $this->validate($request,
            [
				'name'           => 'required|max:255',
                'phone'          => 'required|max:11',
                'address'        => 'max:255',
            ]);
                //dd($request->all());
        $images = array();
        if ($request->hasFile('images')) {
            foreach ($request->images as $image) {

                $name = $image->getClientOriginalName();
                $image->storeAs('public/upload', $name);
                $images[] = $name;

            }
        } 
 
        Quote::create([
			'name'          => request('name'),
            'company'       => request('company'),
            'phone'         => request('phone'),
            'address'       => request('address'),
            'website'       => request('website'),
            'images'        => implode("|", $images), 
            'country'       => request('country'),
            'job_title'       => request('job_title'),
            'to'            => request('to'),
            'message'       => request('message'),

            			
		]);
		Mail::send(new SendMail("message"));

        return redirect('quote');
    }

    public function edit($id)
    {
        $quotes = Quote::find($id);
        return view('rainbow.quote_edit',compact('quotes'));
    }

    public function update(Request $request, $id)
    {
        $quote              = Quote::find($id);
        $quote->name        = $request->name;
        $quote->company     = $request->company;
        $quote->phone       = $request->phone;
        $quote->address     = $request->address;
        $quote->website     = $request->website;
        $quote->images      = $request->images;
        $quote->country     = $request->country;
        $quote->job_title   = $request->job_title;
        $quote->to          = $request->to;
        $quote->message     = $request->message; 
        $quote->save();

        return redirect('/backend/quote');
    }

    public function destroy($id)
    {
        $quotes =  Quote::find($id);
        $quotes->delete();

        return back(); 
    }

}
    