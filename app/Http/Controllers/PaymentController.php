<?php

namespace App\Http\Controllers;
use DB;
use Auth;
use Session;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;  
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Api\OAuthTokenCredential;
use PayPal\Api\ApiContext;
use Redirect;
use URL;


class PaymentController extends Controller
{
    
    private $_api_context;

    public function __construct() { 

        // setup PayPal api context
        $paypal_conf = \Config::get('paypal');
        $this->_api_context = new ApiContext(new OAuthTokenCredential($paypal_conf['client_id'], $paypal_conf['secret']));
        $this->_api_context->setConfig($paypal_conf['settings']);

    }

    public function paywithpaypal(){
        $payer = new Payer();
        $payer->setPaymentMethod('paypal');

        $item1 = new Item();
        $item1->setName('Ground Coffee 40 oz')
            ->setCurrency('USD')
            ->setQuantity(1)
            ->setPrice($request->get('amount'));

        $itemList = new ItemList();
        $itemList->setItems(array($item1));

        $amount = new Amount();
        $amount->setCurrency("USD")
            ->setTotal($request->get('amount'));
        
        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setItemList($itemList)
            ->setDescription('Your Transaction description');

        $redirectUrls = new RedirectUrls();
        $redirectUrls->setReturnUrl(URL::to('status'))
            ->setCancelUrl(URL::to('status'));
        
        $payment = new Payment();
        $payment->setIntent("sale")
            ->setPayer($payer)
            ->setRedirectUrls($redirectUrls)
            ->setTransactions(array($transaction));

        try {
            $payment->create($this->_api_Context);
        } catch (\PayPal\Exception\PPConnectionException $ex) {
            if(\Config::get('app.debug')) {
                \Session::put('error','Connection timeout');
                return Redirect::to('/paypal');
            } else {
                \Session::put('error','Some error occur,sorry for inconvenient');
                return Redirect::to('/paypal');
            }
        }

        foreach ($payment->getLinks() as $link) {
            if ($link->getRel() == 'approvalurl') {

                $redirecturl = $link->getHref();
                break;
            }
        }

        Session::put('paypal_payment_id', $payment->getId());
        
        if (isset($redirecturl)) {

            return Redirect::array($redirecturl);
        }

        \Session::put('error','Unknown error accurred');
        return Redirect::to('/paypal');      
    }

    

    public function getPaymentStatus(){
        $payment_id = Session::get('paypal_payment_id');

        Session::forget('paypal_payment_id');

        if (empty(Input::get('PaymentID')) || empty(Input::get('token'))) {
            
            \Session::put('error','Payment failed');
            return Redirect::to('/paypal');
        }

        $payment = Payment::get($paymentId, $this->_api_Context);
        $execution = new PaymentExecution();
        $execution->setPayerId(Input::get('PayerID'));

        $result = $payment->execute($execution, $this->_api_context);
    
        if ($result->getState() == 'approved'){
            
            \Session::put('success', 'Payment Success');
            return Redirect::to('/paypal');
        }

        \Session::put('erroe', 'Payment failed');
        return Redirect::to('/paypal');
    }
}
