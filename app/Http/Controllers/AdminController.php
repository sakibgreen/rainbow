<?php

namespace App\Http\Controllers;


use App\Index; 
use App\Upload;
use App\Admin;
use App\Quote;
use App\Freetrial;
use App\User;
use DB;
use Auth;
use Session;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function __construct() {
        $this->middleware(['auth', 'isAdmin']); //isAdmin middleware lets only users with a //specific permission permission to access these resources
    }

    public function index()
    {
        return view('adminrainbow.admin');
    }

    public function adminindex()
    {
        $indices = Index::orderby('id','desc')->paginate(5);
        return view('adminrainbow.adminindex',compact('indices'));
        
        // $indices           = Index::all();
        // return view('adminrainbow.adminindex',compact('indices'));
    }

    public function adminupload()
    {
        $uploads  = Upload::orderby('id','desc')->paginate(5);

        return view('adminrainbow.adminupload',compact('uploads'));
    }

    public function adminquote()
    {
        $quotes  = Quote::orderby('id','desc')->paginate(5);
        return view('adminrainbow.adminquote',compact('quotes'));
    }

    public function adminfreetrial()
    {
        $freetrials  = Freetrial::orderby('id','desc')->paginate(5);
        return view('adminrainbow.adminfreetrial',compact('freetrials'));
    }
}
