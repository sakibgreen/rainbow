<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Session;
use App\User;
use App\Index; 
use App\Admin; 
use Mail;
use App\Mail\SendMail;

use Illuminate\Http\Request;

class IndexController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    


    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('rainbow.index');
    }

    public function store(Request $request) 
    {       

            Index::create([
            'name'          => request('name'),
            'subject'       => request('subject'),  
            'phone'         => request('phone'),
            'to'            => request('to'),
            'message'       => request('message'),

            
            ]);  
            Mail::send(new SendMail("message"));

            return redirect('/');
    }

    public function edit($id)
    {
        $indices = Index::find($id);
        // dd($indices);
        return view('rainbow.index_edit',compact('indices'));
    }

    public function update(Request $request, $id)
    {
        $index              = Index::find($id);
        $index->name        = $request->name;
        $index->subject     = $request->subject;
        $index->phone       = $request->phone;
        $index->to          = $request->to;
        $index->message     = $request->message;
        $index->save();

        return redirect('/backend/index')->with('flash_message', 
        'Article, '. $index->name.' updated');
    }

   
    public function destroy($id)
    {
        $indices =  Index::find($id);
        // dd($indices);
        $indices->delete();

        return back()->with('flash_message',
        'Article successfully deleted'); 
    }
}
