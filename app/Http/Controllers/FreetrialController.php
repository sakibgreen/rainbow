<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Session;
use App\User;
use App\Freetrial; 
use Mail;
use App\Mail\SendMail;

use Illuminate\Http\Request;

class FreetrialController extends Controller
{
        
    public function create()
    {
    	return view('rainbow.freetrial');
    }

  
    public function store(Request $request)
    {
        $this->validate($request,
            [
				'name'           => 'required|max:255',
                'phone'          => 'required|max:11',
                'address'        => 'max:255',
            ]);

        $images = array();
        if ($request->hasFile('images')) {
            foreach ($request->images as $image) {

                $name = $image->getClientOriginalName();
                $image->storeAs('public/upload', $name);
                $images[] = $name;
 
            }
        }

        Freetrial::create([
			'name'          => request('name'),
            'company'       => request('company'),
            'phone'         => request('phone'),
            'address'       => request('address'),
            'website'       => request('website'),
            'images'        => implode("|", $images),
            'job_title'     => request('job_title'),
            'to'            => request('to'),
            'message'       => request('message'),


            			
		]);
		Mail::send(new SendMail("message"));

        return redirect('freetrial');
    }

    public function edit($id)
    {
        $freetrials = Freetrial::find($id);
        return view('rainbow.freetrial_edit',compact('freetrials'));
    }

    public function update(Request $request, $id)
    {
        $freetrial             = Freetrial::find($id);
        $freetrial->name       = $request->name;
        $freetrial->company    = $request->name;
        $freetrial->phone      = $request->name;
        $freetrial->address    = $request->name;
        $freetrial->website    = $request->name;
        $freetrial->images     = $request->name;
        $freetrial->job_title  = $request->name;
        $freetrial->to         = $request->name;
        $freetrial->message    = $request->name;

        $freetrial->save();

        return redirect('/backend/freetrial');
    }

    public function destroy($id)
    {
        $freetrials =  Freetrial::find($id);
        $freetrials->delete();

        return back(); 
    }

}
 