<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Upload extends Model
{
    protected $guarded = [];

    public function admins()
	{
		return $this->belongsTo(Admin::class);
	}
}
   