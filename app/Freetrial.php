<?php

namespace App;
use App\Admin;

use Illuminate\Database\Eloquent\Model;

class Freetrial extends Model
{
    protected $guarded=[];

    public function admins()
	{
		return $this->belongsTo(Admin::class);
	}
}
 